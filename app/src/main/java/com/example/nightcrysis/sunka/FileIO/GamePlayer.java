package com.example.nightcrysis.sunka.FileIO;

/**
 * Class responsible for storage of data regarding a single player. A gamePlayer is an object that
 * is used to store the game play statistics for each named player. Players are diferentiated based
 * on their name. Only 1 player can be stored persistently per name.
 * Created by Alex on 24/10/2015.
 */
public class GamePlayer {

    private String name;
    private long totalTime;
    private int wins;
    private int losses;
    private int draws;
    private int score;
    private int totalScore;


    /**
     *Constructor for gamePlayer.
     * @param name name of player
     * @param totalTime total time played
     * @param wins number of wins
     * @param losses number of losses
     * @param draws number of draws
     * @param score average score
     * @param totalScore total score
     */
    public GamePlayer(String name, long totalTime, int wins, int losses, int draws, int score, int totalScore) {
        this.name = name;
        this.totalTime = totalTime;
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
        this.score = score;
        this.totalScore = totalScore;
    }

    public String getName() {
        return name;
    }

    /**
     *Sets the name of the player.
     * @param name sets the player name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds the result of a game to the player object. This is used to update the player win/draw/loss
     * 1= win
     * 0= draw
     * -1 = loss
     *
     * @param winner
     */
    public void addResult(int winner) {
        if (winner == -1) {
            losses++;
        } else if (winner == 0) {
            draws++;
        } else {
            wins++;
        }
    }

    /**
     * Total time player has spent playing the game.
     * @return total time spent playing
     */
    public long getTotalTime() {
        return totalTime;
    }

    /**
     * Increases the total time spent playing by the amount.
     * @param totalTime updates total time playing.
     */
    public void setTotalTime(long totalTime) {
        this.totalTime += totalTime;
    }

    /**
     *
     * @return return number of wins
     */
    public int getWins() {
        return wins;
    }

    /**
     * @return number of losses
     */
    public int getLosses() {
        return losses;
    }

    /**
     * @return number of draws
     */
    public int getDraws() {
        return draws;
    }


    /**
     * Returns the total score/number of games played
     * @return AVERAGE score
     */
    public int getAverageScore() {
        return score;
    }

    /**
     * used to add another score to the statistics.
     * @param score add another score to the player. Will update average.
     */
    public void setScore(int score) {
        totalScore = totalScore + score;
        this.score = totalScore / (wins + losses + draws);

    }

    /**
     * Convinience method to update a player after a game.
     *
     * @param score player's post game score
     * @param win   player win value -1 (loss) 0 (draw) 1 (win)
     * @param time  time spent playing.
     */
    public void addGame(int score, int win, long time) {
        this.addResult(win);
        this.setScore(score);
        this.setTotalTime(time);

    }

    /**
     * @return all points scored by player
     */
    public int getTotalScore() {
        return totalScore;
    }

    /**
     * @return object as csv string.
     */
    @Override
    public String toString() {
        return
                name + ","
                        + totalTime + ","
                        + wins + ","
                        + losses + ","
                        + draws + ","
                        + score + ","
                        + totalScore;
    }


}
