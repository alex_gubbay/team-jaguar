package com.example.nightcrysis.sunka.Screen;

import com.example.nightcrysis.sunka.GameEntity.TrayHardAI;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Starts a new hard AI game.
 *
 * Extends {@link GameScreen}.
 *
 * @author florianaucomte
 */
public class HardAIGameScreen extends GameScreen
{
    /**
     * Creates a new HardAIGameScreen object. Creates a new {@link TrayHardAI} object and adds it
     * to the game entities.
     */
    public HardAIGameScreen()
    {
        float singlePotSize = GlobalVariables.screenSizeX/11; //Used for reference, total width of screen will be divided by 11
        tray = new TrayHardAI(0, GlobalVariables.screenSizeY / 2 - singlePotSize, GlobalVariables.screenSizeX, singlePotSize * 2);
        gameEntities.add(tray);
    }
}
