package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.EditText;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataTransmissionManager;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;

/**
 * Created by NIghtCrysIs on 2015/11/11.
 */
public class JoinGameFrame extends UIFrameWithOverlay{
    private UIButtonFrame joinButton, cancelButton, hostAddressInput, hostPortInput;
    private boolean frameClosing = false, connecting = false;
    private String messageTitle, hostAddressString = "Enter the host's ip address", hostPortString = "Enter the host's port number"; //Displays the host address and port strings
    private final float displacement = GlobalVariables.screenSizeY;

    private Thread clientServer = new Thread()
    {
        @Override
        public void run()
        {
            while(!DataTransmissionManager.connected())
            {
                try
                {
                    Thread.sleep(100);
                    Log.e("CONNECT","Waiting for connection...");
                }
                catch(InterruptedException e)
                {
                    ScreenManager.addToMasterOverlay(new MessageFrame("Join game has been interrupted."));
                    e.printStackTrace();
                    return;
                }
            }

            //Transitioning to online multiplayer game
            EventController screenTransitionEvent = new EventController();
            screenTransitionEvent.addEvent(new ColorFade(true, 0.5f, false, true));
            screenTransitionEvent.addEvent(new ChangeScreenTimer(0, ScreenState.onlineMultiplayerGame));
            screenTransitionEvent.addEvent(new ColorFade(false, 0.5f, false, true));
            ScreenManager.addToMasterOverlay(screenTransitionEvent);

            DataTransmissionManager.queueMessageForSending("Client is ready");
            Log.e("CONNECT", "Connected!");
        }
    };

    public JoinGameFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        initialize();
    }

    public JoinGameFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }

    private void initialize()
    {
        DataTransmissionManager.openServer();
        y -=  displacement; //Move frame off screen for transition

        //Initialize variables
        float tempWidth = width / 5,
                tempHeight = height / 8;

        int colorValue = Color.argb(255, 246, 176, 57);

        hostAddressInput = new UIButtonFrame(x + (width / 8), y + tempHeight,
                width * 6 / 8, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, hostAddressString);
        hostAddressInput.setTextColor(colorValue);
        hostAddressInput.setTextSizeAuto(0, hostAddressInput.getHeight() / 4);

        hostPortInput = new UIButtonFrame(x + (width / 8), y + (tempHeight * 3f),
                width * 6 / 8, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, hostPortString);
        hostPortInput.setTextColor(colorValue);
        hostPortInput.setTextSizeAuto(0, hostPortInput.getHeight() / 4);

        joinButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), y + height - (tileHeight),
                tempWidth, tileHeight/2, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Join game");
        joinButton.setTextColor(Color.GREEN);
        joinButton.setTextSizeAuto(0, joinButton.getHeight() / 4);

        cancelButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), y + height - (tileHeight),
                tempWidth, tileHeight/2, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Cancel");
        cancelButton.setTextColor(Color.RED);
        cancelButton.setTextSizeAuto(0, cancelButton.getHeight() / 4);

        uiSubComponents.add(hostAddressInput);
        uiSubComponents.add(hostPortInput);
        uiSubComponents.add(joinButton);
        uiSubComponents.add(cancelButton);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(hostAddressInput, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(hostPortInput, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(joinButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(cancelButton, 0, displacement, 0.5f, true, false));
    }

    /** Method for editing text using AlertDialogs both ip address and port */
    private void editButtonText(final UIButtonFrame uiBF)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(GlobalVariables.currentActivity);
        if((messageTitle != "Invalid port number - try again." | uiBF != hostPortInput) |
                (messageTitle != "Invalid ip address - try again." | uiBF != hostAddressInput))
        {
            if(uiBF == hostAddressInput)
                messageTitle = "Enter IP address.";
            else if(uiBF == hostPortInput)
                messageTitle = "Enter port number.";
        }
        builder.setTitle(messageTitle);

        // Set up the input
        final EditText input = new EditText(GlobalVariables.currentActivity);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                uiBF.setText(input.getText().toString());
                String temp = uiBF.getText();
                if (uiBF == hostAddressInput)
                {
                    hostAddressString = temp;
                    char[] tempCharArray = hostAddressString.toCharArray();
                    boolean fullStopFound = true, valid = true;
                    for (char x : tempCharArray)
                    {
                        //Check that the port includes a valid ip address, made up of finite number of digits followed by a full stop
                        if ((x >= '0') & (x <= '9'))
                            fullStopFound = false;
                        else if (x == '.' & !fullStopFound)
                            fullStopFound = true;
                        else valid = false;
                    }
                    if (valid)
                        valid = (tempCharArray[tempCharArray.length - 1] != '.');
                    if (!valid)
                    {
                        messageTitle = "Invalid ip address - try again.";
                        editButtonText(uiBF);
                    }
                }
                else if (uiBF == hostPortInput)
                {
                    try
                    {
                        Integer.parseInt(temp);
                        hostPortString = temp;
                        messageTitle = "";
                    }
                    catch(NumberFormatException e)
                    {
                        messageTitle = "Invalid port number - try again.";
                        editButtonText(uiBF);
                    }
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private boolean safetyCheck()
    {
        char[] tempCharArray = hostAddressString.toCharArray();
        boolean fullStopFound = true, valid = true;
        for (char x : tempCharArray)
        {
            //Check that the port includes a valid ip address, made up of finite number of digits followed by a full stop
            if ((x >= '0') & (x <= '9'))
                fullStopFound = false;
            else if (x == '.' & !fullStopFound)
                fullStopFound = true;
            else valid = false;
        }
        if (valid)
            valid = (tempCharArray[tempCharArray.length - 1] != '.');
        if (!valid)
        {
            ScreenManager.addToMasterOverlay(new MessageFrame("Invalid ip address. Please revise your input."));
            return false;
        }

        try
        {
            Integer.parseInt(hostPortString);
        }
        catch (NumberFormatException e)
        {
            ScreenManager.addToMasterOverlay(new MessageFrame("Invalid port. Please revise your input."));
            return false;
        }
        return true;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
    }

    @Override
    public void render(Canvas canvas) {
        super.render(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If the frame is closing, ignore the touches and return true
        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (hostAddressInput.isPressed())
        {
            editButtonText(hostAddressInput);
        }
        else if (hostPortInput.isPressed())
        {
            editButtonText(hostPortInput);
        }
        else if (joinButton.isPressed())
        {
            if (safetyCheck())
            {
                //Start Java Socket server connection
                DataTransmissionManager.openClient(hostAddressString, Integer.parseInt(hostPortString));
                Log.e("CONNECT", "Client: " + DataTransmissionManager.getPort());

                clientServer.start();
                joinButton.setEnabled(false);
                connecting = true;
            }

        }
        else if (cancelButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;
        }
        if (frameClosed)
        {
            if (connecting)
            {
                //Log.e("","Client server interrupted");
                clientServer.interrupt();
                DataTransmissionManager.stopConnection();
            }
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f),
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(hostAddressInput, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(hostPortInput, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(joinButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(cancelButton, 0, velocityY, 0, acceleration, 0.5f, true));
        }

        return true;
    }
}
