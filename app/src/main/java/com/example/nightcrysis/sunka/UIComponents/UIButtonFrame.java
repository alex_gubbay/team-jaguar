package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/10/28.
 * Custom UI class for displaying a button, but the images are that of a frame so that
 * any resizing operations will not skew the button as a whole.
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 *
 * Very similar structure and setup to UIButton
 * @see UIButton
 */
public class UIButtonFrame extends GameEntity {
    protected boolean selected, pressed, enabled; //Selected means the user has pressed down and that this is selected. Pressed means that the user has touched, and untouched the button, making it pressed.
    private UIFrame pressedFrame = null, unpressedFrame = null, disabledOverlay = null;

    private int textColor = Color.RED;
    private float textX = 0, textY = 0, textSize = 32; //Text's x and y offset, and text size
    private String text = ""; //Text to be shown

    /**
     * Constructor for UIButtonFrame. Sets the size and position of the UIButtonFrame.
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedFrameSpriteImageID    unpressed sprite resource ID
     * @param pressedFrameSpriteImageID      pressed sprite resource ID
     */
    public UIButtonFrame(float  _x, float _y, float _width, float _height, int unpressedFrameSpriteImageID, int pressedFrameSpriteImageID)
    {
        initialize(_x, _y, _width, _height, unpressedFrameSpriteImageID, pressedFrameSpriteImageID);
    }

    /**
     * Constructor for UIButtonFrame. Sets the size and position of the UIButtonFrame.
     * Has an extra parameter for disabled sprite to be set.
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedFrameSpriteImageID    unpressed sprite resource ID
     * @param pressedFrameSpriteImageID      pressed sprite resource ID
     * @param disabledOverlayImageID         disables sprite resource ID
     */
    public UIButtonFrame(float  _x, float _y, float _width, float _height, int unpressedFrameSpriteImageID, int pressedFrameSpriteImageID, int disabledOverlayImageID)
    {
        initialize(_x, _y, _width, _height, unpressedFrameSpriteImageID, pressedFrameSpriteImageID);
        disabledOverlay = new UIFrame(x, y, width, height, disabledOverlayImageID);
    }

    /**
     * Constructor for UIButtonFrame. Sets the size and position of the UIButtonFrame.
     * Has an extra parameter for disabled sprite to be set.
     * Further extension of text can be set with this constructor
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedFrameSpriteImageID    unpressed sprite resource ID
     * @param pressedFrameSpriteImageID      pressed sprite resource ID
     * @param disabledOverlayImageID         disables sprite resource ID
     * @param _text                          Text to be displayed over the UIButtonFrame
     */
    public UIButtonFrame(float  _x, float _y, float _width, float _height, int unpressedFrameSpriteImageID, int pressedFrameSpriteImageID, int disabledOverlayImageID, String _text)
    {
        text = _text;

        initialize(_x, _y, _width, _height, unpressedFrameSpriteImageID, pressedFrameSpriteImageID);
        disabledOverlay = new UIFrame(x, y, width, height, disabledOverlayImageID);
        setTextPositions();
    }

    private void initialize(float  _x, float _y, float _width, float _height, int unpressedFrameSpriteImageID, int pressedFrameSpriteImageID)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

        selected = false;
        pressed = false;
        enabled = true;

        unpressedFrame = new UIFrame(x, y, width, height, unpressedFrameSpriteImageID);
        pressedFrame = new UIFrame(x, y, width, height, pressedFrameSpriteImageID);
    }

    private void setTextPositions()
    {
        //Calculates the position to draw the strings, so that it would be at the center of the button
        Rect textBounds = new Rect();
        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.getTextBounds(text, 0, text.length(), textBounds);
        textX = (width/2) - (textBounds.width()/2);
        textY = (height/2) + (textBounds.height()/2);
    }

    /**
     * Sets the text size shown manually
     * @param _textSize    new text size
     */
    public void setTextSize(float _textSize)
    {
        textSize = _textSize;
        setTextPositions();
    }

    /**
     * Sets the text size programmatically and automatically
     * @param xPadding    Spacing on both sides of the frame for the text
     * @param yPadding    Spacing on the top and bottom of the frame for the text.
     */
    public void setTextSizeAuto(float xPadding, float yPadding)
    {
        float textSize1 = UIUtilities.findTextSizeByWidth(text, width - (2 * xPadding));
        float textSize2 = UIUtilities.findTextSizeByHeight(text, height - (2 * yPadding));

        if (textSize1 < textSize2)
            textSize = textSize1;
        else textSize = textSize2;

        setTextPositions();
    }

    /**
     * Sets the text color
     * @param _textColor    new text color
     */
    public void setTextColor(int _textColor)
    {
        textColor = _textColor;
    }

    public void setText(String _text)
    {
        text = _text;
        setTextPositions();
    }

    /**
     * Sets is the UIButtonFrame is selected
     * @param value    selected value
     */
    public void setSelected(boolean value)
    {
        selected = value;
    }

    /**
     * Sets the scale of the sprite image
     * @param value    scale value, floating-point value
     */
    public void setScale(float value)
    {
        pressedFrame.setScale(value);
        unpressedFrame.setScale(value);

        if (disabledOverlay != null)
            disabledOverlay.setScale(value);
    }

    /**
     * Getter method to check if the UIButtonFrame is enabled.
     * @return enabled, boolean
     */
    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * Getter method to check if the UIButtonFrame is pressed.
     * If pressed, it will set pressed to false, then return true.
     * @return pressed boolean value
     */
    public boolean isPressed()
    {
        if (pressed) {
            pressed = false;
            return true;
        }
        return false;
    }

    /**
     * Setter method for setting if the UIButtonFrame is enabled
     * @param enabled    new enabled boolean value
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Sets the text shown for UIButtonFrame
     * @return new set text
     */
    public String getText() {return text;}

    /**
     * Method for changing the position of the frame.
     * @param changeInX    relative change in x
     * @param changeInY    relative change in y
     * @see GameEntity
     */
    @Override
    public void changePosition(float changeInX, float changeInY) {
        super.changePosition(changeInX, changeInY);

        pressedFrame.changePosition(changeInX, changeInY);
        unpressedFrame.changePosition(changeInX, changeInY);

        if (disabledOverlay != null)
            disabledOverlay.changePosition(changeInX, changeInY);
    }

    /**
     * Method for setting the position of the frame
     * @param _x    x coordinates as a floating-point value
     * @param _y    y coordinates as a floating-point value
     * @see GameEntity
     */
    @Override
    public void setPosition(float _x, float _y) {
        super.setPosition(_x, _y);

        pressedFrame.setPosition(x, y);
        unpressedFrame.setPosition(x, y);

        if (disabledOverlay != null)
            disabledOverlay.setPosition(x, y);
    }

    /**
     * method for setting the size of the frame
     * @param _width Sets the width of object
     * @param _height Sets the height of object
     * @see GameEntity
     */
    @Override
    public void setSize(float _width, float _height) {
        super.setSize(_width, _height);
        pressedFrame.setSize(width, height);
        unpressedFrame.setSize(width, height);

        if (disabledOverlay != null)
            disabledOverlay.setSize(width, height);
    }

    /**
     * Update method, does nothing.
     * @param delta The time elapsed since the last update cycle#
     */
    @Override
    public void update(float delta) {

    }

    /**
     * Render method. Draws a pressed sprite if the frame is pressed,
     * draws unpressed sprite otherwise
     * Disabled overlay sprite is also drawn if disabled and the sprite is specifies
     * in the constructor.
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas) {
        if (selected)
            pressedFrame.render(canvas);
        else unpressedFrame.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(text, x + textX, y + textY, GlobalVariables.paint);

        if (!enabled)
            if (disabledOverlay != null)
                disabledOverlay.render(canvas);
    }

    /**
     * User input method. Checks if the user has pressed the UIButtonFrame or not.
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates collides with this object
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                if (enabled)
                    if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                    {

                        SoundManager.playSound(R.raw.ui_press);
                        selected = true;
                        return true;
                    }
                break;
            case MotionEvent.ACTION_UP:
                if (selected)
                    if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                    {
                        pressed = true;
                        selected = false;
                        return true;
                    }
                break;
            case MotionEvent.ACTION_MOVE:
                if (selected)
                    if (!Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                        selected = false;
                break;
        }
        return false;
    }
}
