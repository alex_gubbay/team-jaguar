package com.example.nightcrysis.sunka.FileIO;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.LinkedList;

/**
 * Class through witch all communication with AysncTasks of client and Server occour. Acts as
 * communication point between the the network tasks and the game logic.
 * Created by Alex on 06/11/2015.
 *
 */

public class DataTransmissionManager {
    static private Context context;
    static private Activity activity;
    static private LinkedList<String> transmissionQueue = new LinkedList<>();
    static private LinkedList<String> receivedQueue = new LinkedList<>();
    static private boolean stop = false;
    static private String ip;
    static private int port;
    static private boolean connected;
    static public int sendCounter = 0;
    static public int receiveCounter = 0;
    static private server s1;
    static private client c1;

    /**
     * Method for getting the IP address of the device
     * @return device IP String
     */
    public static String getIp() {

        try {
            //gets all network interfaces of the device.
            Enumeration<NetworkInterface> Interfaces = NetworkInterface.getNetworkInterfaces();
            while (Interfaces.hasMoreElements()) {

                //gets every address of every interface
                NetworkInterface currentInterface = Interfaces.nextElement();
                Enumeration<InetAddress> IpAddresses = currentInterface.getInetAddresses();

                while (IpAddresses.hasMoreElements()) {
                    //gets current address, makes sure its not a loopback address (i.e 127.0.0.1
                    // and that it's an ipv4. This makes it the local IP of the device.

                    InetAddress currentIP = IpAddresses.nextElement();
                    if (!currentIP.isLoopbackAddress() && currentIP.getAddress().length == 4) {

                        //if address is valid, add it to the DataTransmissionManager
                        ip = currentIP.getHostAddress();
                        break;
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("ERROR", e.toString());
        }
        return ip;
    }

    /**
     * Sets the class variable to the IP
     * @param ip to set
     */
    public static void setIp(String ip) {
        DataTransmissionManager.ip = ip;
    }

    /**
     * Get port that device is listening on
     * @return port number
     */
    public static int getPort() {
        return port;
    }

    /**
     * Used to set port from server allocation.
     * @param port port number to set.
     */
    public static void setPort(int port) {
        DataTransmissionManager.port = port;
        Log.d("RUN", " " + port + " Passed into data manager");
    }

    /**
     * Prints string value to console.
     * @param result to print
     */
    public static void printDataFromSocket(String result) {
        Log.d("NETWORK", result);
    }

    /**
     * Adds message to transmission queue to send to remote device.
     * @param message String to send to remote device
     */
    public static void queueMessageForSending(String message) {
        transmissionQueue.add(message);
    }

    /**
     * Get all messages currently queued for sending.
     * @return linkedList of all messages currently queued for sending.
     */
    public static LinkedList<String> getMessagesToSend() {
        return transmissionQueue;
    }

    /**
     * Get all messages recieved from remote device
     * @return LinkedList of all messages received from remote device.
     */
    public static LinkedList<String> getReceivedMessages() {
        return receivedQueue;
    }

    /**
     * Add message to queue of messages recieved from remote.
     * @param message message to add to list of received messages.
     */
    public static void receiveMessage(String message) {
        if(message.equals("CLOSE")){
            DataTransmissionManager.stopConnection();
        }
        receivedQueue.add(message);
        ++sendCounter;
        Log.d("MESSAGE", "MESSAGE ADDED " + message);
    }

    /**
     * Returns the most recently received message from remote device.
     * @return most recently received message from the remote device
     */
    public static String getLastMessage()
    {
        for(String message : receivedQueue)
        {
            Log.e("move",message);
        }
        return receivedQueue.getLast();
    }

    /**
     * Used to increment integrity checker
     */
    public static void updateReceiveCounter()
    {
        ++receiveCounter;
    }

    /**
     * Checks if the remote socket connection is due to close.
     * @return boolean connection close status.
     */
    public static boolean connectionShouldClose() {
        return stop;
    }

    /**
     * Set the status of the connection close instruction.
     */
    public static void stopConnection() {
        DataTransmissionManager.queueMessageForSending("STOP");
        stop = true;
    }
    public static void destroyObjects(){

        s1=null;
        c1=null;
    }

    /**
     * Creates a new AsyncTask for the server, which will dynamically assign itself to a port to
     * listen on,that is free and available for use. The server will automatically
     * update the port variable in this class, which can be used to point a client to the right port.
     */
    public static void openServer() {
        s1 = new server();
    }

    /**
     * Creates a new client AysncTask, which will try to connect to a server on the IP and port
     * specified.
     * @param ip IP of device running the server.
     * @param port Port that server device is listening on.
     */
    public static void openClient(String ip, int port) {
        Log.e("Client", "A " + ip + " " + port);
         c1 = new client(ip, port);
        Log.e("Client", "B created");
    }

    /**
     * Check if the connection between the client and server has been established. This will become
     * true on both devices when the connection is made and the readers/inupts have been
     * successfully established. When true, connection is ready for use.
     * @return
     */
    public static boolean connected() {
        return connected;
    }

    /**
     * Method used by the client and the server to signal that the connection is made and ready
     * for use.
     * @param connected
     */
    public static void setConnected(boolean connected) {
        DataTransmissionManager.connected = connected;
    }


}
