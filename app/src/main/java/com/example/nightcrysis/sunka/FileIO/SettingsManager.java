package com.example.nightcrysis.sunka.FileIO;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.nightcrysis.sunka.GlobalVariables;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * File saving class for settings. Stores user settings in SharedPreferences
 * with the key "settings".
 */
public class SettingsManager {
    private SharedPreferences settings;
    private Set<String> settingsValues;
    private static final String PREFS_NAME = "gameSettings", key = "settings";
    private boolean soundOn, musicOn, animationOn, hintsOn;
    private int soundVolume, musicVolume;

    /**
     * Constructor for SettingsManager.
     * Immediately loads and initializes settings upon call.
     */
    public SettingsManager()
    {
        settings = GlobalVariables.currentActivity.getSharedPreferences(PREFS_NAME, 0);

        loadSettings();
    }

    /**
     * Getter method for soundOn
     * @return soundOn
     */
    public boolean getSoundOn()
    {
        return  soundOn;
    }

    /**
     * Getter method for soundVolume
     * @return soundVolume
     */
    public int getSoundVolume()
    {
        return soundVolume;
    }

    /**
     * Getter method for musicOn
     * @return musicOn
     */
    public boolean getMusicOn()
    {
        return  musicOn;
    }

    /**
     * Getter method for musicVolume
     * @return musicVolume
     */
    public int getMusicVolume()
    {
        return  musicVolume;
    }

    /**
     * Getter method for animationOn (Fancy animation On)
     * @return animationOn
     */
    public boolean getAnimationOn()
    {
        return  animationOn;
    }

    /**
     * Getter method for hintsOn (currently not used any where)
     * @return hintsOn
     */
    public boolean getHintsOn()
    {
        return hintsOn;
    }

    /**
     * Method for removing settings
     */
    private void removeSettings()
    {
        //Remove settings.
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Method for loading settings.
     */
    private void loadSettings()
    {
        settingsValues = settings.getStringSet(key, null);

        //First time in game
        if (settingsValues == null)
        {
            saveSettings(true, 80, true, 60, true, true); //Default settings
            settingsValues = settings.getStringSet(key, null);
        }

        //The following loop only occur once
        for (String s : settingsValues)
        {
            String values[]  = s.split(",");

            //Looping over every settings
            for (String s2 : values)
            {
                String subValues[] = s2.split(" ");

                switch (Integer.parseInt(subValues[0]))
                {
                    case 1: soundOn = Boolean.parseBoolean(subValues[1]); break;
                    case 2: soundVolume = Integer.parseInt(subValues[1]); break;
                    case 3: musicOn = Boolean.parseBoolean(subValues[1]); break;
                    case 4: musicVolume = Integer.parseInt(subValues[1]); break;
                    case 5: animationOn = Boolean.parseBoolean(subValues[1]); break;
                    case 6: hintsOn = Boolean.parseBoolean(subValues[1]); break;

                }
            }
        }
    }

    /** Saves game settings by passing the new values in the parameter.
     * @param _soundOn boolean value
     * @param _soundVolume integer value
     * @param _musicOn boolean value
     * @param _musicVolume integer value
     * @param _animationOn boolean value
     * @param _hintsOn boolean value
     * */
    public void saveSettings(boolean _soundOn, int _soundVolume, boolean _musicOn, int _musicVolume, boolean _animationOn, boolean _hintsOn)
    {
        soundOn = _soundOn;
        soundVolume = _soundVolume;
        musicOn = _musicOn;
        musicVolume = _musicVolume;
        animationOn = _animationOn;
        hintsOn = _hintsOn;

        SharedPreferences.Editor editor = settings.edit();
        Set<String> settingsMap = new HashSet<>();

        //Adding counters to each string to show that they are different values.
        settingsMap.add(1 + " " + String.valueOf(_soundOn));
        settingsMap.add(2 + " " + String.valueOf(_soundVolume));
        settingsMap.add(3 + " " + String.valueOf(_musicOn));
        settingsMap.add(4 + " " + String.valueOf(_musicVolume));
        settingsMap.add(5 + " " + String.valueOf(_animationOn));
        settingsMap.add(6 + " " + String.valueOf(_hintsOn));

        editor.remove(key);
        editor.putStringSet(key, settingsMap);
        editor.commit();
    }
}
