package com.example.nightcrysis.sunka.GameEntity;

import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Graphics.Sprite;

/**
 * Created by NIghtCrysIs on 2015/11/08
 * Class for entities that only needs a Sprite, and needs to utilize the functionality of GameEntity.
 * Sprites created are stretched to match the given width and height of the entity.
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class SpriteEntity extends GameEntity{
    /**
     * Constructor for SpriteEntity.
     * @param _x x position on screen
     * @param _y y position on screen
     * @param _width width (in pixels) of entity
     * @param _height width (in pixels) of entity
     * @param spriteID The resource id of the sprite image
     * */
    public SpriteEntity(float _x, float _y, float _width, float _height, int spriteID)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        sprite = new Sprite(x, y, width, height, spriteID);
    }

    /**
     * Update method. Sets the sprite's position if there are any x/y changes.
     * @param delta The time elapsed since the last update cycle#
     */
    @Override
    public void update(float delta) {
        sprite.setPosition(x, y);
    }

    /**
     * Method for setting needsRemoval to true, used for ArrayList iterations in the application
     */
    public void remove()
    {
        needsRemoval = true;
    }

    /**
     * Getter method for needsRemoval
     * @param event The MotionEvent that the device has detected
     * @return false because nothing needs to be done
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
