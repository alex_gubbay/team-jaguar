package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * Class for the SoundButton that is seen in the main menu.
 * A custom class needed for buttons that changes sprites.
 * When soundOff button is shown, it means both sound and music off, vice versa
 */
public class SoundButton extends GameEntity {
    private UIButton soundOn, soundOff;
    private boolean on = true;

    /** Constructor for SoundButton
     * @param _x         sets the x position of object
     * @param _y         sets the y position of object
     * @param _width     sets the width of object
     * @param _height    sets the height of object
     * */
    public SoundButton(float _x, float _y, float _width, float _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        soundOn = new UIButton(_x, _y, _width, _height, R.drawable.volume_on, R.drawable.volume_on_pressed);
        soundOff = new UIButton(_x, _y, _width, _height, R.drawable.volume_off, R.drawable.volume_off_pressed);
        on = GameSettings.soundOn && GameSettings.musicOn;
    }

    /**
     * Method for setting the position of the button
     * @param _x    x coordinates as a floating-point value
     * @param _y    y coordinates as a floating-point value
     * @see GameEntity
     */
    @Override
    public void setPosition(float _x, float _y) {
        x = _x;
        y = _y;
        soundOn.setPosition(_x, _y);
        soundOff.setPosition(_x, _y);
    }

    /**
     * Method for changing the position of the button.
     * @param changeInX    relative change in x
     * @param changeInY    relative change in y
     * @see GameEntity
     */
    @Override
    public void changePosition(float changeInX, float changeInY) {
        x += changeInX;
        y += changeInY;
        soundOn.changePosition(changeInX, changeInY);
        soundOff.changePosition(changeInX, changeInY);
    }

    /**
     * method for setting the size of the button. The sprite is stretched accordingly
     * @param _width Sets the width of object
     * @param _height Sets the height of object
     * @see GameEntity
     */
    @Override
    public void setSize(float _width, float _height) {
        soundOn.setSize(_width, _height);
        soundOff.setSize(_width, _height);
    }

    /**
     * Update method, Updates the sound button
     * Also sets the on value to if either musicOn is true or if soundOn is true
     * @param delta The time elapsed since the last update cycle#
     */
    @Override
    public void update(float delta) {
        on = (GameSettings.musicOn | GameSettings.soundOn);

        soundOn.update(delta);
        soundOff.update(delta);
    }

    /**
     * Render method. Draws different sprites depending on if the all sounds are off
     * or partially on.
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas) {
        if (!isVisible) return;
        if (on)
            soundOn.render(canvas);
        else soundOff.render(canvas);
    }

    /**
     * User input method. Checks if the user has pressed the SoundButton or not.
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates collides with this object
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (on)
        {
            if (soundOn.onTouchEvent(event))
                if (soundOn.isPressed())
                {
                    GameSettings.soundOn = false;
                    GameSettings.musicOn = false;
                    GameSettings.saveSettings();
                    SoundManager.toggleSound(false);
                    SoundManager.toggleMusic(false);
                    on = false;
                    return true;
                }
        }
        else if (soundOff.onTouchEvent(event))
        {
            if (soundOff.isPressed())
            {
                GameSettings.soundOn = true;
                GameSettings.musicOn = true;
                GameSettings.saveSettings();
                SoundManager.toggleSound(true);
                SoundManager.toggleMusic(true);
                on = true;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean getNeedsRemoval() {
        return false;
    }

    public void setEnabled(boolean enabled) {
        soundOn.setEnabled(enabled);
        soundOff.setEnabled(enabled);
    }
}
