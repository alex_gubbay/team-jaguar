package com.example.nightcrysis.sunka.Graphics;

/**
 * Created by NIghtCrysIs on 2015/10/21.
 * Class that represents a Frame, You will specify the frame's position in the image file in pixels
 * The purpose of this is allowing greater flexibility in create Animation, and to stop the
 * assumption that the entire image area is used.
 * It makes the reusing of the same frames, merging multiple animations into one image file possible
 * @see Animation
 * @see com.example.nightcrysis.sunka.GameEntity.AnimationEntity
 */
public class Frame {
    public int x = 0, y = 0;

    /**
     * Default constructor for a Frame object.
     */
    public Frame()
    {

    }

    /**
     * Constructor for Frame with x and y coordinates passed as parameters
     * @param _x    x coordinate of a frame
     * @param _y    y coordinate of a frame
     */
    public Frame(int _x, int _y)
    {
        x = _x;
        y = _y;
    }
}
