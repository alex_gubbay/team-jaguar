package com.example.nightcrysis.sunka;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Custom View that displays everything on screen for the game.
 * onDraw method is overridden for custom drawing calls.
 */
public class GameDisplay extends View{
    /**
     * Constructor for GameDisplay (Main constructor used)
     * @param context    The current context of the application
     */
    public GameDisplay(Context context) {
        super(context);
    }

    /**
     * Second constructor for GameDisplay
     * @param context    The current context of the application
     * @param attrs      Set attributes for the view
     */
    public GameDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Third  constructor for GameDisplay
     * @param context    The current context of the application
     * @param attrs      Set attributes for the view
     * @param defStyleAttr Sets default style attribute
     */
    public GameDisplay(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /** Render method - Gets a canvas and passes it on to GameManager to start drawing
     * Invalidates the view so that onDraw will be called. */
    public void render()
    {
        //invalidate();
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Calls ScreenManager.render to draw everything on screen.
        ScreenManager.render(canvas);
        //canvas.restore();
    }
}
