package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Specialized class used to control the acceleration of an instance of GameEntityInterface
 * programmatically.
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class AccelerationEvent implements GameEntityInterface {
    private GameEntity puppet;
    private boolean needsRemoval = false, removeAtEnd;
    private float destinationX, destinationY, velocityX, velocityY, accelX, accelY, duration;

    /**
     * @param _duration    Sets the duration of how long the moving should take
     * @param _puppet      Sets the object that is wanted to be moved
     * @param _velocityX   sets the initial velocity in the x direction (per second)
     * @param _velocityY   sets the initial velocity in the y direction (per second)
     * @param _accelX       sets the acceleration in the x direction (per second)
     * @param _accelY       sets the acceleration in the y direction (per second)
     * @param _removeAtEnd Sets when at the end of movement duration, the puppet should be removed (set needsRemoval to true)
     * */
    public AccelerationEvent(GameEntity _puppet, float _velocityX, float _velocityY, float _accelX, float _accelY, float _duration, boolean _removeAtEnd)
    {
        removeAtEnd = _removeAtEnd;
        duration = _duration;
        puppet = _puppet;
        velocityX = _velocityX;
        velocityY = _velocityY;
        accelX = _accelX;
        accelY = _accelY;
    }

    /** Method used to calculate acceleration of any component moving off screen (UPWARDS).This returns the acceleration value
     * with regards to a specified y coordinates of the component that is to be accelerated,
     * the height of component, the initial velocity, and the time duration value.
     * What remains is a mathematical function formed using integration, basically SUVAT
     * @param yPos            Y position of component
     * @param totalHeight     Height of component
     * @param initVelocity    Initial velocity (downwards)
     * @param duration        Time duration of the transition
     * @return Acceleration value
     * */
    public static float accelYOffScreenValueUp(float yPos, float totalHeight, float initVelocity, float duration)
    {
        return (float)(((yPos + totalHeight) + (initVelocity * duration) - yPos) * 2 / Math.pow(duration, 2));
    }


    /* DOESN'T WORK
    /** Method used to calculate acceleration of any component moving in screen (FROM UPWARDS)
     * It returns the acceleration value given a given y-position, positive y offset and total duration
     * of the accelerated movement
     * @param duration           Total duration of accelerated movement event
     * @param positiveYOffset   Y offset from the top of the screen (denotes the final y position of the component)
     * @param totalHeight       Total height of the component
     * @param yPos               Current y position of the component
     * @return Acceleration value
     *
    public static float accelYOnScreenValueFromUp(float yPos, float totalHeight, float positiveYOffset, float duration)
    {
        return (float)((2 * (positiveYOffset + Math.abs(yPos) - (getOnScreenInitVelocity(yPos, positiveYOffset, duration) * duration))) / Math.pow(duration, 2));
    }

    /** Method that calculates an initial velocity value relative to the component's displacement
     * and positive Y offset
     * @param positiveYOffset   Y offset from the top of the screen (denotes the final y position of the component)
     * @param yPos               Current y position of the component
     * @return floating-point value for initial velocity of component
    public static float getOnScreenInitVelocity(float yPos, float positiveYOffset, float duration)
    {
        return (Math.abs(yPos) + positiveYOffset) * 2 / duration;
    }*/

    /**
     * Update method that changes the size of the object, and its position to match the center
     * coordinates of the object respectively
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        puppet.changePosition(velocityX * delta, velocityY * delta);
        velocityX += accelX * delta;
        velocityY += accelY * delta;
        duration -= delta;
        if (duration < 0)
        {
            if (removeAtEnd)
                puppet.remove();
            needsRemoval = true;
        }
    }

    /**
     * Drawing method. Currently does nothing
     * @param canvas Canvas object used to draw images on screen
     */
    @Override
    public void render(Canvas canvas) {

    }

    /**
     * Method for detecting user inputs, currently does nothing
     * @param event The MotionEvent that the device has detected
     * @return always false
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for getting needsRemoval
     * @return boolean value if the object needs to be removed
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
