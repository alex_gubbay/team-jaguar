package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Specialized class used to control the size of an instance of GameEntityInterface
 * programmatically.
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class SizeEvent implements GameEntityInterface {
    private GameEntity puppet;
    private boolean removeAtEnd, needsRemoval = false;
    private float duration, centerX, centerY, originalWidth, originalHeight,
                    currentWidth, currentHeight, changeInWidth, changeInHeight,
                    finalWidth, finalHeight;

    /** Constructor of making GameEntities popping in and out
     * @param _puppet    sets the GameEntity to be controlled
     * @param _isPopIn    sets if the entity is to enter the screen or not. If true then the
     *                   puppet will expand from its center. If false it will shrink towards its center
     * @see GameEntity
     * */
    public SizeEvent(GameEntity _puppet, float _duration, boolean _isPopIn, boolean _removeAtEnd)
    {
        puppet = _puppet;
        removeAtEnd = _removeAtEnd;
        duration = _duration;

        originalWidth = puppet.getWidth();
        originalHeight = puppet.getHeight();
        centerX = puppet.getX() + originalWidth / 2;
        centerY = puppet.getY() + originalHeight / 2;

        if (_isPopIn)
        {
            puppet.setSize(0, 0);
            currentWidth = 0;
            currentHeight = 0;
            changeInWidth = originalWidth * 1.05f / _duration;
            changeInHeight = originalHeight * 1.05f / _duration;
            finalWidth = originalWidth;
            finalHeight = originalHeight;
        }
        else
        {
            puppet.setSize(originalWidth * 1.05f, originalHeight * 1.05f);
            currentWidth = originalWidth;
            currentHeight = originalHeight;
            changeInWidth = - originalWidth * 1.05f / _duration;
            changeInHeight = - originalHeight * 1.05f / _duration;
            finalWidth = 0;
            finalHeight = 0;
        }
    }

    /** Constructor of change entities to a certain size given a duration and specified size
    * @param _puppet       The GameEntity being affected
     * @param _duration    The duration of event
     * @param endHeight    The height of GameEntity when event ends
     * @param endWidth     The width of GameEntity when event ends
     * @see GameEntity
     * */
    public SizeEvent(GameEntity _puppet, float _duration, float endWidth, float endHeight, boolean _removeAtEnd)
    {
        puppet = _puppet;
        duration = _duration;
        removeAtEnd = _removeAtEnd;

        originalWidth = puppet.getWidth();
        originalHeight = puppet.getHeight();

        finalWidth = endWidth;
        finalHeight = endHeight;

        centerX = puppet.getX() + originalWidth / 2;
        centerY = puppet.getY() + originalHeight / 2;
        changeInWidth = (finalWidth - originalWidth) / duration;
        changeInHeight  = (finalHeight - originalHeight) / duration;

    }

    /**
     * Updates the duration of the SizeEvent, and changes the size of the controlled object
     * respectively. When duration reaches <= 0, needsRemoval is set to true.
     * @param delta The time elapsed since the last update cycle
     * @see GameEntityInterface
     */
    @Override
    public void update(float delta) {
        if (duration <= 0)
        {
            if (removeAtEnd)
                puppet.remove();
            puppet.setSize(finalWidth, finalHeight);
            puppet.setPosition(centerX - originalWidth / 2, centerY - originalHeight / 2);
            needsRemoval = true;
            return;
        }
        duration -= delta;
        currentWidth += changeInWidth * delta;
        currentHeight += changeInHeight * delta;
        puppet.setSize(currentWidth, currentHeight);
        puppet.setPosition(centerX - currentWidth/2, centerY - currentHeight/2);

    }

    /**
     * Rendering method. Does nothing
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {

    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
