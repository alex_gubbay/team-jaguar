package com.example.nightcrysis.sunka;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Screen.MainBackground;
import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * MainActivity of the game, the start of the game framework.
 * The sole one and only Activity used for the entire game. UI implementations are
 * ocmpletely customized
 * @author NIghtCrysIs
 * @see Activity
 */
public class MainActivity extends Activity {

    /**
     * OnCreate method. Called when the application first starts,
     * or resumed from after onStop() is called.
     * @param savedInstanceState
     * @see Activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GlobalVariables.gameDisplay = (GameDisplay) findViewById(R.id.gameDisplay);

        // The following gets the screen resolution of any android device
        Display display = getWindowManager().getDefaultDisplay();
        Point p  = new Point();
        display.getSize(p);
        //Sets the screen size in globalVariables for screen-relative sizing and position of objects
        GlobalVariables.screenSizeX = p.x;
        GlobalVariables.screenSizeY = p.y;

        //Sets this Activity as the context (as well as the only activity context)
        GlobalVariables.currentActivity = this;

        //Setups gameDisplay variable in GlobalVariables
        //GlobalVariables.gameDisplay = new GameDisplay(this);
        //setContentView(GlobalVariables.gameDisplay);

        //Initializes other static classes (Ordering is important!)
        GameSettings.initialize();
        ImageManager.initialize();
        MainBackground.initialize(); //Sets screenRatio in GlobalVariables
        ScreenManager.initialize();
        GlobalVariables.initialize(); //Sets font typeface

        //Starts the update cycle
        GlobalVariables.updateCycle = new UpdateCycle();
        GlobalVariables.updateCycle.start();
    }

    /**
     * @param menu
     * @return
     * @see Activity
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** Resumes update cycle when game is resumed */
    @Override
    protected void onResume() {
        super.onResume();
        if (GlobalVariables.updateCycle != null)
            GlobalVariables.updateCycle.onResume();

        if (GameSettings.musicOn)
            SoundManager.resumeBGM();
    }

    /**
     * Called if the application is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (GlobalVariables.updateCycle != null)
            GlobalVariables.updateCycle.onPause();

        if (GameSettings.musicOn)
            SoundManager.pauseBGM();
    }

    /**
     * Called when the application has stopped
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (GlobalVariables.updateCycle != null)
            GlobalVariables.updateCycle.onPause();

        if (GameSettings.musicOn)
            SoundManager.pauseBGM();
    }

    /**
     * onTouchEvent method for user input handling.
     * User input is passed to ScreenManager for further handling.
     * @param event
     * @return
     * @see Activity
     * @see ScreenManager
     */
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        ScreenManager.onTouchEvent(event);
        return super.onTouchEvent(event);
    }
}
