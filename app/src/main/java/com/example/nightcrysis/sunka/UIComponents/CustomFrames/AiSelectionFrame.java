package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;

/**
 * Created by NIghtCrysIs on 2015/11/11.
 * Frame for selecting game difficulty, in which in turn selects the corresponding ai for AiGameScreen
 */
public class AiSelectionFrame extends UIFrameWithOverlay{
    private SpriteEntity title;
    private UIButtonFrame easyAiButton, mediumAiButton, hardAiButton, backButton;
    private boolean frameClosing = false;

    /**
     * Constructor for AiSelectionFrame. Sets the positions and size of the object
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Frame sprite resource ID
     */
    public AiSelectionFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        initialize();
    }

    /**
     * Constructor for AiSelectionFrame. Sets the positions and size of the object.
     * Has an extra parameter for the side paddings of the frame
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Frame sprite resource ID
     * @param frameSidePadding Frame side paddings
     * @deprecated FrameSidePadding is not used
     */
    public AiSelectionFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }

    /** Initializes variables for the frame */
    private void initialize()
    {
        float yOffset = y;
        y -=  y + height; //Move frame off screen for transition

        //Initialize variables
        float tempRatio = width * 0.85f / ImageManager.getBitmapWidth(R.drawable.title_select_difficulty),
                tempWidth = ImageManager.getBitmapWidth(R.drawable.title_select_difficulty) * tempRatio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.title_select_difficulty) * tempRatio;


        title = new SpriteEntity(x + (width / 2) - (tempWidth / 2), y - (tempHeight / 2),
                tempWidth, tempHeight, R.drawable.title_select_difficulty);

        tempHeight = (height - title.getHeight()/2) / 4;
        float yPadding = tempHeight/4;
        tempHeight -= yPadding;
        tempWidth = width - (2 * yPadding);

        int colorValue = Color.argb(255, 246, 176, 57);

        easyAiButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), title.getY() + title.getHeight(),
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Easy");
        easyAiButton.setTextColor(colorValue);
        easyAiButton.setTextSizeAuto(0, easyAiButton.getHeight() / 4);

        mediumAiButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), easyAiButton.getY() + easyAiButton.getHeight() + yPadding,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Medium");
        mediumAiButton.setTextColor(colorValue);
        mediumAiButton.setTextSizeAuto(0, mediumAiButton.getHeight() / 4);

        hardAiButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), mediumAiButton.getY() + mediumAiButton.getHeight() + yPadding,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Hard");
        hardAiButton.setTextColor(colorValue);
        hardAiButton.setTextSizeAuto(0, hardAiButton.getHeight() / 4);

        backButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), hardAiButton.getY() + hardAiButton.getHeight() + yPadding,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Back");
        backButton.setTextColor(colorValue);
        backButton.setTextSizeAuto(0, backButton.getHeight() / 4);

        uiSubComponents.add(title);
        uiSubComponents.add(backButton);
        uiSubComponents.add(easyAiButton);
        uiSubComponents.add(mediumAiButton);
        uiSubComponents.add(hardAiButton);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, height + yOffset, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(title, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(easyAiButton, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(mediumAiButton, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(hardAiButton, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(backButton, 0, height + yOffset, 0.5f, true, false));
    }

    /** Moves all components related to the frame upwards outside the screen */
    private void changeScreen()
    {
        //Accelerate everything upwards :D
        float initialVelocityY = height / 2,
                accelerationValue = -AccelerationEvent.accelYOffScreenValueUp(y, height, initialVelocityY, 0.5f);

        //Setting the buttons to be disabled to prevent further inputs
        backButton.setEnabled(false);
        easyAiButton.setEnabled(false);
        mediumAiButton.setEnabled(false);
        hardAiButton.setEnabled(false);

        uiSubComponents.add(new AccelerationEvent(this, 0, initialVelocityY, 0, accelerationValue, 0.5f, true)); //0.5f is the overlay duration
        uiSubComponents.add(new AccelerationEvent(title, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(backButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(easyAiButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(mediumAiButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(hardAiButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
    }

    /**
     * Updates the AiSelectionFrame
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        super.update(delta);
    }

    /**
     * Renders the AiSelectionFrame
     * @param canvas
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);
    }

    /**
     * User input method. Passes user input to its buttons etc.
     * @param event The MotionEvent that the device has detected
     * @return true if the user input coordinates are within the frame,
     * false if otherwise.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If the frame is closing, ignore the touches and return true
        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (backButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;
        }
        else if (easyAiButton.isPressed())
        {
            ScreenManager.fadeOutTo(ScreenState.aiGameEasy);
            frameClosed = true;
        }
        else if (mediumAiButton.isPressed())
        {
            ScreenManager.fadeOutTo(ScreenState.aiGameMedium);
            frameClosed = true;
        }
        else if (hardAiButton.isPressed())
        {
            ScreenManager.fadeOutTo(ScreenState.aiGameHard);
            frameClosed = true;
        }
        if (frameClosed)
        {
            changeScreen();
        }

        return true;
    }
}
