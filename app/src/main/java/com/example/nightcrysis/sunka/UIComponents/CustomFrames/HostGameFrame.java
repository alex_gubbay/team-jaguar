package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataTransmissionManager;
import com.example.nightcrysis.sunka.GameEntity.AnimationEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.Frame;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

import java.util.LinkedList;

/**
 * Created by NIghtCrysIs on 2015/11/11.
 * Specialized UIFrameWithOverlay class for hosting the game
 * Provides corresponding textbox and buttons for user to interact with.
 * Shows the host address and port
 */
public class HostGameFrame extends UIFrameWithOverlay {
    private UIButtonFrame abortButton, playerAddress, playerPort;
    private boolean frameClosing = false;
    private volatile boolean peerConnected = false;
    private static float connectionFontSize = -1, connectionFontYOffset = -1;
    private AnimationEntity connectingAnimation;

    /* Custom thread for hosting, to prevent the game from freezing up if the user wants to abort */
    private Thread hostingThread = new Thread(){
        @Override
        public void run() {
            LinkedList<String> messages = DataTransmissionManager.getReceivedMessages();

            while(messages.isEmpty())
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch(InterruptedException e)
                {
                    e.printStackTrace();
                    Log.e("HostGameFrame", "Exception occurred during Thread.sleep()");
                    ScreenManager.addToMasterOverlay(new MessageFrame("Host game has been interrupted."));
                    return;
                }
            }
            String lastReceivedMessage = messages.getLast();

            while(!lastReceivedMessage.equals("Client is ready"))
            {
                try
                {
                    Thread.sleep(1000);
                }
                catch(InterruptedException e)
                {
                    e.printStackTrace();
                    Log.e("HostGameFrame", "Exception occurred during Thread.sleep()");
                    ScreenManager.addToMasterOverlay(new MessageFrame("Host game has been interrupted."));
                    return;
                }
                lastReceivedMessage = DataTransmissionManager.getReceivedMessages().getLast();
            }

            peerConnected = true;

            //Transitioning to online multiplayer game
            EventController screenTransitionEvent = new EventController();
            screenTransitionEvent.addEvent(new ColorFade(true, 0.5f, false, true));
            screenTransitionEvent.addEvent(new ChangeScreenTimer(0, ScreenState.onlineMultiplayerGame));
            screenTransitionEvent.addEvent(new ColorFade(false, 0.5f, false, true));
            ScreenManager.addToMasterOverlay(screenTransitionEvent);
        }
    };

    /**
     * Constructor for HostGameFrame. Sets the position and size,as well as instantiating
     * related objects and sets its variables.
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite resource ID
     */
    public HostGameFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        DataTransmissionManager.openServer();
        initialize();
    }

    /**
     * Constructor for HostGameFrame. Sets the position and size,as well as instantiating
     * related objects and sets its variables.
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite resource ID
     * @param frameSidePadding spacing at the sides of the frame.
     * @deprecated frameSidePadding never used
     */
    public HostGameFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }

    private void initialize()
    {
        DataTransmissionManager.openServer();
        hostingThread.start();

        float yOffset = y;
        y =  -height; //Move frame off screen for transition

        //Initialize variables
        float tempWidth =  width / 5,
                tempHeight = height / 10;

        int colorValue = Color.argb(255, 246, 176, 57);

        playerAddress = new UIButtonFrame(x + (width / 8), y + (tempHeight),
                width * 6 / 8, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        playerAddress.setText("Your ip address: " +  DataTransmissionManager.getIp());
        playerAddress.setTextColor(colorValue);
        playerAddress.setTextSizeAuto(0, playerAddress.getHeight() / 4);
        playerAddress.setEnabled(false);

        playerPort = new UIButtonFrame(x + (width / 8), y + (tempHeight * 2.5f),
                width * 6 / 8, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        playerPort.setText("Your port: " +  DataTransmissionManager.getPort());
        playerPort.setTextColor(colorValue);
        playerPort.setTextSizeAuto(0, playerPort.getHeight() / 4);
        playerPort.setEnabled(false);

        abortButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), y + height - (tileHeight),
                tempWidth, tileHeight/2, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Abort");
        abortButton.setTextColor(Color.RED);
        abortButton.setTextSizeAuto(0, abortButton.getHeight() / 4);

        int frameWidth = ImageManager.getBitmapWidth(R.drawable.loading_animation) / 4,
        frameHeight = ImageManager.getBitmapHeight(R.drawable.loading_animation) / 2;
        Frame[] frames = new Frame[8];
        frames[0] = new Frame(0, 0);
        frames[1] = new Frame(frameWidth, 0);
        frames[2] = new Frame(frameWidth * 2, 0);
        frames[3] = new Frame(frameWidth * 3, 0);
        frames[4] = new Frame(0, frameWidth);
        frames[5] = new Frame(frameWidth, frameWidth);
        frames[6] = new Frame(frameWidth * 2, frameWidth);
        frames[7] = new Frame(frameWidth * 3, frameWidth);
        connectingAnimation = new AnimationEntity(x + (width * 5 / 8), y + (height / 2), height / 5, height / 5, frameWidth, frameHeight, frames, 0.05f, R.drawable.loading_animation);

        // If connectionFontSize is still -1 (not initialized)
        if (connectionFontSize < 0)
        {
            //Calculate font size for connection text
            connectionFontSize = UIUtilities.findTextSizeByWidth("Waiting for peer...", width/3);
            Rect rect = new Rect();
            GlobalVariables.paint.setTextSize(connectionFontSize);
            GlobalVariables.paint.getTextBounds("Waiting for peer...", 0, 18, rect);
            connectionFontYOffset = rect.height() / 2;
        }

        uiSubComponents.add(playerAddress);
        uiSubComponents.add(playerPort);
        uiSubComponents.add(abortButton);
        uiSubComponents.add(connectingAnimation);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, yOffset + height, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(playerAddress, 0, yOffset + height, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(playerPort, 0, yOffset + height, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(abortButton, 0, yOffset + height, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(connectingAnimation, 0, yOffset + height, 0.5f, true, false));
    }

    /**
     * Updates the HostGameFrame
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        super.update(delta);

        //Check if incoming connection is true
        if (!peerConnected)
        {
            connectingAnimation.setVisible(false);
        }
        else
        {
            frameClosed = true;
            EventController events = new EventController();
            events.addEvent(new ColorFade(true, 0.5f, false, true));
            events.addEvent(new ChangeScreenTimer(0, ScreenState.onlineMultiplayerGame));
            events.addEvent(new ColorFade(true, 0.5f, false, true));
            ScreenManager.getCurrentScreen().addToOverlay(events);
        }
    }

    /**
     * Renders the HostGameFrame
     * @param canvas Canvas object used to draw images on screen
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        // Connection text
        GlobalVariables.paint.setColor(Color.WHITE);
        GlobalVariables.paint.setTextSize(connectionFontSize);
        if (!peerConnected)
            canvas.drawText("Waiting for peer...", connectingAnimation.getX() - (width / 2),
                    connectingAnimation.getY() + (connectingAnimation.getHeight()/2) - connectionFontYOffset ,GlobalVariables.paint);
        else
            canvas.drawText("Peer connected!", connectingAnimation.getX() - (width / 2),
                    connectingAnimation.getY() + (connectingAnimation.getHeight()/2) - connectionFontYOffset ,GlobalVariables.paint);
    }

    /**
     *
     * @param event The MotionEvent that the device has detected
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If the frame is closing, ignore the touches and return true
        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (abortButton.isPressed())
        {
            //Interrupt and stop server hosting
            if (hostingThread.isAlive())
            {
                hostingThread.interrupt();
            }

            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;

            ScreenManager.getCurrentScreen().addToOverlay(new MessageFrame("You have aborted hosting the game."));
        }
        if (frameClosed)
        {
            hostingThread.interrupt();
            DataTransmissionManager.stopConnection();
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f),
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(playerAddress, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(playerPort, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(connectingAnimation, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(abortButton, 0, velocityY, 0, acceleration, 0.5f, true));
        }

        return true;
    }
}
