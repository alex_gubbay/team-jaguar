package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GameEntity.Pebble;
import com.example.nightcrysis.sunka.GameEntity.Pot;
import com.example.nightcrysis.sunka.GameEntity.Tray;
import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;

import java.util.ArrayList;

/**
 * Created by NIghtCrysIs on 2015/11/14.
 *
 * Customized event for a pebbles movement. This method simulates a real player picking up
 * a pebble then move it to another Pot in a parabola. Due to the mathematical and
 * computational complexity of this task, the increase and decrease in size of the pebble
 * can be manually configured in the settings.
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class PebbleMoveEvent implements GameEntityInterface{
    private static final float moveSpeedPebbleRatio = 8; //distance = 5 times the pebble size (width)
    private boolean needsRemoval = false, isLastPebble;
    private Pebble pebble;
    private Pot pot;
    private ArrayList<GameEntity> pebbles;
    private Tray tray;

    private PositionEvent moveEvent;
    private TempSizeEvent tempSizeEvent;

    /**
     * Constructor for PebbleMoveEvent
     * @param targetPebble     The target pebble to be moved
     * @param targetPot        The target pot the targetPebble is moved to
     * @param _toX             The X coordinates that the target pebble will move towards
     * @param _toY             The Y coordinates that the target pebble will move towards
     * @param _pebbles         The pebbles array list from the Tray class, for adding the pebble back after
     *                         movement
     * @param _tray            The current tray class, for calling isLastPebbleAction if the current moving
     *                         pebble is the last pebble distributed
     * @param _isLastPebble    Boolean value to confirm if targetPebble is the last pebble distributed
     * @see Tray
     * @see com.example.nightcrysis.sunka.GameEntity.TrayAI
     * @see com.example.nightcrysis.sunka.GameEntity.TrayEasyAI
     * @see com.example.nightcrysis.sunka.GameEntity.TrayHardAI
     * @see com.example.nightcrysis.sunka.GameEntity.TrayOnline
     */
    public PebbleMoveEvent(Pebble targetPebble, Pot targetPot, float _toX, float _toY, ArrayList<GameEntity> _pebbles, Tray _tray, boolean _isLastPebble)
    {
        pebble = targetPebble;
        pot = targetPot;
        pebbles = _pebbles;
        isLastPebble = _isLastPebble;
        tray = _tray;

        //Calculate distance between pebble and target location
        //Basic calculus
        double distance = Math.sqrt(Math.pow(targetPebble.getX() - _toX, 2) + Math.pow(targetPebble.getY() - _toY, 2));

        //Then calculate the duration by division
        float duration = (float) (distance / (pebble.getWidth() * moveSpeedPebbleRatio));

        moveEvent = new PositionEvent(targetPebble, _toX, _toY, duration, false, false);

        //Move the pebbles with their max size ratio of 2 (* 10)
        if (GameSettings.fancyAnimationOn)
            tempSizeEvent = new TempSizeEvent(targetPebble, 20, duration);
    }

    /**
     * Update method. It updates the position and the size through the use of
     * SizeEvent and TempSizeEvent.
     * @param delta The time elapsed since the last update cycle
     * @see PositionEvent
     * @see TempSizeEvent
     * @see GameEntityInterface
     */
    @Override
    public void update(float delta) {
        if (needsRemoval) return;

        //If both movement and size changes has stopped
        if (moveEvent.getNeedsRemoval())
        {
            //Add the pebble in the respective pot
            pot.addPebble(pebble);
            pebbles.add(pebble);
            SoundManager.playSound(R.raw.gem_move);
            if (isLastPebble)
            {
                tray.lastPebbleAction(pebble, pot);
            }

            needsRemoval = true;
        }

        //Update all events
        pebble.update(delta);
        moveEvent.update(delta);

        if (tempSizeEvent != null)
            tempSizeEvent.update(delta);
    }

    /**
     * Rendering method, renders the moving pebble.
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {
        pebble.render(canvas);
    }

    /**
     * User input method. Does nothing.
     * @param event The MotionEvent that the device has detected
     * @return always false
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for returning needsRemoval
     * @return the boolean value of needsRemoval
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
