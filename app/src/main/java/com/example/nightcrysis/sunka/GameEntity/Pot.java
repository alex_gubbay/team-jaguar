package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;

import java.util.Stack;

/**
 * Created by tuffail on 21/10/15.
 * Abstract Pot class that is used to represent a Pot which will store the pebbles
 */
public abstract class Pot extends GameEntity {

    Player player;
    Pot nextPot;
    Stack<Pebble> pebbles = new Stack<Pebble>();
    Sprite highlightedSprite1, highlightedSprite2, highlightedSprite3, highlightedSprite4;
    boolean highlighted = false;
    boolean locked = false;
    boolean selected = false;
    int highlightCounter = 0;

    /**
     * Takes the top left coordinate of the pot along with its attributes
     * @param _x X coordinate of Pot
     * @param _y Y coordinate of the Pot
     * @param _width Width of the Pot
     * @param _height Height of the Pot
     */
    public Pot(float _x, float _y, float _width, float _height){
        x = _x;
        y = _y;
        width = _width;
        height = _height;

    }

    /**
     * Returns a collection of all the pebbles in the Pot
     * @return A stack of all the pebbles in the class
     */
    public Stack<Pebble> getPebbles(){
        return pebbles;
    }

    /**
     * Adds a pebble to the Pot
     * @param pebble The Pebble to be added to the Pot
     */
    public void addPebble(Pebble pebble){
        pebbles.push(pebble);
    }

    /**
     * Sets the player who this pot belongs to
     * @param _player Player who the this pot belongs to
     */
    public void setPlayer(Player _player){
        player = _player;
    }

    /**
     * Returns the play who the pot belongs to
     * @return Player who the pot belongs to
     */
    public Player getPlayer(){
        return player;
    }

    /**
     * Sets the reference to the next pot in the tray
     * @param _nextPot next pot in the tray from this pot
     */
    public void setNextPot(Pot _nextPot){
        nextPot = _nextPot;
    }

    /**
     * Returns the next pot in the tray
     * @return next pot in the tray
     */
    public Pot getNextPot(){
        return nextPot;
    }

    /**
     * Returns the number of pebbles in the pot
     * @return number of pebbles in the pot
     */
    public int getPebbleCount(){
        return pebbles.size();
    }

    /**
     * Sets whether this pot is highlighted or not for purpose of predicting the movement of the pebble once a pot is held on
     * @param _highlighted whether the pot is highlighted or not
     */
    public void setHighlighted(boolean _highlighted){
        highlighted = _highlighted;

        if (highlighted) highlightCounter++;
        else highlightCounter = 0;
    }

    /**
     * Returns whether the pot is highlighted or not for purpose of predicting the movement of the pebble once a pot is held on
     * @return whether the pot is highlighted or not
     */
    public boolean getHighlighted(){
        return highlighted;
    }

    /**
     *Sets whether the pots are locked or not (so if the user can click the pot or not)
     * @param _locked whether the user can touch the pot or not
     */
    public void setLocked(boolean _locked) {locked = _locked;}

    /**
     *Returns whether the pot is locked or not (so if the user can click the pot or not)
     * @return whether the user can touch the pot or not
     */
    public boolean isLocked() {return locked;}

    /**
     *Draws the pot on the screen
     * @param canvas The canvas on which the pot should be drawn
     */
    @Override
    public void render(Canvas canvas)
    {
        super.render(canvas);
        if(highlighted)
        {
            switch (highlightCounter)
            {
                case 1: highlightedSprite1.render(canvas); break;
                case 2: highlightedSprite2.render(canvas); break;
                case 3: highlightedSprite3.render(canvas);break;
                default: highlightedSprite4.render(canvas);break;
            }
        }
    }

    /**
     * Checks if pot has been touched or not
     * @param event touch event from user
     * @return whether the pot has been touched or not
     */
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (Physics.checkPointRectangleCollision(event.getX(),event.getY(),x,y,width,height))
        {
            switch (event.getAction())
            {
                case MotionEvent.ACTION_MOVE: if (!selected) return false; break;
                case MotionEvent.ACTION_DOWN: selected = true; break;
            }

            return true;
        }
        else
        {
            if (selected)
            {
                selected = false;
                highlightConsecutivePots(false);
                return true; //Pot has been deselected, return true
            }
            return false;
        }
    }

    /**
     * Highlights the consecutive pots based on number of pebbles in the current pot
     * @param _highlighted whether to highlight the consecutive pots or not
     */
    public void highlightConsecutivePots(boolean _highlighted)
    {
        int pebbleCount = getPebbleCount();
        Pot tempPot  = getNextPot();
        for(int i = 0; i < pebbleCount; ++i)
        {
            if ((tempPot instanceof BigPot) && !(tempPot.getPlayer() == getPlayer()))
            {
                tempPot = tempPot.getNextPot();
            }

            tempPot.setHighlighted(_highlighted);
            tempPot = tempPot.getNextPot();
        }
    }
}
