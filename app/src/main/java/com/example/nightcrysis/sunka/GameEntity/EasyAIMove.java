package com.example.nightcrysis.sunka.GameEntity;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.R;

import java.util.Random;

/**
 * Created by tuffail on 06/11/15.
 * Used to determine and perform a move for the easy AI on a separate tread
 */
public class EasyAIMove extends Thread {

    TrayEasyAI tray;
    SmallPot[] smallPots;

    /**
     * Records references to the tray and the small pots in order to evaluate and make a move on the tray
     * @param _tray Reference to the tray which the AI is to perform the move
     * @param _smallPots Reference to the small pots in the tray which the AI is to perform the move
     */
    public EasyAIMove(TrayEasyAI _tray, SmallPot[] _smallPots){
        tray = _tray;
        smallPots = _smallPots;
    }

    /**
     * Starts the thread then determines and distributes the small pot that has been selected by the easy AI
     */
    public void run(){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean turnTaken = false;
        while (!turnTaken){
            int potIndex = new Random().nextInt(7)+7;
            if(smallPots[potIndex].getPebbleCount() != 0){
                tray.waitTime = 150;
                smallPots[potIndex].highlightConsecutivePots(true);
                try {
                    Thread.sleep(150);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                smallPots[potIndex].highlightConsecutivePots(false);
                tray.distributePebblesAI(potIndex);
                tray.toggleLockedPots();
                turnTaken = true;
            }
        }
        tray.AIThinking = false;
    }

}
