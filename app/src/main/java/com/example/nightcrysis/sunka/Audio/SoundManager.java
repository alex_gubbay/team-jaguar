package com.example.nightcrysis.sunka.Audio;

import android.media.MediaPlayer;

import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Class for playing Audio in general. Methods are simple and self explanatory.
 * Music files are placed in the folder res/raw/
 */
public class SoundManager {
    private static MediaPlayer[] mp = new MediaPlayer[29]; //Android's maximum size is 30 MediaPlayers running at once. Reduce if needed.
    private static MediaPlayer bgm = null;
    private static CustomOnCompletionListener listener = new CustomOnCompletionListener();
    private static boolean musicOn = true, soundOn = true;
    private static int length = 0;

    /** Call this to play a new background music. The background music is looped automatically
     * @param soundID The resource file ID of the sound to be played
     */
    public static void playBGM(int soundID)
    {
        if (bgm != null)
            if (bgm.isPlaying())
                bgm.stop();
        bgm = MediaPlayer.create(GlobalVariables.currentActivity, soundID);
        bgm.setLooping(true);
        bgm.start();
        setMusicVolume(GameSettings.musicVolume);
    }

    /**
     * This method pauses the background music playing if there is any.
     * The last position of the currently playing music time is stored.
     * */
    public static void pauseBGM()
    {
        if (bgm != null)
            if (bgm.isPlaying())
            {
                length = bgm.getCurrentPosition();
                bgm.pause();
            }
    }

    /**
     * This method stops the background music playing, if there is any.
     */
    public static void stopBGM()
    {
        if (bgm != null)
            if (bgm.isPlaying())
                bgm.stop();
    }

    /**
     * This method resumes the background music if paused, and will play from where it
     * last stopped before it was paused.
     * If the background music was not playing, playBGM is called.
     */
    public static void resumeBGM()
    {
        if (bgm != null)
        {
            if (!bgm.isPlaying())
            {
                bgm.seekTo(length);
                bgm.start();
            }
        }
        else
        {
            playBGM(R.raw.bgm2);
        }
    }

    /** This is the method for sound effects, the one you will use most often i.e. menu open or close, etc
     * @param soundID Resource file ID for the sound effect.
     * */
    public static void playSound(int soundID)
    {
        if (!soundOn) return;
        /*MediaPlayer mp = MediaPlayer.create(GlobalVariables.context, soundID);
        mp.start();*/

        for (MediaPlayer x : mp)
        {
            if (x == null)
            {
                loadSound(x, soundID);
                break;
            }
            if (!x.isPlaying())
            {
                loadSound(x, soundID);
                break;
            }
        }
    }

    private static void loadSound(MediaPlayer x, int soundID)
    {
        x = MediaPlayer.create(GlobalVariables.currentActivity, soundID);
        x.setOnCompletionListener(listener);
        x.setVolume(GameSettings.soundVolume/100f, GameSettings.soundVolume/100f);
        x.start();
    }

    /**
     * This methods sets the music volume of the background music
     * @param value Integer value between 0 to 100. Safety check provided for
     *              this method if value exceeds or is below the range,
     */
    public static void setMusicVolume(int value)
    {
        //Music volume safety check
        if (value < 0) value = 0;
        else if (value > 100) value = 100;

        //If music is playing, then set the volume.
        if (bgm != null)
            bgm.setVolume(value/100f, value/100f);
    }

    /**
     * This method plays a sample sound when the user has finished changing the sound volume
     * on the game, to show how loud the sound effect would be with the user's new settings.
     * @param volume    Integer value between 0 to 100Safety check provided for
     *              this method if value exceeds or is below the range,
     */
    public static void playSampleSound(int volume)
    {
        //Sound volume safety check
        if (volume < 0) volume = 0;
        else if (volume > 100) volume = 100;

        //Play a sample sound
        int originalVolume = GameSettings.soundVolume;
        GameSettings.soundVolume = volume;
        playSound(R.raw.gem_steal);
        GameSettings.soundVolume = originalVolume;
    }

    /**
     * Toggles the sound on and off with the given specified value in the parameter
     * @param value    Boolean value passed
     */
    public static void toggleSound(boolean value)
    {
        soundOn = value;
    }

    /**
     * Toggles the music on and off with the given specified value in the parameter
     * @param value    Boolean value passed
     */
    public static void toggleMusic(boolean value)
    {
        musicOn = value;
        if (bgm != null)
        {
            if (!musicOn)
                pauseBGM();
            else resumeBGM();
        }
        else playBGM(R.raw.bgm2);
    }

    /**
     * Custom OnCompletionListener to have the MediaPlayers for sound effects to release itself automatically
     */
    static class CustomOnCompletionListener  implements MediaPlayer.OnCompletionListener
    {
        @Override
        public void onCompletion(MediaPlayer mp) {
            if (mp != null)
            {
                mp.stop();
                mp.release();
            }
        }
    }
}
