package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.UIComponents.UIFrame;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Simple frame that displays a simple message in game
 */
public class GameMessageFrame extends UIFrame{
    private String message;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float textWidth, textHeight, sidePadding;

    private static float textSize = -1;

    /** Constructor for Message
     * @param _message Message to be shown */
    public GameMessageFrame(String _message) {
        super(0, 0, 0, 0, R.drawable.frame2_unpressed);
        message = _message;
        initialize();
    }

    /** Initialization method. Sets the textSize and adjusts frame size to text */
    private void initialize()
    {
        if (textSize < 0)
        {
            textSize = UIUtilities.findTextSizeByHeight("A", GlobalVariables.backgroundHeight / 25);
        }
        sidePadding = GlobalVariables.backgroundHeight / 16;
        setTextSize();
        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 2);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), GlobalVariables.screenSizeY - height);
    }

    /**
     * getter method that gets the message shown
     * @return message shown
     */
    public String getMessage() {
        return message;
    }

    /** Rendering method, draws things on screen
     * @param canvas Canvas object used to draw things on screen
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);
        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(message, x + (width / 2) - (textWidth / 2), y + (height / 2) - (textHeight / 2), GlobalVariables.paint);
    }

    /** Setter method, sets the message shown
     * @param _message Message to be shown*/
    public void setMessage(String _message)
    {
        message = _message;
        setTextSize();
        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 2);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), GlobalVariables.screenSizeY - height);
    }

    /** Setter method for text Color
     * @param color Color of text, passed as an integer */
    public void setTextColor(int color)
    {
        textColor = color;
    }

    /** Setter method for text size. Called internally */
    private void setTextSize()
    {
        GlobalVariables.paint.setTextSize(textSize);
        Rect rect = new Rect();
        GlobalVariables.paint.getTextBounds(message, 0, message.length(), rect);
        textWidth = rect.width();
        textHeight = rect.height();
    }

    /**
     * The message frame does not respond to user touches
     * @param event The MotionEvent that the device has detected
     * @return always false
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
