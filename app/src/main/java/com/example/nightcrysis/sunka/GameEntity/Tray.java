package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.PebbleMoveEvent;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.FileIO.moveTimer;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.TimedMessageFrame;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

/**
 * Governs pebbles and pots objects throughout an entire game.
 * Sets the starting configuration of the game.
 * Manages the players' turns.
 * In charge of the movement of the pebbles for each move, and manages the end of the game.
 *
 * Extends {@link GameEntity}.
 *
 * @author florianaucomte
 */
public class Tray extends GameEntity
{
    public int gameResult;

    //Player messages
    protected String notPlayerTurn = "It is not your turn yet!", notSmallPot = "Please select the smaller pots!",
            gameIsOver = "Game Over!";

    //Private variables
    private int numberOfSmallPots = 14, numberOfBigPots = 2, pebblesPerPot = 7;
    private ArrayList<GameEntity> pebbles = new ArrayList<>();
    private ArrayList<GameEntityInterface> events = new ArrayList<>(), standingEvents = new ArrayList<>();
    private boolean played, hasExtraTurn = false;
    private float pebbleMoveTimeCounter = 0;
    private static final float pebbleMoveTime = 0.1f;

    //Protected variables
    protected boolean isFirstTouch, pebbleMovingLock = false, lastPebbleSet = false;
    protected float notPlayerTurnDuration = 1.5f, notSmallPotDuration = 1.5f,
            extraTurnDuration = 2, gameIsOverDuration = 10, playerSkipsTurnDuration = 2;
    protected moveTimer timer = new moveTimer();
    protected Player currentPlayer = Player.A;
    protected SmallPot[] smallPots = new SmallPot[numberOfSmallPots];
    protected BigPot[] bigPots = new BigPot[numberOfBigPots];
    protected TimedMessageFrame messageFrame;

    /**
     * Creates a new Tray object.
     * Instantiates the pots, and creates the 'next' and 'opposite' references for each pot.
     * Assigns pots to their respective players.
     * @param _x X coordinate of the tray.
     * @param _y Y coordinate of the tray.
     * @param _width Width of the tray.
     * @param _height Height of the tray.
     */
    public Tray(float _x, float _y, float _width, float _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

        bigPots[0] = new BigPot(width-(width/11*2), y, width/11*2, width/11*2);
        bigPots[1] = new BigPot(0, y, width/11*2, width/11*2);

        float temp = width/11*2;

        for(int i = 0; i < smallPots.length/2; ++i)
            smallPots[i] = new SmallPot(temp + (i*temp/2), y+temp/2, temp/2, temp/2);

        for(int i = smallPots.length/2; i < smallPots.length; ++i)
            smallPots[i] = new SmallPot(width-temp-(temp/2*(i-6)), y,temp/2, temp/2);

        isFirstTouch = true;

        setNextReferences();
        setOppositeReferences();
        assignPotsToPlayers();
        setStartingConfiguration();

        //Sets position of pebbles at start
        update(0);
    }

    /**
     * Gets the tray's timer.
     *
     * @return a moveTimer object.
     */
    public moveTimer getTimer()
    {
        return timer;
    }

    /**
     * Sets the game's current player to the player passed as an argument.
     *
     * @param currentPlayer the player whose turn it is.
     */
    public void setCurrentPlayer(Player currentPlayer)
    {
        this.currentPlayer = currentPlayer;
        isFirstTouch = false;
    }

    /**
     * Gets a certain player's score, ie their number of pebbles in their big pot.
     *
     * @param player the player whose score is wanted.
     * @return the number of pebbles in the big pot, ultimately the player's score.
     */
    public int getPlayerScore(Player player)
    {
        if (player == Player.A)
            return bigPots[0].getPebbleCount();
        else return bigPots[1].getPebbleCount();
    }

    private void setNextReferences()
    {
        //setting the 'next' reference for each pot
        for(int i = 0; i < smallPots.length/2-1; ++i)
            smallPots[i].setNextPot(smallPots[i + 1]);

        smallPots[smallPots.length/2-1].setNextPot(bigPots[0]);
        bigPots[0].setNextPot(smallPots[smallPots.length / 2]);

        for(int i = smallPots.length/2; i < smallPots.length-1; ++i)
            smallPots[i].setNextPot(smallPots[i + 1]);

        smallPots[smallPots.length-1].setNextPot(bigPots[1]);
        bigPots[1].setNextPot(smallPots[0]);
    }

    private void setOppositeReferences()
    {
        //setting the 'opposite' reference for each pot
        for(int i = 0; i < smallPots.length; ++i)
            smallPots[i].setOppositePot(smallPots[smallPots.length - 1 - i]);
    }

    private void assignPotsToPlayers()
    {
        for(int i = 0; i < smallPots.length/2; ++i)
            smallPots[i].setPlayer(Player.A);

        for(int i = smallPots.length/2; i < smallPots.length; ++i)
            smallPots[i].setPlayer(Player.B);

        bigPots[0].setPlayer(Player.A);
        bigPots[1].setPlayer(Player.B);
    }

    private void setStartingConfiguration()
    {
        for(SmallPot sp : smallPots)
        {
            //instantiate all pebbles (7 in each small pot)
            for(int j = 0; j < pebblesPerPot; ++j)
            {
                Pebble pebble;
                int pebbleImageID = 0;
                switch (GlobalVariables.random.nextInt(7))
                {
                    case 0: pebbleImageID = R.drawable.red_gem_1; break;
                    case 1: pebbleImageID = R.drawable.green_gem_1; break;
                    case 2: pebbleImageID = R.drawable.yellow_gem_1; break;
                    case 3: pebbleImageID = R.drawable.cerulean_gem_1; break;
                    case 4: pebbleImageID = R.drawable.blue_gem_1; break;
                    case 5: pebbleImageID = R.drawable.purple_gem_1; break;
                    case 6: pebbleImageID = R.drawable.magenta_gem_1; break;
                }
                pebble = new Pebble(0, 0, sp.getWidth() / 2, sp.getHeight() / 2, pebbleImageID);

                //Finding random position to add pebbles in
                float totalDisplacement = GlobalVariables.random.nextInt((int)(sp.getHeight()/2 - pebble.getHeight()/2)) + GlobalVariables.random.nextFloat(),
                        degree = (float)(GlobalVariables.random.nextInt(360)),
                        dx = (float) (Math.cos(Math.toRadians(degree)) * totalDisplacement),
                        dy = (float)(Math.sin(Math.toRadians(degree)) * totalDisplacement);
                pebble.setPosition((sp.getX() + sp.getHeight() / 4) + dx, (sp.getY() + sp.getHeight() / 4) + dy);

                pebbles.add(pebble); //Ignore this error
                sp.addPebble(pebble);
            }
        }
    }

    /**
     * Method for showing a message. Automatically checks with the existing one if its message has
     * the same content or not. If so, this method is ignored. If not, a new message will be created.
     */
    protected void showMessage(String message, float duration)
    {
        if (messageFrame != null)
        {
            //If the current message frame's message is different to the new one
            // (as the new one always takes precedence), set its new message and duration values
            if (!messageFrame.getMessage().equals(message) & !messageFrame.getNeedsRemoval())
            {
                messageFrame.setDuration(duration);
                messageFrame.setMessage(message);
            }
            //If the messageFrame needs removal (a.k.a. not shown on screen)
            else if (messageFrame.getNeedsRemoval())
            {
                messageFrame = new TimedMessageFrame(message, duration);
                ScreenManager.getCurrentScreen().addToOverlay(messageFrame);
            }
        }
        else
        {
            messageFrame = new TimedMessageFrame(message, duration);
            ScreenManager.getCurrentScreen().addToOverlay(messageFrame);
        }
    }

    /**
     * After each move, this method locks the pots according to whose turn it is.
     */
    public void toggleLockedPots()
    {
        if(isGameOver())
        {
            for(SmallPot sp : smallPots)
                sp.setLocked(true);

            bigPots[0].setLocked(true);
            bigPots[1].setLocked(true);

            showMessage(gameIsOver, gameIsOverDuration);
        }
        else
        {
            boolean toggleLocked;

            toggleLocked = currentPlayer != Player.A;

            for(int i = 0; i < smallPots.length; ++i)
            {
                if(i < smallPots.length/2)
                    smallPots[i].setLocked(toggleLocked);
                else
                    smallPots[i].setLocked(!toggleLocked);
            }
            bigPots[0].setLocked(toggleLocked);
            bigPots[1].setLocked(!toggleLocked);
        }
    }

    private void findGameEnd()
    {
        int playerAScore = bigPots[0].getPebbleCount();
        int playerBScore = bigPots[1].getPebbleCount();

        if(playerAScore > playerBScore)
            gameResult = 1;
        else if(playerAScore == playerBScore)
            gameResult = 0;
        else
            gameResult = -1;
    }

    private void switchPlayer()
    {
        if (currentPlayer == Player.A)
        {
            //if all of player B's pots are empty but not player A's
            if(playerPotsAreEmpty(Player.B) && !playerPotsAreEmpty(Player.A))
                showPlayerSkipsTurnMessage(currentPlayer);
            else
                currentPlayer = Player.B;
        }
        else
        {
            if(playerPotsAreEmpty(Player.A) && !playerPotsAreEmpty(Player.B))
                showPlayerSkipsTurnMessage(currentPlayer);
            else
                currentPlayer = Player.A;
        }
    }

    /**
     * Update method which is in charge of the animation, switching the players' turn and
     * checking if the game is over.
     *
     * @param delta The time elapsed since the last update cycle.
     */
    @Override
    public void update(float delta)
    {
        if (pebbleMoveTimeCounter > 0)
            pebbleMoveTimeCounter -= delta;

        if (standingEvents.size() > 0 & pebbleMoveTimeCounter <= 0)
        {
            events.add(standingEvents.get(0));
            standingEvents.remove(0);
            pebbleMoveTimeCounter = pebbleMoveTime;
        }

        for(int i = 0; i < pebbles.size(); ++i)
            pebbles.get(i).update(delta);

        Iterator<GameEntityInterface> tempIterator = events.iterator();
        while (tempIterator.hasNext())
            if (tempIterator.next().getNeedsRemoval())
                tempIterator.remove();

        //Update the events by creating a new array list
        ArrayList<GameEntityInterface> tempEvents = new ArrayList<>(events);
        for (GameEntityInterface x : events)
            x.update(delta);

        //If all pebble moving events are completed, and if (hasExtraTurn or all player pots are empty), then switch players.
        if (pebbleMovingLock & lastPebbleSet & (events.size() == 0) & (standingEvents.size() == 0))
        {
            pebbleMovingLock = false;
            lastPebbleSet = false;
            if (!hasExtraTurn)
            {
                switchPlayer();
                toggleLockedPots();
            }
        }

        if (isGameOver())
        {
            if(played==false)
                played=true;

            findGameEnd();
            currentPlayer = null;
        }
    }

    /**
     * Renders the pots and the animation of the pebbles.
     *
     * @param canvas Canvas object that is passed for drawing calls.
     */
    @Override
    public void render(Canvas canvas)
    {
        GlobalVariables.paint.setColor(Color.BLUE);
        GlobalVariables.paint.setTextSize(UIUtilities.findTextSizeByHeight("1", smallPots[0].getHeight())/2);
        for(SmallPot sp : smallPots)
        {
            sp.render(canvas);
            if (sp.getPlayer() == Player.A)
                canvas.drawText(String.valueOf(sp.getPebbleCount()), sp.getX() + sp.getWidth()/2, sp.getY() + (sp.getHeight() * 3 / 2), GlobalVariables.paint);
            else canvas.drawText(String.valueOf(sp.getPebbleCount()), sp.getX() + sp.getWidth()/2, sp.getY(), GlobalVariables.paint);
        }

        for(BigPot bp : bigPots)
        {
            bp.render(canvas);

            if (bp.getPlayer() == Player.A)
                canvas.drawText(String.valueOf(bp.getPebbleCount()), bp.getX() + bp.getWidth()/2, bp.getY() + bp.getHeight() + smallPots[0].getHeight()/2, GlobalVariables.paint);
            else canvas.drawText(String.valueOf(bp.getPebbleCount()), bp.getX() + bp.getWidth()/2, bp.getY(), GlobalVariables.paint);
        }

        //Render the events by creating a new array list as they may be modified in update(float delta)
        ArrayList<GameEntityInterface> tempPebbles = new ArrayList<GameEntityInterface>(pebbles);
        for (GameEntityInterface x : tempPebbles)
            x.render(canvas);

        /* The following are drawn in the reverse order*/
        ArrayList<GameEntityInterface> tempEvents = new ArrayList<>(standingEvents);
        for (int i = tempEvents.size() - 1; i >= 0; i--)
            tempEvents.get(i).render(canvas);

        tempEvents = new ArrayList<>(events);
        for (int i = tempEvents.size() - 1; i >= 0; i--)
            tempEvents.get(i).render(canvas);
    }

    /**
     * Called when the user touches the device.
     * This method triggers the distribution of the pebbles as soon as the user's finger is lifted
     * from the selected pot.
     *
     * @param event The MotionEvent that the device has detected.
     * @return a boolean value for when the device has been touched.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //If pebbles are moving
        if (pebbleMovingLock) return true;

        for(int i = 0; i < smallPots.length; ++i)
        {
            if(smallPots[i].onTouchEvent(event))
            {
                switch(event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        if(smallPots[i].isLocked())
                        {
                            showMessage(notPlayerTurn, notPlayerTurnDuration);
                            smallPots[i].selected = false;
                        }
                        else smallPots[i].highlightConsecutivePots(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (smallPots[i].selected)
                        {
                            if(!smallPots[i].isLocked())
                            {
                                // If it's the first turn of the game
                                if(isFirstTouch)
                                {
                                    if(i < 7)
                                        currentPlayer = Player.A;
                                    else
                                        currentPlayer = Player.B;
                                    isFirstTouch = false;
                                    toggleLockedPots();
                                }
                                smallPots[i].selected = false;
                                smallPots[i].highlightConsecutivePots(false);
                                distributePebbles(smallPots[i]);
                            }
                        }
                        break;
                }
                return true;
            }
        }

        for(BigPot bp : bigPots)
        {
            if(bp.onTouchEvent(event))
            {
                showMessage(notSmallPot, notSmallPotDuration);
                return true;
            }
        }
        return false;
    }

    //This method is missing the actual movement of the pebbles to make the animation possible!!
    //This is also the very big method with all the rules for the game

    /**
     * Distributes the pebbles along the pots according to the game rules.
     *
     * @param touchedPot the small pot which has been touched by the player.
     */
    public void distributePebbles(SmallPot touchedPot)
    {
        pebbleMovingLock = true;
        lastPebbleSet = true;
        int pebbleCount = touchedPot.getPebbleCount();

        //get current pot's pebbles
        Stack<Pebble> pebbles= touchedPot.getPebbles();

        Pot tempPot = touchedPot;

        for (int i = 0; i < pebbleCount; ++i)
        {
            tempPot = tempPot.getNextPot();

            //if the next pot is the opponent's big pot, skip it
            if ((tempPot instanceof BigPot) && !(tempPot.getPlayer() == touchedPot.getPlayer()))
                tempPot = tempPot.getNextPot();

            //add one pebble to next pot then move on to following pot
            Pebble tempPebble = pebbles.pop();

            Iterator tempIterator = pebbles.iterator();
            //If the memory address of the pebble is the same as the pebble selected,
            while (tempIterator.hasNext())
                if (tempIterator.next() == tempPebble)
                {
                    //Then remove the pebble from the array list.
                    tempIterator.remove();
                    break;
                }
            setPebbleInPot(tempPebble, tempPot, (i == pebbleCount - 1));
        }
    }

    /**
     * For each last pebble in a move, this method is called to check the possible action, ie
     * getting an extra turn or stealing the opponent's pebbles.
     *
     * @param tempPebble the current last pebble in the stack of pebbles.
     * @param tempPot the pot the last pebble landed in.
     */
    public void lastPebbleAction(Pebble tempPebble, Pot tempPot)
    {
        hasExtraTurn = false;
        //if the last pebble didn't land on the player's big pot, switch players
        if (tempPot == bigPots[0] || tempPot == bigPots[1])
        {
            if(!playerPotsAreEmpty(currentPlayer))
            {
                String extraTurn = "Player " + currentPlayer.name() + " gets an extra turn!";
                showMessage(extraTurn, extraTurnDuration);
                hasExtraTurn = true;
            }
        }
        //if the last pebble landed on one of the player's empty small pots
        else if (tempPot.getPebbleCount() == 1 && tempPot.getPlayer() == currentPlayer)
        {
            //retrieve the last pebble we just dropped
            Stack<Pebble> lastPotPebble = tempPot.getPebbles();
            SmallPot lastPot = (SmallPot) tempPot;

            //get the pebbles from the opposite pot
            SmallPot oppositePot = lastPot.getOppositePot();
            Stack<Pebble> oppositePotPebbles = oppositePot.getPebbles();
            SoundManager.playSound(R.raw.gem_steal);

            //add the two stacks together
            oppositePotPebbles.push(lastPotPebble.pop());

            //add the pebbles to the player's big pot
            int initialStackSize = oppositePotPebbles.size();
            for (int j = 0; j < initialStackSize; ++j)
            {
                if (currentPlayer == Player.A)
                {
                    tempPebble = oppositePotPebbles.pop();

                    Iterator<GameEntity> tempIterator = pebbles.iterator();
                    //If the memory address of the pebble is the same as the pebble selected,
                    while (tempIterator.hasNext())
                        if (tempIterator.next() == tempPebble)
                        {
                            //Then remove the pebble from the array list.
                            tempIterator.remove();
                            break;
                        }
                    setPebbleInPot(tempPebble, bigPots[0], false);
                }
                else
                {
                    tempPebble = oppositePotPebbles.pop();

                    Iterator<GameEntity> tempIterator = pebbles.iterator();
                    //If the memory address of the pebble is the same as the pebble selected,
                    while (tempIterator.hasNext())
                        if (tempIterator.next() == tempPebble)
                        {
                            //Then remove the pebble from the array list.
                            tempIterator.remove();
                            break;
                        }
                    setPebbleInPot(tempPebble, bigPots[1], false);
                }
            }
        }
        lastPebbleSet = true;
    }

    /**
     * Checks if it is the AI player's turn.
     *
     * @return a boolean value, true if it is its turn.
     */
    protected boolean isTurnAI()
    {
        return ((currentPlayer == Player.B) & (!pebbleMovingLock) & (!lastPebbleSet));
    }

    /**
     * Computes and shows a message when a player cannot make any further moves since there are no
     * more pebbles on his side of the board.
     *
     * @param player the player whose turn has to be skipped.
     */
    protected void showPlayerSkipsTurnMessage(Player player)
    {
        String opponentName = "";

        if(player == Player.A)
            opponentName = "B";
        else
            opponentName = "A";

        String playerSkipsTurn = "Player " + player.name() + "'s side is empty. It's still Player " + opponentName + "'s turn.";
        showMessage(playerSkipsTurn, playerSkipsTurnDuration);
    }

    /**
     * Method to make the pebbles land in the big pots at the end of the animation.
     *
     * @param pebble the pebble which is landing in the pot.
     * @param pot the pot in which the pebble is landing.
     * @param isLastPebble a boolean to check if the current pebble is the last in the stack.
     */
    public void setPebbleInPot(Pebble pebble, Pot pot, boolean isLastPebble)
    {
        float totalDisplacement = GlobalVariables.random.nextInt((int)(pot.getHeight()/2 - pebble.getHeight()/2)) + GlobalVariables.random.nextFloat(),
                degree = (float)(GlobalVariables.random.nextInt(360)),
                dx = (float) (Math.cos(Math.toRadians(degree)) * totalDisplacement),
                dy = (float)(Math.sin(Math.toRadians(degree)) * totalDisplacement),
                toX, toY;
        if(pot instanceof BigPot)
        {
            toX = (pot.getX() + pot.getHeight() / 5 * 2) + dx;
            toY = (pot.getY() + pot.getHeight() / 5 * 2) + dy;
        }
        else
        {
            toX = (pot.getX() + pot.getHeight() / 4) + dx;
            toY = (pot.getY() + pot.getHeight() / 4) + dy;
        }
        standingEvents.add(new PebbleMoveEvent(pebble, pot, toX, toY, pebbles, this, isLastPebble));
    }

    /**
     * Checks if all of the player's pot are empty, thus knowing if they can make a move or not.
     *
     * @param player the player whose pots may be empty.
     * @return true if the pots are empty, false it they aren't.
     */
    protected boolean playerPotsAreEmpty(Player player)
    {
        if(player == Player.A)
        {
            for(int i = 0; i < smallPots.length/2; ++i)
            {
                if(smallPots[i].getPebbleCount() != 0)
                    return false;
            }
            return true;
        }
        else
        {
            for(int i = smallPots.length/2; i < smallPots.length; ++i)
            {
                if(smallPots[i].getPebbleCount() != 0)
                    return false;
            }
            return true;
        }
    }

    /**
     * From the pot which is selected, highlights the consecutive pots which will be affected by
     * the move, ie which amount of pebbles will change after the move is made.
     *
     * @param potIndex the pot which has been selected by the player.
     */
    public void highlightOpponentConsecutivePots(int potIndex)
    {
        smallPots[potIndex].highlightConsecutivePots(true);
        try
        {
            Thread.sleep(150);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        smallPots[potIndex].highlightConsecutivePots(false);
    }

    /**
     * Checks if the game is over or not by checking that no pots have any pebbles.
     *
     * @return true if the game is over, false if it isn't.
     */
    public boolean isGameOver()
    {
        //If pebbles are moving, return false
        if (pebbleMovingLock | lastPebbleSet) return false;

        for(SmallPot sp : smallPots)
        {
            if(sp.getPebbleCount() != 0)
                return false;
        }
        return true;
    }
}