package com.example.nightcrysis.sunka.Screen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.EditText;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataManager;
import com.example.nightcrysis.sunka.FileIO.GamePlayer;
import com.example.nightcrysis.sunka.FileIO.HighScoreManager;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GameEntity.Tray;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.HighScoreInputFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.HighscoreFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.MenuDropDownFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.MessageFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.SelectTurnFrame;
import com.example.nightcrysis.sunka.UIComponents.UIButton;
import com.example.nightcrysis.sunka.UIComponents.UIFrame;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

import static com.example.nightcrysis.sunka.GlobalVariables.backgroundHeight;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundOffsetX;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundOffsetY;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundWidth;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Abstract class GameScreen used for different game modes of the game.
 * Main implementations of the in game UI, message handling and high score saving methods are all
 * implemented in this class.
 */
public abstract class GameScreen extends Screen{
    protected Tray tray;
    private UIButton menuButton;
    private UIFrame timeElapsedFrame;
    private boolean timerStarted = false, isAiGame = false, aiTurnSet = false;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float textSize, textHeight;
    private SelectTurnFrame selectTurnFrame;

    //Event controllers for controlling end game logic and UI displays/inputs
    private EventController endGameEvent1;
    private HighscoreFrame highscoreFrame;

    /**
     * Constructor for GameScreen
     * Instantiates the variables and objects, as well as their coordinates on screen.
     * It also creates a prompt to ask if the player wants to go first if the game is an AI game.
     */
    public GameScreen()
    {
        float singlePotSize = GlobalVariables.screenSizeX/11;

        float menuButtonSize = GlobalVariables.screenSizeY / 8;
        menuButton = new UIButton(GlobalVariables.screenSizeX - menuButtonSize, 0, menuButtonSize, menuButtonSize, R.drawable.menu_button_unpressed, R.drawable.menu_button_pressed, R.drawable.menu_button_overlay);
        gameEntities.add(menuButton);

        SpriteEntity background = new SpriteEntity(0, 0, GlobalVariables.screenSizeX, GlobalVariables.screenSizeY, R.drawable.game_background);
        underlay.add(background);

        SpriteEntity trayImage = new SpriteEntity(- singlePotSize / 2, (GlobalVariables.screenSizeY / 2 - singlePotSize) - singlePotSize / 2,
                GlobalVariables.screenSizeX + singlePotSize * 1.5f, singlePotSize * 2 + singlePotSize, R.drawable.tray_image);
        underlay.add(trayImage);

        float tempWidth = GlobalVariables.screenSizeX * 0.75f,
                tempHeight = trayImage.getY() / 2;
        textHeight = tempHeight * 0.5f;
        textSize = UIUtilities.findTextSizeByHeight("A", textHeight);
        timeElapsedFrame = new UIFrame(GlobalVariables.screenSizeX / 2 - tempWidth / 2, 0, tempWidth, tempHeight, R.drawable.frame2_unpressed);
        userInterface.add(timeElapsedFrame);

        selectTurnFrame = new SelectTurnFrame();

        //Create select turn overlay.
        ScreenState screenState = ScreenManager.getScreenState();

        if (screenState == ScreenState.aiGameHard | screenState == ScreenState.aiGameMedium | screenState == ScreenState.aiGameEasy)
        {
            isAiGame = true;
            ScreenManager.addToMasterOverlay(selectTurnFrame);
        }
        else
        {
            selectTurnFrame.remove();
        }
    }

    /**
     * Update method. Makes a constant check if the game has ended. If it has ended, it will
     * proceed on creating a sequence of message dialogues, and prompts the user for input
     * to be placed onto the highscore.
     * If the game is an AI game, the update method will not update anything in game until
     * the user has selected to go first or second.
     * @param delta    Time passed since last update cycle
     */
    @Override
    public void update(float delta) {
        if (isAiGame)
        {
            if (!selectTurnFrame.getNeedsRemoval()) return;
            else
            {
                if (selectTurnFrame.isGoFirst())
                    tray.setCurrentPlayer(Player.A);
                else tray.setCurrentPlayer(Player.B);
                tray.toggleLockedPots();
                isAiGame = false;
            }
        }

        super.update(delta);

        if (tray != null)
        {
            if (!timerStarted)
            {
                tray.getTimer().startGame();
                tray.getTimer().startNewEvent("TotalGameTime");
                timerStarted = true;
            }

            if (tray.isGameOver())
            {
                if (endGameEvent1 == null)
                {
                    tray.getTimer().endEvent("TotalGameTime");
                    tray.getTimer().stopGame();

                    endGameEvent1 = new EventController();

                    //Not single player game
                    if (ScreenManager.getScreenState() == ScreenState.localMultiplayerGame)
                    {
                        switch (tray.gameResult)
                        {
                            case -1: endGameEvent1.addEvent(new MessageFrame("Player B has won! (Top tray)")); break;
                            case 0: endGameEvent1.addEvent(new MessageFrame("The game is a draw!")); break;
                            case 1: endGameEvent1.addEvent(new MessageFrame("Player A has won! (Bottom tray)")); break;
                        }

                        endGameEvent1.addEvent(new MessageFrame("Game over! Player 1 has got " + tray.getPlayerScore(Player.A) + " points!"));
                        endGameEvent1.addEvent(new HighScoreInputFrame(tray.gameResult, tray.getPlayerScore(Player.A), tray.getTimer().getDeadEventSeconds("TotalGameTime")));
                        endGameEvent1.addEvent(new MessageFrame("Game over! Player 2 has got " + tray.getPlayerScore(Player.B) + " points!"));
                        endGameEvent1.addEvent(new HighScoreInputFrame(-tray.gameResult, tray.getPlayerScore(Player.B), tray.getTimer().getDeadEventSeconds("TotalGameTime")));
                    }
                    else if (ScreenManager.getScreenState() == ScreenState.onlineMultiplayerGame)
                    {
                        if (ScreenManager.isHost)
                        {
                            switch (tray.gameResult) {
                                case -1:
                                    endGameEvent1.addEvent(new MessageFrame("You have lost! Game over!"));
                                    break;
                                case 0:
                                    endGameEvent1.addEvent(new MessageFrame("The game is a draw! Game over!"));
                                    break;
                                case 1:
                                    endGameEvent1.addEvent(new MessageFrame("You have won! Game over!"));
                                    break;
                            }
                            endGameEvent1.addEvent(new HighScoreInputFrame(tray.gameResult, tray.getPlayerScore(Player.A), tray.getTimer().getDeadEventSeconds("TotalGameTime")));
                        }
                        else
                        {
                            switch (tray.gameResult)
                            {
                                case -1: endGameEvent1.addEvent(new MessageFrame("You have won! Game over!")); break;
                                case 0: endGameEvent1.addEvent(new MessageFrame("The game is a draw! Game over!")); break;
                                case 1: endGameEvent1.addEvent(new MessageFrame("You have lost! Game over!")); break;
                            }
                            endGameEvent1.addEvent(new HighScoreInputFrame(tray.gameResult, tray.getPlayerScore(Player.B), tray.getTimer().getDeadEventSeconds("TotalGameTime")));
                        }

                    }
                    //Is single player game (1 player input)
                    else
                    {//Player won
                        if (tray.gameResult == 1)
                        {
                            SoundManager.playSound(R.raw.game_opposite_pot_capture);
                            endGameEvent1.addEvent(new MessageFrame("You have won! Game over!"));
                        }
                        else if (tray.gameResult == 0)
                        {
                            //Every body is a winner sound?
                            endGameEvent1.addEvent(new MessageFrame("The game is a draw! Game over!"));
                        }
                        else if (tray.gameResult == -1)
                        {
                            SoundManager.playSound(R.raw.cliched_losing_sound);
                            endGameEvent1.addEvent(new MessageFrame("You have lost! Game over!"));
                        }

                        endGameEvent1.addEvent(new HighScoreInputFrame(tray.gameResult, tray.getPlayerScore(Player.A), tray.getTimer().getDeadEventSeconds("TotalGameTime")));
                    }

                    ScreenManager.addToMasterOverlay(endGameEvent1);
                }
                else if (endGameEvent1.getNeedsRemoval())
                {
                    if (highscoreFrame == null)
                    {
                        float sidePadding = backgroundWidth / 8;
                        highscoreFrame= new HighscoreFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + (sidePadding * 2 / 3),
                                backgroundWidth - (sidePadding * 2), backgroundHeight - sidePadding, R.drawable.frame2_unpressed);
                        ScreenManager.addToMasterOverlay(highscoreFrame);
                    }
                    else if (highscoreFrame.getNeedsRemoval())
                        ScreenManager.fadeOutTo(ScreenState.mainMenu);
                }
            }
        }
    }

    /**
     * Render method. Calls render(canvas) in parent class Screen
     * @param canvas    Canvas object used to draw images on screen
     * @see Screen
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.setColor(textColor);
        canvas.drawText("Time elapsed:", timeElapsedFrame.getX() + timeElapsedFrame.getWidth() / 10, timeElapsedFrame.getY() + timeElapsedFrame.getHeight() / 2 + textHeight / 2, GlobalVariables.paint);

        //Rendering is done on a separate thread, so needs to check if tray has been instantiated or not.
        if (tray != null)
        {
            canvas.drawText(tray.getTimer().getGameElapsedTimeSeconds() + "s", timeElapsedFrame.getX() + timeElapsedFrame.getWidth() * 2.2f / 3, timeElapsedFrame.getY() + timeElapsedFrame.getHeight()/2 + textHeight / 2, GlobalVariables.paint);
        }
    }

    /**
     * Calls parent onTouchEvent. Checks if menuButton is pressed. If true, a MenuDropDownFrame will
     * be added to ScreenManager's master overlay array list
     * @param event    The MotionEvent that the device has detected
     * @see Screen
     * @see ScreenManager
     * @see MenuDropDownFrame
     */
    @Override
    public void onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        switch (event.getAction())
        {
            case MotionEvent.ACTION_UP:
                if (menuButton.isPressed())
                {
                    ScreenManager.addToMasterOverlay(new MenuDropDownFrame());
                }
        }
    }
}
