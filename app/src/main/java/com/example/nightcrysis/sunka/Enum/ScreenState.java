package com.example.nightcrysis.sunka.Enum;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Defines the screen state of which the game is currently in. May be extended!
 * Each screen state is defined by an enum value. This is used in a swithc statement to
 * navigate between different screens in ScreenManager
 * @see com.example.nightcrysis.sunka.Screen.ScreenManager
 */
public enum ScreenState {
    mainMenu,
    gameSelection, aiGameEasy, aiGameMedium, aiGameHard, localMultiplayerGame, onlineMultiplayerGame, onlineMultiplayerSetup,
    highScores, instructions,
    onlineMenu
}
