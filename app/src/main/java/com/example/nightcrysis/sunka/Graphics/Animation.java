package com.example.nightcrysis.sunka.Graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;

import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * This Animation class is made very simple. It simply loops through all the existing frames in order and that's it.
 *
 */
public class Animation {
    private Bitmap sprite = null;
    private float x = 0, y = 0, width = 0, height = 0, frameDuration = 0, durationCounter = 0;
    private int frameWidth = 0, frameHeight = 0, frameCounter = 0;
    private Frame[] frames;

    /**
     * Constructor for an Animation object.
     * @_x x position
     * @_y y position
     * @_width width of drawn animation (width shown on screen)
     * @_height height of drawn animation (height shown on screen)
     * @_frameWidth actual width of a single frame in pixels
     * @_frameHeight actual height of a single frame in pixels
     * @_frames An array of Frame objects, which contains x, y position of where the upper left corner of a frame is
     * @_frameDurationSeconds how long (in seconds) until the next frame is shown?
     * @_animationResourceId the Id of the file in res/drawable. E.g. R.drawable.filename
     * */
    public Animation(float _x, float _y, float _width, float _height, int _frameWidth, int _frameHeight, Frame[] _frames, float _frameDurationSeconds, int animationResourceId)
    {
        //Setting the Bitmap image
        sprite = ImageManager.getBitmap(animationResourceId);

        x = _x;
        y = _y;
        width = _width;
        height = _height;
        frameWidth = _frameWidth;
        frameHeight = _frameHeight;
        frames = _frames;
        frameDuration = _frameDurationSeconds;
        durationCounter = frameDuration;
    }

    /**
     * Update method. Must be called. Changes frames depending on duration per frame specified.
     * @param delta    variable time since the last update cycle was called
     */
    public void update(float delta)
    {
        durationCounter -= delta;
        if (durationCounter <= 0)
        {
            durationCounter = frameDuration;
            if (frameCounter == frames.length - 1)
            {
                frameCounter = 0;
            }
            else frameCounter++;
        }
    }

    /**
     *  Rendering method for Animation. Must be called.
     * @param canvas    Canvas object used to draw images on screen
     */
    public void render(Canvas canvas)
    {
        canvas.drawBitmap(sprite,
                          new Rect(frames[frameCounter].x, frames[frameCounter].y, frameWidth + frames[frameCounter].x, frameHeight + frames[frameCounter].y),
                          new RectF(x, y, x + width, y + height),
                          GlobalVariables.paint);
    }

    /**
     * Setter method for positions
     * @param _x    new x position
     * @param _y    new y position
     */
    public void setPosition(float _x, float _y) {
        x = _x;
        y = _y;
    }

    /**
     * Setter method for size
     * @param _width     new width
     * @param _height    new height
     */
    public void setSize(float _width, float _height)
    {
        width = _width;
        height = _height;
    }

    /**
     * Changes each frame duration with the specified input
     * @param _frameDuration    new frame duration
     */
    public void setFrameDuration(float _frameDuration) {
        frameDuration = _frameDuration;
    }

    /** Under normal and absolute circumstances, this method will never be needed. */
    /*public void setFrameSize(float _frameWidth, float _frameHeight)
    {
        frameWidth = _frameWidth;
        frameHeight = _frameHeight;
    }*/

    /**
     * Getter method for the x coordinate
     * @return x
     */
    public float getX() {
        return x;
    }

    /**
     * Getter method for the y coordinate
     * @return y
     */
    public float getY() {
        return y;
    }

    /**
     * Getter method for width
     * @return width
     */
    public float getWidth() {
        return width;
    }

    /**
     * Getter method for height
     * @return height
     */
    public float getHeight() {
        return height;
    }

    /**
     * Getter method for the frame duration
     * @return frameDuration
     */
    public float getFrameDuration() {
        return frameDuration;
    }
}
