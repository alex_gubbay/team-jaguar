package com.example.nightcrysis.sunka.FileIO;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.NoSuchElementException;

/**
 * Class for server side communication in sockets. Internal class, never directly interacted with
 * by the game. All communication done through static {@link DataTransmissionManager}
 * @Author Alex
 */
public class server {


    private BufferedReader incomingReader;
    private PrintWriter outgoingSender;

    /**
     * Server class constructor
     */
    public server() {

        /**
         * Continuously checks for new connections, and informs DataTransmissionManager of the port
         * and IP it is listening on. Will start the AsyncTask for handling the communication once
         * the connection is made.
         */
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    //Create a server socket object and bind it to a port
                    ServerSocket localSocket = new ServerSocket(0);
                    DataTransmissionManager.setPort(localSocket.getLocalPort());

                    DataTransmissionManager.setPort(localSocket.getLocalPort());
                    Socket clientSocket = null;
                    //thread to check for connections will run continuously.
                    while (!DataTransmissionManager.connectionShouldClose()) {
                        //ServerControlThread serverControl = new ServerControlThread();
                        Log.e("C", "A");
                        clientSocket = localSocket.accept();
                        Log.e("C", "B");
                        ServerControlThread serverControl = new ServerControlThread(clientSocket);
                        Log.e("C", "C");
                        serverControl.start();
                        Log.e("C", "D");
                        //serverControl.execute(clientSocket);
                    }
                } catch (IOException e) {
                    Log.e("ERROR", e.toString());
                }
            }
        }).start();
    }

    //temporary class that is a simple thread, called internally
    class ServerControlThread extends Thread {
        Socket remoteSocket;
        public ServerControlThread(Socket _remoteSocket)
        {
            //Get the accepted socket object
            remoteSocket = _remoteSocket;
        }

        @Override
        public void run() {
            try {
                DataTransmissionManager.setPort(remoteSocket.getLocalPort());
                Log.d("CONNECTED", "READERS AND INPUTS ESTABLISHED");

                //lets data transmission manager know a connection has been made.
                DataTransmissionManager.setConnected(true);
                boolean stop = false;

                do {
                    //check if connection should close
                    stop = DataTransmissionManager.connectionShouldClose();
                    Log.e("", stop + "");
                    // get new messages to send
                    try {

                        String transmit = DataTransmissionManager.getMessagesToSend().removeFirst();
                        if(transmit != "")
                        {
                            outgoingSender = new PrintWriter(remoteSocket.getOutputStream(), true);
                            Log.d("WARN", "MESSAGE SENT SUCCESSFULLY");}
                        else
                        { throw new NoSuchElementException();}
                        outgoingSender.println(transmit);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NoSuchElementException e) {
                        Log.d("WARN", "TRIED TO SEND MESSAGE, NONE FOUND (THIS IS NORMAL)");
                    }

                    try {
                        InputStreamReader inputStreamReader = new InputStreamReader(remoteSocket.getInputStream());
                        incomingReader = new BufferedReader(inputStreamReader);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String message;


                    try {
                        if (incomingReader.ready()) {
                            message = incomingReader.readLine();
                            //remote has sent a message instructing connection to close.
                            if (message.equals("close")) {
                                //return close instruction
                                DataTransmissionManager.queueMessageForSending("close");
                                //exit loop, jump to close instructions.
                                break;

                            }
                            //add data to the queue of received messages.
                            DataTransmissionManager.receiveMessage(message);
                        }
                    } catch (IOException e) {

                        Log.d("ERROR", "IO ERROR");
                        Log.e("ERROR", e.toString());
                    }
                    //reduces cpu usage
                    Thread.sleep(100);

                }while (!stop);
                //close outgoing connection

                incomingReader.close();
                if (outgoingSender != null)
                {
                    outgoingSender.flush();
                    outgoingSender.close();
                    DataTransmissionManager.setConnected(false);
                    Log.d("Test", "Connection Closed");
                }

                //Close the client socket
                remoteSocket.close();
            } catch (IOException e) {
                Log.e("ERROR", e.toString());
            } catch (InterruptedException e) {
                Log.e("ERROR", e.toString());
            }
            //if the code gets this far, everything has worked as expected. Therefore god=true.
            //return true;
        }

    }
}