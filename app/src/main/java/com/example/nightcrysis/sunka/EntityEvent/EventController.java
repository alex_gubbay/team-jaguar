package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * The different Controllers can be used after another. This class allows such implementation
 * so that multiple Controller events can be executed in a linear order.
 *
 * Due to the nature of the arrayList (of type GameEntityInterface). It can be used in conjunction
 * with MessageFrames, and many other 'terminatable' GameEntityObjects
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class EventController implements GameEntityInterface{
    private ArrayList<GameEntityInterface> events;
    private boolean needsRemoval = false;

    /** Constructor for EventController. Instantiates the events arraylist. */
    public EventController()
    {
        events = new ArrayList<>();
    }

    /** Method for adding events
     * @param event Event object to be added  for execution */
    public void addEvent(GameEntityInterface event)
    {
        events.add(event);
    }

    /**
     * Update method.Only updates the first object in the events arraylist at position 0.
     * If the object needs removing, it will remove it and proceed on updating the second object.
     * If the events arraylist is empty, needsRemoval is set to true and will be removed by the
     * update cycle and the garbage collector.
     * @param delta The time elapsed since the last update cycle
     * @see GameEntityInterface
     */
    @Override
    public void update(float delta) {
        GameEntityInterface x = events.get(0);
        if (x.getNeedsRemoval())
            events.remove(0);
        else x.update(delta);

        if (events.size() == 0) needsRemoval = true;
    }

    /**
     * Rendering method. Renders the first object in the events array list.
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {
        if (needsRemoval) return;
        GameEntityInterface x = events.get(0);
         x.render(canvas);
    }

    /**
     * Method for detecting user input. It passes the user input to the first
     * object in the events array list
     * @param event The MotionEvent that the device has detected
     * @return true if the first object is touched, false otherwise.
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //Checks if the events array list has any elements, to prevent exceptions
        if (events.size() != 0)
            return events.get(0).onTouchEvent(event);
        else return false;
    }

    /**
     * Returns needsRemoval boolean value
     * @return needsRemoval as a boolean value
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
