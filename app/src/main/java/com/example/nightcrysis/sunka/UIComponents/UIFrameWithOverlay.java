package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;

/**
 * Created by NIghtCrysIs on 2015/10/27.
 * Same with UIFrame, but has a dark overlay image behind the UIFrame
 */
public class UIFrameWithOverlay extends UIFrame{

    protected ColorFade darkOverlay;
    protected boolean frameClosed = false, outsideFrameTouched = false;

    /**
     * Constructor for UIFrameWithOverlay. Sets the position and size of the frame.
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     * @see UIFrame
     */
    public UIFrameWithOverlay(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        darkOverlay = new ColorFade(true, 0.35f, 180, 0, 0, 0, false);
        SoundManager.playSound(R.raw.ui_transition2);
    }

    /**
     * Constructor for UIFrameWithOverlay but with frameSide padding added. Currently does nothing and is not used
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     * @param frameSpriteID
     * @param frameSidePadding
     * @deprecated because it does nothing and is not really used.
     * @see UIFrame
     */
    public UIFrameWithOverlay(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        SoundManager.playSound(R.raw.ui_transition2);
    }

    /**
     * Update method. Calls parent class' update method.
     * The darkOverlay is also updated if not needsRemoval is not true.
     * @param delta The time elapsed since the last update cycle
     * @see UIFrame
     * @see ColorFade
     */
    @Override
    public void update(float delta) {
        super.update(delta);
        darkOverlay.update(delta);

        if (frameClosed & darkOverlay.getNeedsRemoval())
            needsRemoval = true;
    }

    /**
     * Render method. Calls parent class' render method.
     * The darkOverlay is also rendered if not needsRemoval is not true.
     * Canvas object used to draw images on screen
     * @param canvas Canvas object used to draw images on screen
     * @see UIFrame
     * @see ColorFade
     */
    @Override
    public void render(Canvas canvas) {
        //Draws the overlay before anything else.
        darkOverlay.render(canvas);

        super.render(canvas);
    }

    /** Touch event method. Detects if the user has pressed outside the frame, in which
     * the frame will close. Calls parent class' onTouchEvent(MotionEvent event),
     * @param event The MotionEvent that the device has detected
     * @see UIFrame */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!super.onTouchEvent(event))
        {
            if ((event.getAction() == MotionEvent.ACTION_DOWN))
            {
                outsideFrameTouched = true;
            }
            if ((event.getAction() == MotionEvent.ACTION_UP) & outsideFrameTouched)
            {
                darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
                frameClosed = true;
            }
        }
        else if (event.getAction() == MotionEvent.ACTION_UP)
        {
            outsideFrameTouched = false;
        }
        // Always returns true because this UIFrame take precedence over all other components on screen.
        return true;
    }
}
