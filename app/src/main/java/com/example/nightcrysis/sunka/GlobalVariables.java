package com.example.nightcrysis.sunka;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;

import java.util.Random;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Class for global variables that are not associated with game settings in anyway
 * Variables declared in this class are used to calculate positions of the UI components programmatically
 * by setting their positions and size relative to the new ratio of the screen size.
 *
 * Default screen resolution of the game is 1500x1000, in which can be resized automatically in ImageManager
 * @see com.example.nightcrysis.sunka.Graphics.ImageManager
 */
public class GlobalVariables {
    public static int screenSizeX = 0, screenSizeY = 0; //Provides the screen size of the device

    public static float backgroundWidth, backgroundHeight, backgroundOffsetX, backgroundOffsetY, backgroundRatio;; //All graphics are calculated relative to the background

    public static Activity currentActivity = null; //Used for getResource() from methods outside MainActivity
    public static GameDisplay gameDisplay = null; //A reference to our gameDisplay
    public static UpdateCycle updateCycle = null; //A reference to the on-going UpdateCycle, initialized and starts in GameDisplay class
    //public static Typeface typeface = null; //Specifies a ttf font file, which can be used for custom font

    public static Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG); //Used to draw bitmaps (png, jpg etc.), shapes, strings and many more

    public static Random random = new Random();

    /**
     * Initialization method for setting the custom type face of the game.
     */
    public static void initialize()
    {
         //Sets the typeface for a custom font. Un-comment this if you want to use it. Place the custom font in an assets folder, in the same directory as res folder
        Typeface typeface = Typeface.createFromAsset(currentActivity.getResources().getAssets(), "font/JungleFeverNF.ttf");
        paint.setTypeface(typeface); //Sets the typeface (font file) for our Paint object
    }
}
