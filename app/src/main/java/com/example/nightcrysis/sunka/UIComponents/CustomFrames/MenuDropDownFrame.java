package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UICheckBox;
import com.example.nightcrysis.sunka.UIComponents.UIFrame;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

import static com.example.nightcrysis.sunka.GlobalVariables.backgroundHeight;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundOffsetX;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundOffsetY;
import static com.example.nightcrysis.sunka.GlobalVariables.backgroundWidth;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Custom frame for menu drop down during the game
 */
public class MenuDropDownFrame extends UIFrame{
    private UIButtonFrame leaveButton, settingsButton;
    private UICheckBox soundOn, musicOn;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float textSize;

    /**
     * Constructor for MenuDropDownFrame. Sets the size and position and instantiates
     * corresponding objects and variables.
     */
    public MenuDropDownFrame() {
        super(GlobalVariables.screenSizeX * 4 / 5, 0, GlobalVariables.screenSizeX / 5, GlobalVariables.screenSizeY / 3, R.drawable.frame1_unpressed);

        float buttonHeight = height / 4;

        leaveButton = new UIButtonFrame(x, y, width, buttonHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        leaveButton.setText("Leave");
        leaveButton.setTextSizeAuto(leaveButton.getWidth() / 6, leaveButton.getHeight() / 6);
        leaveButton.setTextColor(textColor);

        settingsButton = new UIButtonFrame(x, y + buttonHeight, width, buttonHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        settingsButton.setText("Settings");
        settingsButton.setTextSizeAuto(settingsButton.getWidth() / 6, settingsButton.getHeight() / 8);
        settingsButton.setTextColor(textColor);

        soundOn = new UICheckBox(x + width - (buttonHeight / 0.75f), y + (buttonHeight * 2.125f), buttonHeight / 1.5f, buttonHeight / 1.5f);
        soundOn.setValue(GameSettings.soundOn);
        musicOn = new UICheckBox(x + width - (buttonHeight / 0.75f), y + (buttonHeight * 3.125f), buttonHeight / 1.5f, buttonHeight / 1.5f);
        musicOn.setValue(GameSettings.musicOn);

        uiSubComponents.add(leaveButton);
        uiSubComponents.add(settingsButton);
        uiSubComponents.add(soundOn);
        uiSubComponents.add(musicOn);

        textSize = UIUtilities.findTextSizeByHeight("A", soundOn.getHeight() * 0.7f);
    }

    /**
     * Updates the MenuDropDownFrame
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        super.update(delta);
    }

    /**
     * Renders the MenuDropDownFrame
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText("Sound", x + soundOn.getWidth() / 2, soundOn.getY() + soundOn.getHeight(), GlobalVariables.paint);
        canvas.drawText("Music", x + musicOn.getWidth()/2, musicOn.getY() + musicOn.getHeight(), GlobalVariables.paint);
    }

    /**
     * User input method. Passes the user input to corresponding objects of the game.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrame
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (super.onTouchEvent(event))
        {
            if (leaveButton.isPressed())
            {
                needsRemoval = true;
                ScreenManager.addToMasterOverlay(new LeaveGameFrame());
            }
            else if (settingsButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                SettingsFrame tempFrame = new SettingsFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + (sidePadding / 2),
                        backgroundWidth - (sidePadding * 2), backgroundHeight - sidePadding, R.drawable.frame2_unpressed);
                ScreenManager.addToMasterOverlay(tempFrame);
                needsRemoval = true;
            }
            else if (soundOn.isPressed())
            {
                GameSettings.soundOn = soundOn.getValue();
                SoundManager.toggleSound(GameSettings.soundOn);
                GameSettings.saveSettings();
            }
            else if (musicOn.isPressed())
            {
                GameSettings.musicOn = musicOn.getValue();
                SoundManager.toggleMusic(musicOn.getValue());
                GameSettings.saveSettings();
            }
        }
        else if (event.getAction() == MotionEvent.ACTION_DOWN)
            needsRemoval = true;
        return true;
    }
}
