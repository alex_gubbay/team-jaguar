package com.example.nightcrysis.sunka.Screen;

import com.example.nightcrysis.sunka.GameEntity.TrayEasyAI;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Starts a new easy AI game.
 *
 * Extends {@link GameScreen}.
 *
 * @author florianaucomte
 */
public class EasyAIGameScreen extends GameScreen
{
    /**
     * Creates a new EasyAIGameScreen object. Creates a new {@link TrayEasyAI} object and adds it
     * to the game entities.
     */
    public EasyAIGameScreen()
    {
        float singlePotSize = GlobalVariables.screenSizeX/11; //Used for reference, total width of screen will be divided by 11
        tray = new TrayEasyAI(0, GlobalVariables.screenSizeY / 2 - singlePotSize, GlobalVariables.screenSizeX, singlePotSize * 2);
        gameEntities.add(tray);
    }
}
