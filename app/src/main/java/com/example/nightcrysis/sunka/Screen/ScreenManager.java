package com.example.nightcrysis.sunka.Screen;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Class for managing Screen instances.
 */
public class ScreenManager {
    public static boolean transitioning = false;
    private static Screen currentScreen = null; //The screen the is drawn on the Android device
    private static ScreenState currentState = null;
    private static ArrayList<GameEntityInterface> masterOverlay = new ArrayList<>(),
            masterOverlayToAdd = new ArrayList<>();
    public static boolean isHost;

    /**
     * Initialization method. Called once upon application starting (or resuming after onStop())
     */
    public static void initialize()
    {
        currentScreen = new MainMenuScreen();

        ScreenManager.transitioning = true;
        currentScreen.addToOverlay(new ColorFade(false, 1, true, true));
    }

    /**
     * Change screen method by fading in and out
     * @param screenState The new screen state to be transitioned to.
     * */
    public static void fadeOutTo(ScreenState screenState)
    {
        //Transitioning to a specified screen state
        EventController screenTransitionEvent = new EventController();
        screenTransitionEvent.addEvent(new ColorFade(true, 0.5f, false, true));
        screenTransitionEvent.addEvent(new ChangeScreenTimer(0, screenState, Color.BLACK));
        screenTransitionEvent.addEvent(new ColorFade(false, 0.5f, false, true));
        ScreenManager.addToMasterOverlay(screenTransitionEvent);
    }

    /** Changes currentScreen to the new screen specified by newScreenState instantaneously */
    public static void changeScreen(ScreenState newScreenState)
    {
        currentState = newScreenState;

        // The actual name of the class doesn't matter, as long you understand how it works.
        switch (newScreenState)
        {
            case mainMenu:currentScreen = new MainMenuScreen(); break;
            case localMultiplayerGame: currentScreen = new MultiplayerGameScreen(); break;
            case gameSelection: currentScreen = new GameSelectionScreen(); break;
            case aiGameEasy: currentScreen = new EasyAIGameScreen(); break;
            case aiGameMedium: currentScreen = new AIGameScreen(); break;
            case aiGameHard: currentScreen = new HardAIGameScreen(); break;
            case onlineMultiplayerGame: currentScreen = new OnlineGameScreen(isHost); break;
            case onlineMenu: currentScreen = new OnlineMenuScreen(); break;
            default: break;
        }
    }

    /**
     * Getter method for the current screenState
     * @return currentState
     * @see ScreenState
     */
    public static ScreenState getScreenState()
    {
        return currentState;
    }

    /**
     * Getter method for the current screen being displayed
     * @return currentScreen
     * @see Screen
     */
    public static Screen getCurrentScreen(){
        return currentScreen;
    }

    /**
     * Update method. Updates the current screen.
     * It then updates the masterOverlay array list, in which contains GameEntityInterface objects
     * @param delta
     * @see GameEntityInterface
     */
    public static void update(float delta)
    {
        currentScreen.update(delta);

        Iterator<GameEntityInterface> i = masterOverlay.iterator();
        while (i.hasNext())
        {
            GameEntityInterface tempGameEntity = i.next();
            if (tempGameEntity.getNeedsRemoval())
                i.remove();
        }

        ArrayList<GameEntityInterface> tempArrayList = new ArrayList<>(masterOverlay);
        for (GameEntityInterface temp : tempArrayList)
            temp.update(delta);

        masterOverlay.addAll(masterOverlayToAdd);
        masterOverlayToAdd.clear();
    }

    /**
     * Method for adding GameEntityInterface objects to masterOverlay
     * @param object    object to be added to master overlay
     * @see GameEntityInterface
     */
    public static void addToMasterOverlay(GameEntityInterface object)
    {
        masterOverlayToAdd.add(object);
    }

    /**
     * Render method. Renders the current screen.
     * It then renders the masterOverlay array list, in which contains GameEntityInterface objects
     * @param canvas    Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    public static void render(Canvas canvas)
    {
        currentScreen.render(canvas);

        ArrayList<GameEntityInterface> tempArrayList = new ArrayList<>(masterOverlay);
        for (GameEntityInterface temp : tempArrayList)
            temp.render(canvas);
    }

    /**
     * User input method. It passes the user input to the current screen.
     * It then passes the user input to the masterOverlay array list, in which contains GameEntityInterface objects
     * @param event The MotionEvent that the device has detected
     * @see GameEntityInterface
     */
    public static void onTouchEvent(MotionEvent event)
    {
        boolean touched = false;
        ArrayList<GameEntityInterface> tempArrayList = new ArrayList<>(masterOverlay);
        for (GameEntityInterface temp : tempArrayList)
            if (temp.onTouchEvent(event))
                touched = true;
        if (!touched)
            currentScreen.onTouchEvent(event);
    }
}