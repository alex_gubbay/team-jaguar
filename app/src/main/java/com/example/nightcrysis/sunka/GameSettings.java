package com.example.nightcrysis.sunka;

import android.util.Log;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.FileIO.SettingsManager;

/**
 * Created by NIghtCrysIs on 2015/10/27.
 * Static class for storing the in game settings for the game.
 */
public class GameSettings {
    public static boolean soundOn, musicOn, fancyAnimationOn, hintsOn;
    public static int soundVolume, musicVolume;

    /**
     * Initializes the game settings by reading the settings from SettingsManager
     * @see SettingsManager
     */
    public static void initialize()
    {
        //Load configurations from a stored file if exists
        SettingsManager manager = new SettingsManager();
        soundOn = manager.getSoundOn();
        soundVolume = manager.getSoundVolume();
        musicVolume = manager.getMusicVolume();
        musicOn = manager.getMusicOn();
        fancyAnimationOn = manager.getAnimationOn();
        hintsOn = manager.getHintsOn();

        Log.e("", "" + musicOn);

        //Used to play back ground music
        if (musicOn)
            SoundManager.playBGM(R.raw.bgm2);

        SoundManager.setMusicVolume(musicVolume);
    }

    /**
     * Public method for saving the settings to the preference file through SettingsManager
     * @see SettingsManager
     */
    public static void saveSettings()
    {
        SettingsManager manager = new SettingsManager();
        manager.saveSettings(soundOn, soundVolume, musicOn, musicVolume, fancyAnimationOn, hintsOn);
    }
}
