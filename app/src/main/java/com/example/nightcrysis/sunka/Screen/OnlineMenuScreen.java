package com.example.nightcrysis.sunka.Screen;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.EntityEvent.TimerEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.HostGameFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.JoinGameFrame;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.UIComponents.UIButton;

import static com.example.nightcrysis.sunka.GlobalVariables.*;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * Online menu screen for the game.Shows the available options for playing online
 * Host, join and back buttons are provided
 *
 * Extends Screen
 */
public class OnlineMenuScreen extends Screen {
    private boolean menuDown = false;
    private SpriteEntity titleName, ropes;
    private UIButton hostGameButton, joinGameButton, backButton;

    /**
     * Constructor for OnlineMenuScreen. Instantiates variables and objects to their
     * respective positions, sizes, etc.
     */
    public OnlineMenuScreen()
    {
        //Setting ratio for ropes and buttons
        float ratio = (backgroundWidth * 0.45f) / ImageManager.getBitmapWidth(R.drawable.rope),

                //Configuring size and position for ropes
                tempWidth = ImageManager.getBitmapWidth(R.drawable.rope3) * ratio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.rope3) * ratio;

        ropes = new SpriteEntity(screenSizeX/2 - (tempWidth/2), backgroundOffsetY - (70 * backgroundRatio) - tempHeight,
                tempWidth, tempHeight, R.drawable.rope3);

        // Instantiating, setting text, and adding to screen cycle for each button.
        hostGameButton = new UIButton(ropes.getX(), ropes.getY() + (354 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.online_menu_host) * ratio, ImageManager.getBitmapHeight(R.drawable.online_menu_host) * ratio,
                R.drawable.online_menu_host, R.drawable.online_menu_host_pressed);
        userInterface.add(hostGameButton);

        joinGameButton = new UIButton(ropes.getX(), hostGameButton.getY() + (120 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.online_menu_join) * ratio, ImageManager.getBitmapHeight(R.drawable.online_menu_join) * ratio,
                R.drawable.online_menu_join, R.drawable.online_menu_join_pressed);
        userInterface.add(joinGameButton);

        backButton = new UIButton(ropes.getX(), joinGameButton.getY() + (120 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_back) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_back) * ratio,
                R.drawable.main_menu_back, R.drawable.main_menu_back_pressed);
        userInterface.add(backButton);

        userInterface.add(ropes);

        //Setting ratio for Sunka title
        ratio = (backgroundWidth * 0.7f) / ImageManager.getBitmapWidth(R.drawable.title_online_game);

        //Configuring size and position for title
        tempWidth = ImageManager.getBitmapWidth(R.drawable.title_online_game) * ratio;
        tempHeight = ImageManager.getBitmapHeight(R.drawable.title_online_game) * ratio;

        titleName = new SpriteEntity(screenSizeX/2 - (tempWidth/2), backgroundOffsetY - ropes.getHeight() + (50 * backgroundRatio) ,
                tempWidth, tempHeight, R.drawable.title_online_game);
        userInterface.add(titleName);


        SoundManager.playSound(R.raw.ui_transition1);
    }

    /**
     * Update method. Makes a check to is the menu is down,
     * if not, it will enterScreen() will be called to drop the
     * menu down once.
     * Calls MainBackground.update to update the cloud positions
     * @param delta    time passed since last update cycle
     * @see MainBackground
     * @see Screen
     */
    @Override
    public void update(float delta) {
        MainBackground.update(delta);

        if (!menuDown & !ScreenManager.transitioning)
        {
            enterScreen();
            menuDown = true;
        }

        super.update(delta);
    }

    /**
     * Rendering method. Renders MainBackground's image and clouds,
     * and makes a call to parent class' render method
     * @param canvas    Canvas object used to draw images on screen
     * @see MainBackground
     * @see Screen (parent)
     */
    @Override
    public void render(Canvas canvas) {
        MainBackground.render(canvas);
        super.render(canvas);
    }

    private void enterScreen()
    {
        float displacement = ropes.getHeight();

        //Moves menu down
        underlay.add(new PositionEvent(titleName, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(ropes, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(hostGameButton, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(joinGameButton, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(backButton, 0, displacement, 0.8f, true, false));

        EventController eventController = new EventController();
        eventController.addEvent(new TimerEvent(0.5f));
        underlay.add(eventController);
    }

    private void changeScreen(ScreenState transitionTo)
    {
        //Moves the entire menu up (accelerated movement)
        float initVelocityY = hostGameButton.getHeight() * 2,
                accelerationY = -AccelerationEvent.accelYOffScreenValueUp(0, ropes.getHeight(), initVelocityY, 0.8f);

        underlay.add(new AccelerationEvent(titleName, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(ropes, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(hostGameButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(joinGameButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(backButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));

        hostGameButton.setEnabled(false);
        joinGameButton.setEnabled(false);
        backButton.setEnabled(false);

        //Adds a timer for screen transition
        underlay.add(new ChangeScreenTimer(1, transitionTo));
    }

    /**
     * User input method. Checks collision between input and buttons, then
     * makes calls to respective pressed buttons.
     * @param event    The MotionEvent that the device has detected
     */
    @Override
    public void onTouchEvent(MotionEvent event)
    {
        super.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            if (hostGameButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                HostGameFrame tempFrame = new HostGameFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + sidePadding,
                        backgroundWidth - (sidePadding * 2), backgroundHeight - (sidePadding * 2), R.drawable.frame2_unpressed);
                addToUserInterface(tempFrame);
                ScreenManager.isHost = true;
            }
            else if (joinGameButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                JoinGameFrame tempFrame = new JoinGameFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + sidePadding * 1.5f,
                        backgroundWidth - (sidePadding * 2), backgroundHeight - (sidePadding * 3), R.drawable.frame2_unpressed);
                addToUserInterface(tempFrame);
                ScreenManager.isHost = false;
            }
            else if (backButton.isPressed())
                changeScreen(ScreenState.gameSelection);
        }

    }
}
