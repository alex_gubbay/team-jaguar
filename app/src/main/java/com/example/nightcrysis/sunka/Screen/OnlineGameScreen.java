package com.example.nightcrysis.sunka.Screen;

import com.example.nightcrysis.sunka.GameEntity.TrayOnline;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Starts a new online multiplayer game.
 *
 * Extends {@link GameScreen}.
 *
 * @author florianaucomte
 */
public class OnlineGameScreen extends GameScreen
{
    private boolean isHost;

    /**
     * Creates a new OnlineGameScreen object. Creates a {@link TrayOnline} object, which can vary
     * depending on whether the user is the host of the online game or not.
     *
     * @param _isHost True if the user is the host, false otherwise.
     */
    public OnlineGameScreen(boolean _isHost)
    {
        isHost = _isHost;

        float singlePotSize = GlobalVariables.screenSizeX/11; //Used for reference, total width of screen will be divided by 11
        tray = new TrayOnline(0, GlobalVariables.screenSizeY / 2 - singlePotSize, GlobalVariables.screenSizeX, singlePotSize * 2, isHost);
        gameEntities.add(tray);
    }
}