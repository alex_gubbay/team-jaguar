package com.example.nightcrysis.sunka.FileIO;

import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Utility class used to record the duration of various events in the game
 * Created by Alex on 03/11/2015.
 */
public class moveTimer {

    private boolean gameEnded = false, gameStarted = false;
    private long gameTimeStart, gameTimeEnd;
    private HashMap <String, Long> timerEvents;
    private HashMap <String, Long> deadEvents;

    /**
     * Constructor for moveTimer
     */
public moveTimer(){

    timerEvents = new HashMap<String, Long>();
    deadEvents = new HashMap<String, Long>();
}

    /**
     * Starts the game timer running. Once started, it can be stopped by calling the stopGame
     * method.
     */
    public void startGame(){
        gameStarted = true;
        gameTimeStart =  System.currentTimeMillis();
    }

    /**
     *
     * @return the total time in seconds since the the gameTimer was started.
     */
    public long getGameElapsedTimeSeconds(){
        if (!gameStarted) return 0;
        if (gameEnded) return calculateSeconds(gameTimeEnd - gameTimeStart);

        long elapsedTime = System.currentTimeMillis() - gameTimeStart;
        long elapsedTimeSeconds = calculateSeconds(elapsedTime);

        return elapsedTimeSeconds;

    }

    /**
     * Creates and starts a timer for a new event. Event is defined by a string. Two events
     * cannot have the same string.
     * @param eventName name to identify the event by. Unique.
     */
    public void startNewEvent(String eventName){
        timerEvents.put(eventName, System.currentTimeMillis());
    }

    /**
     * Returns the time since a specified event was started.
     * @param eventName Name string of the event to get the time for.
     * @return time since the event was started.
     */
    public long getEventTimeSeconds(String eventName){
        long elapsedTime = System.currentTimeMillis() - timerEvents.get(eventName);
        long elapsedTimeSeconds = calculateSeconds(elapsedTime);

        return elapsedTimeSeconds;
    }

    /**
     * Stops the timer on a previously created event. Once an event is killed it cannot be
     * recreated. The age of the event when killed will be available by calling getDeadEventSeconds,
     * and specifying the name of the event.
     * @param eventName name of event to kill.
     */
    public void endEvent(String eventName){
       long finalTime = System.currentTimeMillis() - timerEvents.remove(eventName);
        Log.d("PRINT",finalTime+" final time for object");
        deadEvents.put(eventName, finalTime);

    }

    /**
     * Get the age of an event when it was killed. The age will not increase.
     * @param eventName name of dead event.
     * @return age of dead event at time of death.
     */
    public long getDeadEventSeconds(String eventName){

        long time = calculateSeconds(deadEvents.get(eventName));
        return time;
    }


    private long calculateSeconds(long milisTime){
        long elapsedTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(milisTime);

        return elapsedTimeSeconds;
    }

    /**
     * Stops the game timer, if it is running.
     */
    public void stopGame()
    {
        gameEnded = true;
        gameTimeEnd = System.currentTimeMillis();
    }
}
