package com.example.nightcrysis.sunka.GameEntity;

import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.R;

/**
 * Created by tuffail on 21/10/15.
 * Adds onto the pot class by having a reference to the opposite pot for the small pot
 */
public class SmallPot extends Pot {

    SmallPot oppositePot;

    /**
     * Takes the top left coordinate of the small pot along with its attributes
     * @param _x X coordinate of small pot
     * @param _y Y coordinate of the small pot
     * @param _width Width of the small pot
     * @param _height Height of the small pot
     */
    public SmallPot(float _x, float _y, float _width, float _height){
        super(_x,_y,_width,_height);

        sprite = new Sprite(x, y, width, height, R.drawable.small_hole);
        highlightedSprite1 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_1);
        highlightedSprite2 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_2);
        highlightedSprite3 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_3);
        highlightedSprite4 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_4);
    }

    /**
     * Sets the pot opposite to the current small pot
     * @param _oppositePot opposite pot to the current small pot
     */
    public void setOppositePot(SmallPot _oppositePot){
        oppositePot = _oppositePot;
    }

    /**
     * Returns the opposite pot to the current small pot
     * @return opposite pot to the current small pot
     */
    public SmallPot getOppositePot(){
        return oppositePot;
    }
}
