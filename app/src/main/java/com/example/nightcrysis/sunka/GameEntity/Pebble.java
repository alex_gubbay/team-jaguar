package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Graphics.Animation;
import com.example.nightcrysis.sunka.Graphics.Sprite;

/**
 * Created by tuffail on 21/10/15.
 * Represents a pebble for the pot
 */
public class Pebble extends GameEntity{
    /**
     *Takes the top left coordinate of the pebble along with its attributes
     * @param _x X coordinate for the pebble
     * @param _y Y coordinate for the pebble
     * @param _width Width of the pebble
     * @param _height Height of the pebble
     * @param animationResId Resource location of the image of the pebble
     */
    public Pebble(float _x, float _y, float _width, float _height, int animationResId)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

       sprite = new Sprite(x, y, width, height, animationResId);
    }

    /**
     * Overrides the onTouchEvent method
     * @return returns @code false as you cannot touch a pebble in the game
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
