package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataTransmissionManager;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Leave game confirmation when the player has pressed leave game from the side menu
 */
public class LeaveGameFrame extends UIFrameWithOverlay {
    private UIButtonFrame yesButton, noButton;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float sidePadding, textWidth, textHeight;
    private String message = "Are you sure you want to leave?";

    private static float textSize = -1;

    /** Constructor for LeaveGameFrame */
    public LeaveGameFrame() {
        super(0, 0, 0, 0, R.drawable.frame2_unpressed);
        initialize();
    }

    /** Initialization method for instantiating objects and settings different variables */
    private void initialize()
    {
        if (textSize < 0)
        {
            textSize = UIUtilities.findTextSizeByHeight("A", GlobalVariables.backgroundHeight / 32);
        }
        sidePadding = GlobalVariables.backgroundHeight / 16;

        //Set the text size, as well as its width and height
        setTextSize();

        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 6);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), -height);

        float tempWidth = GlobalVariables.backgroundWidth / 5,
        tempHeight = height / 7;

        yesButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        yesButton.setText("Yes");
        yesButton.setTextColor(textColor);

        noButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        noButton.setText("No");
        noButton.setTextColor(textColor);

        uiSubComponents.add(yesButton);
        uiSubComponents.add(noButton);

        float displacement = (GlobalVariables.screenSizeY / 2) + (height * 0.5f);
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(yesButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(noButton, 0, displacement, 0.5f, true, false));
    }

    /** Setter method for text Color
     * @param color Color of text, passed as an integer */
    public void setTextColor(int color)
    {
        textColor = color;
    }

    /** Setter method for text size. Called internally */
    private void setTextSize()
    {
        GlobalVariables.paint.setTextSize(textSize);
        Rect rect = new Rect();
        GlobalVariables.paint.getTextBounds(message, 0, message.length(), rect);
        textWidth = rect.width();
        textHeight = rect.height();
    }

    /** Render method, Renders the frame, buttons and the text message
     * @param canvas Canvas object for drawing
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(message, x + (width / 2) - (textWidth / 2), y + (height / 2) - (textHeight / 2), GlobalVariables.paint);
    }

    /**
     * User input method. Passes the user input to corresponding objects of the game.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrameWithOverlay
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (super.onTouchEvent(event))
        {
            if (yesButton.isPressed())
            {
                frameClosed = true;
                DataTransmissionManager.stopConnection();
                ScreenManager.fadeOutTo(ScreenState.mainMenu);
            }
            else if (noButton.isPressed())
            {
                frameClosed = true;
            }
        }

        if (frameClosed & !needsRemoval)
        {
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f) * 2,
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(yesButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(noButton, 0, velocityY, 0, acceleration, 0.5f, true));

            yesButton.setEnabled(false);
            noButton.setEnabled(false);
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
        }

        return true;
    }
}
