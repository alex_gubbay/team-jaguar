package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Created by NIghtCrysIs on 2015/10/21.
 * Temporary text to be shown on screen, with a specified duration.
 * Used in early stages of development, as placeholders for MessageDialogs planned in game.
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class TemporaryText extends GameEntity{
    private String text;
    private int color = Color.BLACK;
    private int textSize = 32;
    private float duration = 0, textHeight = 0;

    /** Constructor for TemporaryText.
     * @param _x                 X position of the text to be displayed
     * @param _y                 Y position of the text to be displayed
     * @param _text              The text to be shown on screen
     * @param _durationInSeconds The total duration of the text's display */
    public TemporaryText(int _x, int _y, String _text, float _durationInSeconds)
    {
        x = _x;
        y = _y;
        text = _text;
        duration = _durationInSeconds;
        Rect bounds = new Rect();
        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.getTextBounds(text, 0, text.length(), bounds);
        textHeight = bounds.height();
    }

    /** Constructor for TemporaryText. This version includes the textsize and color as the parameter
     * @param _x                 X position of the text to be displayed
     * @param _y                 Y position of the text to be displayed
     * @param _text              The text to be shown on screen
     * @param _durationInSeconds The total duration of the text's display
     * @param _textSize The size of text shown.
     * @param _color Takes an integer value, as referenced from Android's Color class. */
    public TemporaryText(int _x, int _y, String _text, float _durationInSeconds, int _textSize, int _color)
    {
        x = _x;
        y = _y;
        text = _text;
        duration = _durationInSeconds;
        textSize = _textSize;
        color = _color;
        Rect bounds = new Rect();
        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.getTextBounds(text, 0, text.length(), bounds);
        textHeight = bounds.height();
    }

    /**
     * Setter method for the text color
     * @param _color    Color, and integer value
     */
    public void setColor(int _color)
    {
        color = _color;
    }

    /**
     * Setter method for text size
     * @param _textSize    Text size of message to be displayed
     */
    public void setTextSize(int _textSize)
    {
        textSize = _textSize;
        Rect bounds = new Rect();
        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.getTextBounds(text, 0, text.length(), bounds);
        textHeight = bounds.height();
    }

    /**
     * Setter method for setting the text
     * @param _text    New text to be displayed
     */
    public void setText(String _text)
    {
        text = _text;
    }

    @Override
    public void update(float delta) {
        duration -= delta;
        if (duration <= 0)
            needsRemoval = true;
    }

    /**
     * Rendering method. Renders the text by its specified text size (if any), and color
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntity
     */
    @Override
    public void render(Canvas canvas) {
        GlobalVariables.paint.setTextSize(textSize); //Changes the rendered text size.
        GlobalVariables.paint.setColor(color); //Changes the color of paint
        canvas.drawText(text, x, y + textHeight, GlobalVariables.paint);
    }

    /**
     * Getter method for needsRemoval
     * @param event The MotionEvent that the device has detected
     * @return false because nothing needs to be done
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
