package com.example.nightcrysis.sunka.GameEntity;

import android.util.Log;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.R;

import java.util.ArrayList;

/**
 * Created by tuffail on 06/11/15.
 * Used to determine and perform a move for the hard AI on a separate tread
 */
public class HardAIMove extends Thread {

    VirtualStateTray virtualTray;
    TrayHardAI actualTray;

    /**
     * Records references to the cloned tray to exmaine moves on and the actual tray to perform the final move on
     * @param _virtualTray The cloned tray to examine moves of the AI on
     * @param _actualTray The actual tray to perform the final move on
     */
    public HardAIMove(VirtualStateTray _virtualTray, TrayHardAI _actualTray){
        virtualTray = _virtualTray;
        actualTray = _actualTray;
    }

    /**
     * Starts the thread then determines and distributes the small pot that has been selected by the hard AI
     */
    public void run(){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int index = getAIMoveIndex();
        if (index >= 7 && index <= 13){
            SmallPot potToClick = actualTray.smallPots[index];
            potToClick.highlightConsecutivePots(true);
            if (potToClick.getPebbleCount() != 0){
                actualTray.waitTime = 150;
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                potToClick.highlightConsecutivePots(false);
                actualTray.distributePebbles(potToClick);
                actualTray.toggleLockedPots();
                actualTray.AIThinking = false;
            }
        }
    }

    private int getGameScore(VirtualStatePot[] bigPots){
        int number = bigPots[1].getPebbles() - bigPots[0].getPebbles();
        return number;
    }

    private int getAIMoveIndex(){
        TrayHardAI.AINextPot = minimax(virtualTray);
        return TrayHardAI.AINextPot;
    }

    private int minimax(VirtualStateTray tray){
        ArrayList<VirtualStateTray> AImoves = new ArrayList<>();
        ArrayList<Integer> bestValues = new ArrayList<>();
        int bestMove = Integer.MIN_VALUE;
        int index = -1;

        //Generate all possible moves for the AI
        for (int i = tray.smallPots.length/2;i < tray.smallPots.length; ++i){
            VirtualStateTray clonedTray = new VirtualStateTray(tray);
            clonedTray.AIDistributePebbles(clonedTray.smallPots[i]);
            AImoves.add(clonedTray);
        }

        //Check if any of the moves give the AI another move
        for (int i = 0; i < AImoves.size(); ++i){
            if (AImoves.get(i).currentPlayer == Player.B && tray.smallPots[i+7].getPebbles() != 0){
                index = i + 7;
                bestMove = Integer.MAX_VALUE;
            }
        }

        //If extra turn found return it
        if (bestMove == Integer.MAX_VALUE){
            return index;
        }

        //Generate Players move after AIs move
        for (int i = 0; i < AImoves.size(); ++i){
            int bestValue = Integer.MAX_VALUE;
            for (int j = 0; j < AImoves.get(i).smallPots.length/2; ++j){
                VirtualStateTray clonedTray = new VirtualStateTray(AImoves.get(i));
                clonedTray.AIDistributePebbles(clonedTray.smallPots[j]);

                int value = getGameScore(clonedTray.bigPots);

                if (value <= bestValue){
                    bestValue = value;
                }
            }
            if (bestValue > bestMove && tray.smallPots[i+7].getPebbles() != 0){
                index = i+7;
                bestMove = bestValue;
            }
        }
        return index;
    }

}