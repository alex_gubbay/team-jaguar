package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * Class for waiting delays, used in conjunction with EventController nad other event classes
 * to achieve certain desired effects
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class TimerEvent implements GameEntityInterface {
    private float duration;
    private boolean needsRemoval = false;

    /**
     * Constructor for the TimerEvent.
     * @param _duration    The amount of time to wait
     */
    public TimerEvent(float _duration)
    {
        duration = _duration;
    }

    /**
     * Update method. Reduces the duration by variable length delta at every call.
     * When the duration becomes <= 0, needsRemoval becomes true.
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        duration -= delta;
        if (duration <= 0) needsRemoval = true;
    }

    /**
     * Rendering method. Does nothing
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {

    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
