package com.example.nightcrysis.sunka.FileIO;

import android.content.SharedPreferences;
import android.util.Log;

import com.example.nightcrysis.sunka.GlobalVariables;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alex on 21/10/2015.
 * Manages the IO of data to persistent storage, for the display of scores and other player metrics
 * Manages player data types and others.
 */
public class DataManager {

    SharedPreferences players;
    Set<String> playerSet;
    String PREFS_NAME = "playerFile";


    /**
     * Constructor for DataManager
     */
    public DataManager() {

        players = GlobalVariables.currentActivity.getSharedPreferences(PREFS_NAME, 0);
        playerSet = players.getStringSet("players", null);
    }

    /**
     * @return An arraylist of player information that has been saved to file.
     */
    public ArrayList<GamePlayer> getAllPlayers() {

        ArrayList<GamePlayer> gamePlayerArrayList = new ArrayList<GamePlayer>();


        if (!(playerSet == null)) {

            for (String aPlayerSet : playerSet) {
                String[] values =
                        aPlayerSet.split(",");
                //create a new player for each entry in the storage.
                gamePlayerArrayList.add(
                        new GamePlayer(values[0], Long.valueOf(values[1]),
                                Integer.valueOf(values[2]),
                                Integer.valueOf(values[3]),
                                Integer.valueOf(values[4]),
                                Integer.valueOf(values[5]),
                                Integer.valueOf(values[6])));
            }
        }
        return gamePlayerArrayList;


    }

    /**
     * Searches for and returns a player object with the matching name string. If not found will
     * create and return new blank player object.
     * @param name name of player object to return
     * @return the player object with the matching name. If not found, will return a new
     * empty template player object, with the name set to the input of this method.
     */
    public GamePlayer getPlayer(String name) {

        ArrayList<GamePlayer> gamePlayerArrayList = this.getAllPlayers();
        GamePlayer template = new GamePlayer(name, 0, 0, 0, 0, 0, 0);
        if (gamePlayerArrayList == null) {
            //no players exist yet return a blank player
            this.addPlayer(template);
            return template;
        } else {
            //check existing players
            for (int i = 0; i < gamePlayerArrayList.size(); i++) {
                if (gamePlayerArrayList.get(i).getName().equals(name)) {
                    //player found

                    return gamePlayerArrayList.get(i);
                }
            }
            //player not found
        }

        return template;
    }


    /**
     *
     * @param gamePlayer adds a new gamePlayer to the file. If a player with the same name exists
     *                   already, it will be overwritten with the new player object.
     */
    public void addPlayer(GamePlayer gamePlayer) {
        Log.d("Pint", "ADD PLAYER CALLED");

        SharedPreferences.Editor editor = players.edit();
        ArrayList<GamePlayer> gamePlayerArrayList = this.getAllPlayers();


        playerSet = players.getStringSet("players", null);

        //Create an entire new set, opposed to a reference.
        if (playerSet != null)
        {
            playerSet = new HashSet<String>(playerSet);
        }

        //If no players are saved
        if (playerSet == null) {
            //first time adding a player, create a new hashSet to store.
            playerSet = new HashSet<String>();

            //add player to the hashSet.
            playerSet.add(gamePlayer.toString());
            gamePlayerArrayList.add(gamePlayer);

        } else {

            if (gamePlayerArrayList.size() == 0) {
                playerSet.add(gamePlayer.toString());
            } else {
                //we have to store every item every time. Not sure of a way around this.

                //not the first addition.
                boolean playerFound = false;
                for (int i = 0; i < gamePlayerArrayList.size(); i++) {
                    if (gamePlayerArrayList.get(i).getName().equals(gamePlayer.getName())) {
                        //player already exists in the storage

                        //remove and replace it with modified player
                        gamePlayerArrayList.set(i, gamePlayer);

                        //Re-save all players there is in the array list to the playerSet
                        playerSet.clear();
                        for (GamePlayer tempPlayer : gamePlayerArrayList)
                            playerSet.add(tempPlayer.toString());
                        playerFound = true;
                        break;
                    }
                }

                if (!playerFound)
                {
                    //Player doesn't exist in hash set, so it is added as a new entry.
                    playerSet.add(gamePlayer.toString());
                }


            }


        }

        //commit updated list to storage.
        editor.putStringSet("players", playerSet);
        editor.commit();
    }




    /**
     * Permanently erases the player data from storage.
     */
    public void clearPlayers() {
        SharedPreferences.Editor editor = players.edit();
        //Remove everything associated with the following String
        editor.remove("players");

        editor.commit();
    }

    /**
     * Persistently saves the arraylist of strings to file, in a file with name argument.
     * @param name name of file to save to. If not found it is created.
     * @param input ArrayList to save.
     */
    public void saveData(String name, ArrayList<String> input) {

        HashSet<String> values = new HashSet<String>();
        for (int i = 0; i < input.size(); i++) {
            values.add(input.get(i));
        }
        players.edit().putStringSet(name, values).commit();

    }

    /**
     * returns data stored in file.
     * @param name of file to extract data from.
     * @return arrayList of strings in file.
     */
    public ArrayList<String> getData(String name) {
        ArrayList<String> data = new ArrayList<>();

        Set<String> set = players.getStringSet(name, null);
        if (set != null) {


            for (String aSet : set) {
                data.add(aSet);
            }
        }
        return data;
    }
}
