package com.example.nightcrysis.sunka.Screen;

import com.example.nightcrysis.sunka.GameEntity.TrayAI;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Starts a new medium AI game.
 *
 * Extends {@link GameScreen}.
 *
 * @author florianaucomte
 */
public class AIGameScreen extends GameScreen
{
    /**
     * Creates anew AIGameScreen object. Creates a new {@link TrayAI} object and adds it to the
     * game entities.
     */
    public AIGameScreen()
    {
        float singlePotSize = GlobalVariables.screenSizeX/11; //Used for reference, total width of screen will be divided by 11
        tray = new TrayAI(0, GlobalVariables.screenSizeY / 2 - singlePotSize, GlobalVariables.screenSizeX, singlePotSize * 2);
        gameEntities.add(tray);
    }
}
