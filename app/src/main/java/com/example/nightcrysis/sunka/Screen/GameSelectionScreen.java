package com.example.nightcrysis.sunka.Screen;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.EntityEvent.TimerEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.AiSelectionFrame;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.UIComponents.UIButton;


/**
 * Created by Alex on 25/10/2015.
 * Game Selection screen for the game.Shows the available options for Single player (AI) game,
 * local multiplayer (multiplayer) game, and online multiplayer (online) game.
 *
 * Extends Screen
 * @author and edited by NIghtCrysIs
 */
public class GameSelectionScreen extends Screen
{
    private boolean menuDown = false;
    private SpriteEntity titleName, ropes;
    private UIButton singlePlayerButton, multiplayerButton, onlineGameButton, backButton;

    /**
     * Constructor for GameSelectionScreen. Instantiates variables and objects to their
     * respective positions, sizes, etc.
     */
    public GameSelectionScreen()
    {

        //Setting ratio for ropes and buttons
        float ratio = (GlobalVariables.backgroundWidth * 0.45f) / ImageManager.getBitmapWidth(R.drawable.rope),

                //Configuring size and position for ropes
                tempWidth = ImageManager.getBitmapWidth(R.drawable.rope2) * ratio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.rope2) * ratio;

        ropes = new SpriteEntity(GlobalVariables.screenSizeX/2 - (tempWidth/2), GlobalVariables.backgroundOffsetY - (70 * GlobalVariables.backgroundRatio) - tempHeight,
                tempWidth, tempHeight, R.drawable.rope2);

        // Instantiating, setting text, and adding to screen cycle for each button.
        singlePlayerButton = new UIButton(ropes.getX(), ropes.getY() + (354 * GlobalVariables.backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.play_menu_single_player) * ratio, ImageManager.getBitmapHeight(R.drawable.play_menu_single_player) * ratio,
                R.drawable.play_menu_single_player, R.drawable.play_menu_single_player_pressed);
        userInterface.add(singlePlayerButton);

        multiplayerButton = new UIButton(ropes.getX(), singlePlayerButton.getY() + (120 * GlobalVariables.backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.play_menu_multiplayer) * ratio, ImageManager.getBitmapHeight(R.drawable.play_menu_multiplayer) * ratio,
                R.drawable.play_menu_multiplayer, R.drawable.play_menu_multiplayer_pressed);
        userInterface.add(multiplayerButton);

        onlineGameButton = new UIButton(ropes.getX(), multiplayerButton.getY() + (120 * GlobalVariables.backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.play_menu_online) * ratio, ImageManager.getBitmapHeight(R.drawable.play_menu_online) * ratio,
                R.drawable.play_menu_online, R.drawable.play_menu_online_pressed);
        userInterface.add(onlineGameButton);

        backButton = new UIButton(ropes.getX(), onlineGameButton.getY() + (120 * GlobalVariables.backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_back) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_back) * ratio,
                R.drawable.main_menu_back, R.drawable.main_menu_back_pressed);
        userInterface.add(backButton);

        userInterface.add(ropes);

        //Setting ratio for Sunka title
        ratio = (GlobalVariables.backgroundWidth * 0.8f) / ImageManager.getBitmapWidth(R.drawable.sunka_title);

        //Configuring size and position for title
        tempWidth = ImageManager.getBitmapWidth(R.drawable.title_game_selection) * ratio;
        tempHeight = ImageManager.getBitmapHeight(R.drawable.title_game_selection) * ratio;

        titleName = new SpriteEntity(GlobalVariables.screenSizeX/2 - (tempWidth/2), GlobalVariables.backgroundOffsetY + (50 * GlobalVariables.backgroundRatio) - ropes.getHeight(),
                tempWidth, tempHeight, R.drawable.title_game_selection);
        userInterface.add(titleName);

        SoundManager.playSound(R.raw.ui_transition1);
    }

    /**
     * Update method. Makes a check to is the menu is down,
     * if not, it will enterScreen() will be called to drop the
     * menu down once.
     * Calls MainBackground.update to update the cloud positions
     * @param delta    time passed since last update cycle
     * @see MainBackground
     * @see Screen
     */
    @Override
    public void update(float delta) {
        MainBackground.update(delta);

        if (!menuDown & !ScreenManager.transitioning)
        {
            enterScreen();
            menuDown = true;
        }

        super.update(delta);
    }

    /**
     * Rendering method. Renders MainBackground's image and clouds,
     * and makes a call to parent class' render method
     * @param canvas    Canvas object used to draw images on screen
     * @see MainBackground
     * @see Screen (parent)
     */
    @Override
    public void render(Canvas canvas) {
        MainBackground.render(canvas);
        super.render(canvas);
    }

    /**
     * enterScreen method makes calls to PositionEvent for all related on screen entities
     * to make them drop down on screen.
     */
    private void enterScreen()
    {
        float displacement = ropes.getHeight();

        //Moves menu down
        underlay.add(new PositionEvent(titleName, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(ropes, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(singlePlayerButton, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(multiplayerButton, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(onlineGameButton, 0, displacement, 0.8f, true, false));
        underlay.add(new PositionEvent(backButton, 0, displacement, 0.8f, true, false));

        EventController eventController = new EventController();
        eventController.addEvent(new TimerEvent(0.5f));
        underlay.add(eventController);
    }

    private void changeScreen(ScreenState transitionTo)
    {
        //Moves the entire menu up (accelerated movement)
        float initVelocityY = singlePlayerButton.getHeight() / 2,
                accelerationY = (-ropes.getHeight() - (0.8f * initVelocityY)) / 0.32f;

        underlay.add(new AccelerationEvent(titleName, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(ropes, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(singlePlayerButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(multiplayerButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(onlineGameButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        underlay.add(new AccelerationEvent(backButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));

        singlePlayerButton.setEnabled(false);
        multiplayerButton.setEnabled(false);
        onlineGameButton.setEnabled(false);
        backButton.setEnabled(false);

        //Adds a timer for screen transition
        underlay.add(new ChangeScreenTimer(1, transitionTo));
    }

    /**
     * User input method. Checks collision between input and buttons, then
     * makes calls to respective pressed buttons.
     * @param event    The MotionEvent that the device has detected
     */
    @Override
    public void onTouchEvent(MotionEvent event)
    {
        super.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            if (singlePlayerButton.isPressed())
            {
                /*changeScreen(ScreenState.aiGame);
                addToOverlay(new ColorFade(true, 0.8f, false));*/
                float tempWidth = GlobalVariables.backgroundWidth / 5 * 3,
                        tempHeight = GlobalVariables.backgroundHeight / 2;
                addToOverlay(new AiSelectionFrame((GlobalVariables.screenSizeX / 2) - (tempWidth / 2), tempHeight / 2, tempWidth, tempHeight, R.drawable.frame2_unpressed));
            }
            else if (multiplayerButton.isPressed())
            {
               ScreenManager.fadeOutTo(ScreenState.localMultiplayerGame);
            }
            else if (onlineGameButton.isPressed())
            {
                //changeScreen(ScreenState.onlineMultiplayerSetup);
                changeScreen(ScreenState.onlineMenu);
            }
            else if (backButton.isPressed())
                changeScreen(ScreenState.mainMenu);
        }

    }
}