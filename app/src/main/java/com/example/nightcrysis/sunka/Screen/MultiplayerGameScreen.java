package com.example.nightcrysis.sunka.Screen;

import com.example.nightcrysis.sunka.GameEntity.Tray;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Starts a new local multiplayer game.
 *
 * Extends {@link GameScreen}.
 *
 * @author florianaucomte
 */
public class MultiplayerGameScreen extends GameScreen
{
    /**
     * Creates a new MultiplayerGameScreen object. Creates a new {@link Tray} object and adds it to
     * the game entities.
     */
    public MultiplayerGameScreen()
    {
        float singlePotSize = GlobalVariables.screenSizeX/11; //Used for reference, total width of screen will be divided by 11
        tray = new Tray(0, GlobalVariables.screenSizeY / 2 - singlePotSize, GlobalVariables.screenSizeX, singlePotSize * 2);
        gameEntities.add(tray);
    }
}
