package com.example.nightcrysis.sunka.GameEntity;

import android.util.Log;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * Created by tuffail on 21/10/15.
 * Extends {@link Tray} class (which contains all the components of the game board) so user can play against an easy AI
 */
public class TrayEasyAI extends Tray
{
    protected boolean AIThinking = false;
    protected int waitTime = 150;
    private String message = "AI is thinking";

    /**
     * Takes the top left coordinate of the Tray along with it's attributes
     * @param _x X coordinate of Tray
     * @param _y Y coordinate of the Tray
     * @param _width Width of the Tray
     * @param _height Height of the Tray
     */
    public TrayEasyAI(float _x, float _y, float _width, float _height)
    {
        super(_x, _y, _width, _height);
        isFirstTouch = false;
        notPlayerTurn = "Please select pots from the other side!";
        toggleLockedPots();
    }

    /**
     * Distribute pebbles in a pot using the index of the small pot
     * @param potIndex index of small pot
     */
    public void distributePebblesAI(int potIndex)
    {
        distributePebbles(smallPots[potIndex]);
    }

    /**
     * Updates the state of the game and checks if the game is over or it is the AI's turn
     * @param delta Variable time elapsed since the last update cycle
     */
    @Override
    public void update(float delta)
    {
        super.update(delta);

        if(isGameOver()){
            lockPots();
            currentPlayer = null;
        }

        if (isTurnAI()){
            if (waitTime > 0)
            {
                waitTime -= delta;
                if(waitTime%20.0f == 0)
                {
                    if(message.endsWith("..."))
                        message = "AI is thinking";
                    else
                        message += ".";
                }
                showMessage(message, 0.5f);
            }
        }

        if (isTurnAI() && !AIThinking){
            AIThinking = true;
            EasyAIMove easyAiMove = new EasyAIMove(this,smallPots);
            easyAiMove.start();
        }

    }

    private void lockPots()
    {
        for(int i = 0; i < smallPots.length; ++i)
        {
            if(i < smallPots.length/2)
            {
                smallPots[i].setLocked(false);
            }
            else
            {
                smallPots[i].setLocked(true);
            }
        }

        bigPots[0].setLocked(false);
        bigPots[1].setLocked(true);
    }

    /**
     * Toggles the small and big pots according to the player who's turn it is
     */
    @Override
    public void toggleLockedPots()
    {
        for(int i = 7; i < smallPots.length; ++i)
        {
            smallPots[i].setLocked(true);
        }
        bigPots[1].setLocked(true);

        if(isGameOver())
        {
            for(SmallPot sp : smallPots)
            {
                sp.setLocked(true);
            }
            bigPots[0].setLocked(true);
            bigPots[1].setLocked(true);

            showMessage(gameIsOver, gameIsOverDuration);
        } else {
            boolean toggleLocked;

            if(currentPlayer == Player.A)
            {
                toggleLocked = false;
            } else {
                toggleLocked = true;
            }

            for(int i = 0; i < smallPots.length/2; ++i)
            {
                smallPots[i].setLocked(toggleLocked);
            }
            bigPots[0].setLocked(toggleLocked);
        }
    }


}