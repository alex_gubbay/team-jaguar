package com.example.nightcrysis.sunka.Graphics;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * This class displays the ENTIRE size of the image specified
 */
public class Sprite {
    private static Rect srcRect = new Rect();
    private static RectF drawRect = new RectF();

    private float x, y, width, height;;
    private Bitmap sprite;

    /**
     * Constructor for the sprite class
     * @param _x                  x coordinate of the object
     * @param _y                  y coordinate of the object
     * @param _width              width of the object
     * @param _height             height of the object
     * @param spriteResourceId    Resource id of the image file
     */
    public Sprite(float _x, float _y, float _width, float _height, int spriteResourceId)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        sprite = ImageManager.getBitmap(spriteResourceId);
    }

    /**
     * Render method for the sprite image.
     * @param canvas    Canvas object used to draw images on screen
     */
    public void render(Canvas canvas)
    {
        srcRect.set(0, 0, sprite.getWidth(), sprite.getHeight());
        drawRect.set(x, y, x + width, y + height);
        canvas.drawBitmap(sprite, srcRect, drawRect, GlobalVariables.paint);
    }

    /**
     * Setter method for the x and y position of the sprite
     * @param _x    x coordinates
     * @param _y    y coordinates
     */
    public void setPosition(float _x, float _y)
    {
        x = _x;
        y = _y;
    }

    /**
     * Setter method for changing the x and y position of the sprite relative to its
     * current position
     * @param changeInX    relative change in x
     * @param changeInY    relative change in y
     */
    public void changePosition(int changeInX, int changeInY)
    {
        x += changeInX;
        y += changeInY;
    }

    /**
     * Changes the size of the object (in compliance to the GameEntity or in game object this
     * sprite is attached to)
     * @param _width     new width
     * @param _height    new height
     * @deprecated Replaced by setSize
     */
    public void changeSize(float _width, float _height)
    {
        if (_width <= 0 | _height <= 0)
        {
            Log.e("Sprite", "Invalid input in changeSize(float, float): input(s) cannot be less or equal to 0.");
            return;
        }
        width = _width;
        height = _height;
    }

    /**
     * Changes the size of the object (in compliance to the GameEntity or in game object this
     * sprite is attached to)
     * @param _width     new width
     * @param _height    new height
     */
    public void setSize(float _width, float _height)
    {
        width = _width;
        height = _height;
    }

    /**
     * Getter method for x position
     * @return x
     */
    public float getX() {
        return x;
    }

    /**
     * Getter method for y position
     * @return y
     */
    public float getY() {
        return y;
    }

    /**
     * Getter method for the width
     * @return width
     */
    public float getWidth() {
        return width;
    }

    /**
     * Getter method for the height
     * @return hegiht
     */
    public float getHeight() {
        return height;
    }

    /**
     * Getter method for the sprite image
     * @return sprite image as Bitmap
     * @see Bitmap
     */
    public Bitmap getSprite() {
        return sprite;
    }
}
