package com.example.nightcrysis.sunka;

import android.util.Log;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 */
public class Physics {
    public static boolean checkPointRectangleCollision(float pointX, float pointY, float rectX, float rectY, float rectWidth, float rectHeight)
    {
        /*pointX -= GlobalVariables.screenOffsetX;
        pointY -= GlobalVariables.screenOffsetY;*/

        return ((pointX > rectX) & (pointX < (rectX + rectWidth)) & (pointY > rectY) & (pointY < (rectY + rectHeight)));
    }
}
