package com.example.nightcrysis.sunka.Screen.TransitionEffect;

import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Screen transition effect class. Used to transition between screens using fading effect.
 * Default color is black, and can be changed if need be.
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class ColorFade implements GameEntityInterface{
    private boolean fadeOut, needsRemoval = false, setsTransitionFalse, removedAtEnd;
    private float changeOfOpacity, currentOpacity;
    private int a = 255, r = 0, g = 0, b = 0;

    /**
     * ColorFade effect primary constructor. Takes two parameters:
     * @param _fadeOut Fading out to a black screen. false if transitioning black overlay to screen,
     *                 true if transitioning from screen to black overlay
     * @param _duration The duration the fading takes place. Changes in overlay opacity is linear
     *                  using the equation 255/duration
     * */
    public ColorFade(boolean _fadeOut, float _duration, boolean setsTransitionFalseInScrnMngr, boolean _removedAtEnd)
    {
        initialize(_fadeOut, _duration);
        setsTransitionFalse = setsTransitionFalseInScrnMngr;
        removedAtEnd = _removedAtEnd;
    }

    /** Second constructor. Same as the primary constructor, however user can define colour values manually
     * @param _r color red, value range: 0 - 255
     * @param _g color green, value range: 0 - 255
     * @param _b color blue, value range: 0 - 255 */
    public ColorFade(boolean _fadeOut, float _duration,int _a, int _r, int _g, int _b, boolean _removedAtEnd)
    {
        a = _a;
        r = _r;
        g = _g;
        b = _b;

        initialize(_fadeOut, _duration);
        removedAtEnd = _removedAtEnd;
    }

    private void initialize(boolean _fadeOut, float _duration)
    {
        fadeOut = _fadeOut;
        changeOfOpacity = a/_duration;

        if (fadeOut)
        {
            currentOpacity = 0;
        }
        else
        {
            currentOpacity = a;
            changeOfOpacity = -changeOfOpacity;
        }
    }

    /**
     * Changes the opacity value at every frame rendered
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        if ((currentOpacity >= 0) && (currentOpacity <= a))
        {
            currentOpacity += changeOfOpacity * delta;
            if (currentOpacity < 0)
            {
                currentOpacity = 0;
                if (removedAtEnd)
                    needsRemoval = true;
                if (setsTransitionFalse)
                    ScreenManager.transitioning = false;
            }
            else if (currentOpacity > a)
            {
                currentOpacity = a;
                if (removedAtEnd)
                    needsRemoval = true;
                if (setsTransitionFalse)
                    ScreenManager.transitioning = false;
            }
        }

    }

    /**
     * Renders the selected colour (default black) over the entire screen
     * along with its opacity
     * @param canvas Canvas object used to draw images on screen
     */
    @Override
    public void render(Canvas canvas) {
        canvas.drawColor(Color.argb((int) currentOpacity, r, g, b));
    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return always false
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
