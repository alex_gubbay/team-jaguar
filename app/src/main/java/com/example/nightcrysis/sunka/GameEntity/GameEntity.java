package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.util.Log;

import com.example.nightcrysis.sunka.Graphics.Animation;
import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Most of everything should extend GameEntity, unless there is a reason not to.
 * This class contains methods for changing/modifying/getting the x, y coordinates, the size,
 * and the sprite/animation. Anything that's shown on screen should extend this class or
 * GameEntityInterface if it is more specialized
 * @see GameEntityInterface
 */
public abstract class GameEntity implements GameEntityInterface {

    protected Sprite sprite = null;
    protected Animation animation = null;

    protected boolean needsRemoval = false, isVisible = true; //Governs whether this object needs to be removed from the ArrayList in Screen class
    protected float x = 0, y = 0, width = 0, height = 0; //width, height defines the object size on screen (unrelated to actual image size)

    /** Update method: for purposes such as updating animation/sprite position.
     * @param delta The time elapsed since the last update cycle#
     * @see GameEntityInterface */
    @Override
    public void update(float delta)
    {
        if (sprite != null)
            sprite.setPosition(x, y);

        if (animation != null)
        {
            animation.setPosition(x, y);
            animation.update(delta);
        }
    }

    /** Method rendering the sprite/animation for the object.
     * Remember to call super();
     * @param canvas Canvas object that is passed for drawing calls
     * @see GameEntityInterface*/
    @Override
    public void render(Canvas canvas) //Drawing method: for drawing only
    {
        if (!isVisible) return;

        // Renders sprite if it is not null (if one is instantiated)
        if (sprite != null)
            sprite.render(canvas);

        // Renders animation if it is not null (if one is instantiated)
        if (animation != null) {
            animation.render(canvas);
        }
    }

    /**
     * Sets the position of GameEntity on screen.
     * @param _x    x coordinates as a floating-point value
     * @param _y    y coordinates as a floating-point value
     * */
    public void setPosition(float _x, float _y)
    {
        x = _x;
        y = _y;
    }

    /**
     * Changes the position of the GameEntity object relatively to its current position
     * @param changeInX
     * @param changeInY
     */
    public void changePosition(float changeInX, float changeInY)
    {
        x += changeInX;
        y += changeInY;
    }

    /**
     * Sets the isVisible boolean value. Affects visibility of the SoundButton object
     * @param value    boolean value to be set for isVisible
     */
    public void setVisible(boolean value)
    {
        isVisible = value;
    }

    /** Sets the size of the object
     * @param _width Sets the width of object
     * @param _height Sets the height of object*/
    public void setSize(float _width, float _height)
    {
        width = _width;
        height = _height;

        if (sprite != null)
            sprite.setSize(width, height);

        if (animation != null)
            animation.setSize(width, height);
    }

    /** Method for setting needsRemoval to true, where it will be removed from the ArrayLists in game */
    public void remove()
    {
        needsRemoval = true;
    }

    /**
     * Getter method for needsRemoval
     * @return boolean value if the object needs removal or not
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }

    /**
     * Getter method for the x coordinate of a GameEntity object
     * @return the x coordinate as a floating-point value
     */
    public float getX() {
        return x;
    }

    /**
     * Getter method for the y coordinate of a GameEntity object
     * @return the y coordinate as a floating-point value
     */
    public float getY() {
        return y;
    }

    /**
     * Getter method for the width of a GameEntity object
     * @return width of object, floating-point value
     */
    public float getWidth() {
        return width;
    }

    /**
     * Getter method for the height of a GameEntity object
     * @return height of object, floating-point value
     */
    public float getHeight() {
        return height;
    }

    /** Returns sprite object. BEWARE THAT VALUE MAY BE NULL!
     * @return Sprite object
     * @see Sprite*/
    public Sprite getSprite()
    {
        return sprite;
    }

    /** Returns animation object. BEWARE THAT VALUE MAY BE NULL!
     * @return Animation object
     * @see Animation*/
    public Animation getAnimation()
    {
        return animation;
    }
}
