package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/12.
 * Very simple class to tell the user what has happened
 * Centers itself to the center of the screen.
 * Used for error handling and notifications within game.
 */
public class MessageFrame extends UIFrameWithOverlay {
    private String message;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float textWidth, textHeight, sidePadding;
    private UIButtonFrame okButton;

    private static float textSize = -1;

    /**
     * Constructor for MessageFrame. It automatically centers itself to the center of the
     * screen so very little parameter is needed. Only the message set
     * @param _message    Message to be shown
     */
    public MessageFrame(String _message) {
        super(0, 0, 0, 0, R.drawable.frame2_unpressed);
        message = _message;
        initialize();
    }

    /** Initialization method for instantiating objects and settings different variables */
    private void initialize()
    {
        if (textSize < 0)
        {
            textSize = UIUtilities.findTextSizeByHeight("A", GlobalVariables.backgroundHeight / 32);
        }
        sidePadding = GlobalVariables.backgroundHeight / 16;
        setTextSize();
        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 6);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), -height);
        float tempWidth = GlobalVariables.backgroundWidth / 6;
        okButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), y + (height * 4 / 6), tempWidth, height / 6,
                R.drawable.frame1_unpressed, R.drawable.frame1_pressed, R.drawable.button1_overlay, "Ok");
        okButton.setTextSizeAuto(okButton.getWidth() / 6, okButton.getHeight() / 8);
        uiSubComponents.add(okButton);
        okButton.setTextColor(textColor);

        float displacement = (GlobalVariables.screenSizeY / 2) + (height * 0.5f);
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(okButton, 0, displacement, 0.5f, true, false));
    }

    /** Render method for displaying the message frame and its messages
     * @param canvas Canvas object for drawing
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);
        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(message, x + (width / 2) - (textWidth / 2), y + (height / 2) - (textHeight / 2), GlobalVariables.paint);
    }

    /** Setter method for text size. Called internally */
    private void setTextSize()
    {
        GlobalVariables.paint.setTextSize(textSize);
        Rect rect = new Rect();
        GlobalVariables.paint.getTextBounds(message, 0, message.length(), rect);
        textWidth = rect.width();
        textHeight = rect.height();
    }

    /**
     * Checks if the user input has touched the frame, and passes the input
     * event to other objects for further user input handling.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrameWithOverlay
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (super.onTouchEvent(event))
            if (okButton.isPressed())
            {
                frameClosed = true;

                float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f) * 2,
                        velocityY = height/2;
                uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
                uiSubComponents.add(new AccelerationEvent(okButton, 0, velocityY, 0, acceleration, 0.5f, true));
                okButton.setEnabled(false);
                darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            }
        return true;
    }
}
