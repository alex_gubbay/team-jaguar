package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.FileIO.DataManager;
import com.example.nightcrysis.sunka.FileIO.HighScoreManager;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UICheckBox;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UISlider;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * Specialized UIFrameWithOverlay for SettingsFrame.
 * Shows the available and current settings for the user to adjust to their liking.
 */
public class SettingsFrame extends UIFrameWithOverlay {
    private SpriteEntity title;
    private UIButtonFrame okButton, cancelButton, defaultButton, clearHighScores, clearPlayerHistory;
    private UICheckBox soundButton, musicButton, fancyAnimationButton;
    private UISlider soundVolume, musicVolume;
    private boolean frameClosing = false, tempSoundOn, tempMusicOn, tempFancyAnimation, soundVolumeTouched;
    private int tempSoundVolume, tempMusicVolume;

    private final float displacement = GlobalVariables.screenSizeY;

    private static boolean settingsSet = false;
    private static float settingsFontSize = 0;

    /**
     * Constructor for SettingsFrame
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite Resource ID
     */
    public SettingsFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        initialize();
    }

    /**
     * Constructor for SettingsFrame
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite Resource ID
     * @param frameSidePadding Sets the side padding of the frame.
     * @deprecated frameSidePadding is never used
     */
    public SettingsFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }

    /** Initialize temporary settings */
    private void initializeSettings()
    {
        tempSoundOn = GameSettings.soundOn;
        tempMusicOn = GameSettings.musicOn;
        tempSoundVolume = GameSettings.soundVolume;
        tempMusicVolume = GameSettings.musicVolume;
        tempFancyAnimation = GameSettings.fancyAnimationOn;
    }

    private void initialize()
    {
        initializeSettings();

        y -=  displacement; //Move frame off screen for transition

        //Initialize variables
        float tempRatio = width * 0.7f / ImageManager.getBitmapWidth(R.drawable.title_settings),
                tempWidth = ImageManager.getBitmapWidth(R.drawable.title_settings) * tempRatio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.title_settings) * tempRatio;

        title = new SpriteEntity(x + (width / 2) - (tempWidth / 2), y - (tempHeight / 2),
                tempWidth, tempHeight, R.drawable.title_settings);

        //Setting the width of buttons
        tempWidth = width/5;
        tempHeight = height / 16;

        int colorValue = Color.argb(255, 246, 176, 57);
        float sidePadding = width * 3 / 16, middlePadding = width / 16;

        okButton = new UIButtonFrame(x + sidePadding - (tempWidth / 2), y + height - (tempHeight * 2),
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "OK");
        okButton.setTextColor(colorValue);
        okButton.setTextSizeAuto(okButton.getWidth() / 8, okButton.getHeight() / 4);

        cancelButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), y + height - (tempHeight * 2),
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Cancel");
        cancelButton.setTextColor(colorValue);
        cancelButton.setTextSizeAuto(cancelButton.getWidth() / 8, cancelButton.getHeight() / 4);

        defaultButton = new UIButtonFrame(x + width - sidePadding - (tempWidth / 2), y + height - (tempHeight * 2),
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Default");
        defaultButton.setTextColor(colorValue);
        defaultButton.setTextSizeAuto(defaultButton.getWidth() / 8, defaultButton.getHeight() / 4);

        tempWidth = width / 20;
        sidePadding /= 2;

        //Instantiating audio widgets
        soundButton = new UICheckBox(x  + (width / 2) - middlePadding - tempWidth, y + sidePadding, tempWidth, tempWidth);
        soundButton.setValue(GameSettings.soundOn);
        soundButton.setScale(0.5f);

        soundVolume = new UISlider(x + sidePadding, soundButton.getY() + soundButton.getHeight() + sidePadding, (width / 2) - (2 * sidePadding), sidePadding / 16,
                R.drawable.slider_bar_green, R.drawable.slider_bar_empty, R.drawable.slider_button, R.drawable.slider_button_pressed);
        soundVolume.setCurrentValue(GameSettings.soundVolume);

        musicButton = new UICheckBox(x  + (width / 2) - middlePadding - tempWidth, soundVolume.getY() + soundVolume.getHeight() + sidePadding, tempWidth, tempWidth);
        musicButton.setValue(GameSettings.musicOn);
        musicButton.setScale(0.5f);

        musicVolume = new UISlider(x  + sidePadding, musicButton.getY() + musicButton.getHeight() + sidePadding, (width / 2) - (2 * sidePadding), sidePadding / 16,
                R.drawable.slider_bar_green, R.drawable.slider_bar_empty, R.drawable.slider_button, R.drawable.slider_button_pressed);
        musicVolume.setCurrentValue(GameSettings.musicVolume);

        float sliderButtonScale = (height / 15) / soundVolume.getButtonHeight();
        soundVolume.setButtonSize((int) (soundVolume.getButtonWidth() * sliderButtonScale), (int) (soundVolume.getButtonHeight() * sliderButtonScale));
        musicVolume.setButtonSize((int) (musicVolume.getButtonWidth() * sliderButtonScale), (int) (musicVolume.getButtonHeight() * sliderButtonScale));

        fancyAnimationButton = new UICheckBox(x + width - sidePadding - tempWidth, y + sidePadding + tempWidth, tempWidth, tempWidth);
        fancyAnimationButton.setValue(GameSettings.fancyAnimationOn);
        fancyAnimationButton.setScale(0.5f);

        tempHeight = height / 16;
        tempWidth = (width / 2) - sidePadding - (middlePadding / 2);

        clearHighScores = new UIButtonFrame(x + width - sidePadding - tempWidth, y + height - (height * 2 / 5), tempWidth, tempHeight,
                R.drawable.frame1_unpressed, R.drawable.frame1_pressed, R.drawable.frame1_overlay, "Clear high scores");
        clearHighScores.setTextColor(colorValue);
        clearHighScores.setTextSizeAuto(clearHighScores.getWidth() / 6, clearHighScores.getHeight() / 4);

        clearPlayerHistory = new UIButtonFrame(x + width - sidePadding - tempWidth, y + height - (height * 3 / 10), tempWidth, tempHeight,
                R.drawable.frame1_unpressed, R.drawable.frame1_pressed, R.drawable.frame1_overlay, "Clear player history");
        clearPlayerHistory.setTextColor(colorValue);
        clearPlayerHistory.setTextSizeAuto(clearPlayerHistory.getWidth() / 6, clearPlayerHistory.getHeight() / 4);

        uiSubComponents.add(title);
        uiSubComponents.add(okButton);
        uiSubComponents.add(cancelButton);
        uiSubComponents.add(defaultButton);
        uiSubComponents.add(soundButton);
        uiSubComponents.add(musicButton);
        uiSubComponents.add(soundVolume);
        uiSubComponents.add(musicVolume);
        uiSubComponents.add(fancyAnimationButton);
        uiSubComponents.add(clearHighScores);
        uiSubComponents.add(clearPlayerHistory);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(title, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(okButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(cancelButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(defaultButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(soundButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(musicButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(fancyAnimationButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(soundVolume, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(musicVolume, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(clearHighScores, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(clearPlayerHistory, 0, displacement, 0.5f, true, false));

        //Set settings once
        if (!settingsSet)
        {
            settingsSet = true;
            settingsFontSize = UIUtilities.findTextSizeByHeight("A", soundButton.getHeight()) / 2;
        }
    }

    /**
     * Renders the SettingsFrame.
     * @param canvas Canvas object used to draw images on screen
     * @see UIFrameWithOverlay
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setTextSize(settingsFontSize);
        GlobalVariables.paint.setColor(Color.argb(255, 246, 176, 57));

        float sidePadding = width / 16;

        //Draw sound labels
        canvas.drawText("Sound", x + sidePadding, soundButton.getY() + (soundButton.getHeight() / 2) - (settingsFontSize / 2), GlobalVariables.paint);
        canvas.drawText(String.valueOf(tempSoundOn), soundButton.getX() - (soundButton.getWidth() * 2.5f), soundButton.getY() + soundButton.getHeight() - (settingsFontSize / 2), GlobalVariables.paint);

        canvas.drawText("Sound volume:", x + sidePadding, soundVolume.getY() + (soundVolume.getHeight() / 2) - soundVolume.getButtonHeight() - (settingsFontSize / 2), GlobalVariables.paint);
        canvas.drawText(String.valueOf(tempSoundVolume), soundVolume.getX() + soundVolume.getWidth() + (soundVolume.getButtonWidth() / 3),
                soundVolume.getY() + soundVolume.getHeight() - (settingsFontSize / 2), GlobalVariables.paint);

        //Draw music labels
        canvas.drawText("Music", x + sidePadding, musicButton.getY() + (musicButton.getHeight() / 2) - (settingsFontSize / 2), GlobalVariables.paint);
        canvas.drawText(String.valueOf(tempMusicOn), musicButton.getX() - (musicButton.getWidth() * 2.5f), musicButton.getY() + soundButton.getHeight() - (settingsFontSize / 2), GlobalVariables.paint);

        canvas.drawText("Music volume:", x + sidePadding, musicVolume.getY() + ( musicVolume.getHeight() / 2) - (soundVolume.getButtonHeight() / 2) - (settingsFontSize / 2), GlobalVariables.paint);
        canvas.drawText(String.valueOf(tempMusicVolume), musicVolume.getX() + musicVolume.getWidth() + (musicVolume.getButtonWidth() / 3),
                musicVolume.getY() + musicVolume.getHeight() - (settingsFontSize / 2), GlobalVariables.paint);

        //Draw animation label
        canvas.drawText("Fancy animations", x + (width / 2) + sidePadding, fancyAnimationButton.getY() - (fancyAnimationButton.getHeight()), GlobalVariables.paint);
        canvas.drawText(String.valueOf(tempFancyAnimation), fancyAnimationButton.getX() - (fancyAnimationButton.getWidth() * 2.5f), fancyAnimationButton.getY() + fancyAnimationButton.getHeight() - (settingsFontSize / 2), GlobalVariables.paint);
    }

    /**
     * Checks if the user input has touched the frame, and passes the input
     * event to other objects for further user input handling.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrameWithOverlay
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        soundVolumeTouched = soundVolume.getIsTouched();
        tempMusicVolume = musicVolume.getCurrentValue();
        tempSoundVolume = soundVolume.getCurrentValue();
        SoundManager.setMusicVolume(tempMusicVolume);

        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP && soundVolumeTouched)
        {
            SoundManager.playSampleSound(tempSoundVolume);
            soundVolumeTouched = false;
            return true;
        }

        if (soundButton.isPressed())
        {
            tempSoundOn = soundButton.getValue();
            SoundManager.toggleSound(tempSoundOn);
        }
        else if (musicButton.isPressed())
        {
            tempMusicOn = musicButton.getValue();
            SoundManager.toggleMusic(tempMusicOn);
        }
        else if (fancyAnimationButton.isPressed())
        {
            tempFancyAnimation = fancyAnimationButton.getValue();
        }
        else if (okButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;

            GameSettings.soundOn = tempSoundOn;
            GameSettings.soundVolume = tempSoundVolume;
            GameSettings.musicOn = tempMusicOn;
            GameSettings.musicVolume = tempMusicVolume;
            GameSettings.fancyAnimationOn = tempFancyAnimation;
            SoundManager.toggleMusic(GameSettings.musicOn);
            SoundManager.toggleSound(GameSettings.soundOn);
            SoundManager.setMusicVolume(GameSettings.musicVolume);
            GameSettings.saveSettings();
        }
        else if (cancelButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;

            SoundManager.toggleMusic(GameSettings.musicOn);
            SoundManager.toggleSound(GameSettings.soundOn);
            SoundManager.setMusicVolume(GameSettings.musicVolume);
        }
        else if (defaultButton.isPressed())
        {
            tempSoundOn = true;
            tempSoundVolume = 80;
            tempMusicOn = true;
            tempMusicVolume = 60;
            tempFancyAnimation = true;;
            soundButton.setValue(tempSoundOn);
            musicButton.setValue(tempMusicOn);
            fancyAnimationButton.setValue(tempFancyAnimation);
            soundVolume.setCurrentValue(tempSoundVolume);
            musicVolume.setCurrentValue(tempMusicVolume);
            SoundManager.toggleMusic(tempMusicOn);
            SoundManager.toggleSound(tempSoundOn);
            SoundManager.setMusicVolume(tempMusicVolume);
        }
        else if (clearPlayerHistory.isPressed())
        {
            ScreenManager.getCurrentScreen().addToOverlay(new MessageFrame("All player history has been cleared."));
            DataManager dataManager = new DataManager();
            dataManager.clearPlayers();
        }
        else if (clearHighScores.isPressed())
        {
            ScreenManager.getCurrentScreen().addToOverlay(new MessageFrame("All high scores have been cleared."));
            HighScoreManager highScoreManager = new HighScoreManager();
            highScoreManager.clearScores();
        }

        if (frameClosed)
        {
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f),
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(title, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(okButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(cancelButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(defaultButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(soundButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(musicButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(fancyAnimationButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(soundVolume, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(musicVolume, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(clearHighScores, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(clearPlayerHistory, 0, velocityY, 0, acceleration, 0.5f, true));

            okButton.setEnabled(false);
            cancelButton.setEnabled(false);
            defaultButton.setEnabled(false);
            soundButton.setEnabled(false);
            musicButton.setEnabled(false);
            fancyAnimationButton.setEnabled(false);
            clearHighScores.setEnabled(false);
            clearPlayerHistory.setEnabled(false);

        }

        return true;
    }
}
