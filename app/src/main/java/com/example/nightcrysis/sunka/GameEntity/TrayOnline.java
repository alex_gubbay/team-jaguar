package com.example.nightcrysis.sunka.GameEntity;

import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataTransmissionManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.MessageFrame;

/**
 * Created by florianaucomte on 07/11/2015.
 * Governs the moves and logic for the online multiplayer game, by
 * sending the game state data back and forth from each device using the Java Sockets used in the
 * {@link DataTransmissionManager} class.
 *
 * Extends {@link Tray}.
 *
 * @author florianaucomte
 */
public class TrayOnline extends Tray
{
    String gameState = "";
    String waitForOpponent = "Opponent is thinking";
    String[] gameStateData;
    private boolean isHost, isGameOver = false;
    private Player deviceUser;
    private int touchedPotIndex = -1, gameResult = -2;
    private float opponentMessageTimer = 0, opponentMessageTimerDuration = 0.4f;

    /**
     * Creates a new TrayOnline object. This checks who is the host of the game to set the starting
     * configuration accordingly for each player.
     * @param _x X coordinate of the tray.
     * @param _y Y coordinate of the tray.
     * @param _width Width of the tray.
     * @param _height Height of the tray.
     * @param _isHost True if the user is the host, false otherwise.
     */
    public TrayOnline(float _x, float _y, float _width, float _height, boolean _isHost)
    {
        super(_x, _y, _width, _height);
        isHost = _isHost;
        if(isHost)
            deviceUser = Player.A;
        else
            deviceUser = Player.B;

        toggleLockedPots();

        DataTransmissionManager.sendCounter = 0;
        DataTransmissionManager.receiveCounter = 0;
    }

    /**
     * Overrides the {@link Tray}'s update method. This method contains the code for receiving the
     * game state data after the current player has made their move, which consequently updates
     * the game in the other player's device.
     *
     * @param delta The time elapsed since the last update cycle.
     */
    @Override
    public void update(float delta)
    {
        super.update(delta);

        if (gameState == null)
            gameState = "";

        //FOR RECEIVER
        if(DataTransmissionManager.sendCounter == DataTransmissionManager.receiveCounter+1)
        {
            String newGameState = "";
            if(!DataTransmissionManager.getReceivedMessages().isEmpty() && (!gameState.equals(DataTransmissionManager.getLastMessage()) || gameState.equals("")))
            {
                newGameState = DataTransmissionManager.getLastMessage();
                gameState = newGameState;
                if(!gameState.equals("Client is ready") && !gameState.equals("Host is ready"))
                    gameStateData = newGameState.split(",");
            }
            if(!gameState.equals("") && !gameState.equals("Client is ready") && !gameState.equals("Host is ready"))
            {
                if (gameStateData.length == 1){
                    Log.d("Test", gameStateData[0]);
                    DataTransmissionManager.stopConnection();
                    EventController temp = new EventController();
                    temp.addEvent(new MessageFrame("The other player has left the game!"));
                    temp.addEvent(new ChangeScreenTimer(0, ScreenState.mainMenu));
                    ScreenManager.addToMasterOverlay(temp);
                    DataTransmissionManager.destroyObjects();
                    needsRemoval = true;
                    return;
                }
                else if(deviceUser.name().equals(gameStateData[1]))
                {
                    if(currentPlayer == null)
                        currentPlayer = deviceUser;
                }
                else if(!deviceUser.name().equals(gameStateData[1]))
                {
                    if(currentPlayer == null)
                        currentPlayer = getOpponent(deviceUser);
                    isFirstTouch = false;
                }
                highlightOpponentConsecutivePots(Integer.parseInt(gameStateData[0]));
                SoundManager.playSound(R.raw.game_wood_hit_hard);
                distributePebbles(smallPots[Integer.parseInt(gameStateData[0])]);
                currentPlayer = Player.valueOf(gameStateData[1]);
                isGameOver = Boolean.parseBoolean(gameStateData[2]);
                if(isGameOver)
                {
                    DataTransmissionManager.stopConnection();
                    gameResult = Integer.parseInt(gameStateData[3]);
                    ScreenManager.getCurrentScreen().addToOverlay(new TemporaryText(0, 0, computeEndGameMessage(), 10));
                }
                toggleLockedPots();
                DataTransmissionManager.updateReceiveCounter();
            }
        }
        else
        {
            if (opponentMessageTimer > 0)
                opponentMessageTimer -= delta;
            else if (!isFirstTouch && deviceUser != currentPlayer)
            {
                opponentMessageTimer = opponentMessageTimerDuration;

                if(waitForOpponent.endsWith("..."))
                    waitForOpponent = "Opponent is thinking";
                else
                    waitForOpponent += ".";
                showMessage(waitForOpponent, 0.5f);
            }
        }
    }

    /**
     * Overrides the {@link Tray}'s onTouchEvent method. This method contains the code for sending
     * the game state data as the current player chooses their move.
     *
     * @param event The MotionEvent that the device has detected.
     * @return a boolean value for when the device has been touched.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //If pebbles are moving
        if (pebbleMovingLock)
            return true;

        if (currentPlayer == deviceUser || isFirstTouch)
        {
            for(int i = 0; i < smallPots.length; ++i)
            {
                if(smallPots[i].onTouchEvent(event))
                {
                    switch(event.getAction())
                    {
                        case MotionEvent.ACTION_DOWN:
                            if(smallPots[i].isLocked())
                            {
                                showMessage("Please select pots from the other side!", notPlayerTurnDuration);
                                smallPots[i].selected = false;
                            }
                            else smallPots[i].highlightConsecutivePots(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            if (smallPots[i].selected)
                            {
                                if(!smallPots[i].isLocked())
                                {
                                    // If it's the first turn of the game
                                    if(isFirstTouch)
                                    {
                                        if (i < 7)
                                            currentPlayer = Player.A;
                                        else
                                            currentPlayer = Player.B;
                                        isFirstTouch = false;
                                        DataTransmissionManager.sendCounter = 0;
                                        DataTransmissionManager.receiveCounter = 0;
                                    }
                                    smallPots[i].selected = false;
                                    touchedPotIndex = i;
                                    smallPots[i].highlightConsecutivePots(false);
                                    distributePebbles(smallPots[i]);
                                    toggleLockedPots();

                                    if(isGameOver)
                                        showMessage(computeEndGameMessage(), 10);
                                    //FOR SENDER
                                    gameState = updateGameState();
                                    gameStateData = getGameStateData();
                                    DataTransmissionManager.queueMessageForSending(gameState);
                                }
                            }
                            break;
                    }
                    return true;
                }
            }

            for(BigPot bp : bigPots)
            {
                if(bp.onTouchEvent(event))
                {
                    showMessage(notSmallPot, notSmallPotDuration);
                    return true;
                }
            }
        }
        else showMessage(notPlayerTurn, notPlayerTurnDuration);
        return false;
    }

    /**
     * Overrides the {@link Tray}'s toggleLockedPots method. This effectively locks the pots for
     * each player, depending on whether the first touch has been made, or if the device user is
     * the current player or not.
     */
    @Override
    public void toggleLockedPots()
    {
        if(isFirstTouch)
        {
            if(isHost)
            {
                for(int i = 0; i < smallPots.length/2; ++i)
                    smallPots[i].setLocked(false);
                for(int i = 7; i < smallPots.length; ++i)
                    smallPots[i].setLocked(true);
            }
            else
            {
                for(int i = 0; i < smallPots.length/2; ++i)
                    smallPots[i].setLocked(true);
                for(int i = 7; i < smallPots.length; ++i)
                    smallPots[i].setLocked(false);
            }
        }
        else if(isGameOver)
        {
            for(SmallPot sp : smallPots)
                sp.setLocked(true);

            bigPots[0].setLocked(true);
            bigPots[1].setLocked(true);

            showMessage(gameIsOver, gameIsOverDuration);
        }
        else
        {
            if(currentPlayer == Player.A)
            {
                if(isHost)
                {
                    for(int i = 0; i < smallPots.length/2; ++i)
                        smallPots[i].setLocked(false);
                }
                else
                {
                    for(int i = 7; i < smallPots.length; ++i)
                        smallPots[i].setLocked(true);
                }
            }
            else
            {
                if(isHost)
                {
                    for(int i = 0; i < smallPots.length/2; ++i)
                        smallPots[i].setLocked(true);
                }
                else
                {
                    for(int i = 7; i < smallPots.length; ++i)
                        smallPots[i].setLocked(false);
                }
            }
        }
    }

    private String computeEndGameMessage()
    {
        if(gameResult == 0)
            return "It's a draw.";
        else
        {
            if(isHost)
            {
                if(gameResult == 1)
                    return "You won!";
                else if(gameResult == -1)
                    return "You lost...";
            }
            else
            {
                if(gameResult == 1)
                    return "You lost.";
                else if(gameResult == -1)
                    return "You won!";
            }
        }
        return "";
    }

    private Player getOpponent(Player currentPlayer)
    {
        if(currentPlayer == Player.A)
            return Player.B;
        else
            return Player.A;
    }

    private String updateGameState()
    {
        return touchedPotIndex + "," + currentPlayer.name() + "," + isGameOver + "," + gameResult;
    }

    private String[] getGameStateData()
    {
        return gameState.split(",");
    }
}
