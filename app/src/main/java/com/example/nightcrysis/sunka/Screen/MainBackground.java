package com.example.nightcrysis.sunka.Screen;

import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;

import com.example.nightcrysis.sunka.GameEntity.Cloud;
import com.example.nightcrysis.sunka.GameSettings;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by NIghtCrysIs on 2015/10/28.
 * Main background of the game, used for all screens that are not in game, in which are
 * MainMenuScreen, SettingsScreen, etc.
 */
public class MainBackground {
    private static Sprite background, foreground;
    private static ArrayList<GameEntityInterface> clouds = new ArrayList<>(); //Dynamic arraylist allows the random generation of clouds

    /** Public method that is called to set the screen offset of the background, and
     * instantiating the static Sprite objects. It also sets the screenRatio, and
     * adds values of screenOffsets in the GlobalVariables class.*/
    public static void initialize()
    {
        float imageWidth = 1500,
                imageHeight = 1000, //Both values are the dimensions of background images, they will never change.
                ratioX = GlobalVariables.screenSizeX / imageWidth, ratioY = GlobalVariables.screenSizeY / imageHeight;

        //Setting the screen ratio in GlobalVariables
        if (ratioX < ratioY) GlobalVariables.backgroundRatio = ratioX;
        else GlobalVariables.backgroundRatio = ratioY;

        float ratio = GlobalVariables.backgroundRatio;

        //Adds the offset value due to image proportion difference between the background images and the android phone screen
        GlobalVariables.backgroundOffsetX = (GlobalVariables.screenSizeX / 2) - (imageWidth * ratio / 2);
        GlobalVariables.backgroundOffsetY = (GlobalVariables.screenSizeY / 2) - ((imageHeight * ratio / 2));

        GlobalVariables.backgroundWidth = imageWidth * ratio;
        GlobalVariables.backgroundHeight = imageHeight * ratio;

        //Instantiating Sprite objects
        background = new Sprite(GlobalVariables.backgroundOffsetX, GlobalVariables.backgroundOffsetY,
                GlobalVariables.backgroundWidth, GlobalVariables.backgroundHeight, R.drawable.main_background);
        foreground = new Sprite(GlobalVariables.backgroundOffsetX, GlobalVariables.backgroundOffsetY,
                GlobalVariables.backgroundWidth, GlobalVariables.backgroundHeight, R.drawable.main_foreground);
    }

    /**
     * Handles random cloud generation and their movements in game
     * @param delta    time passed since last update cycle
     */
    public static void update(float delta)
    {
        // 1% in generating a new cloud every 1/60 per second
        if (GlobalVariables.random.nextFloat() < 0.005f)
            clouds.add(new Cloud());

        //If there are less than 3 clouds on screen, add in an extra chance of creating one
        if (clouds.size() < 1)
            if (GlobalVariables.random.nextFloat() < 0.005f)
                clouds.add(new Cloud());

        Iterator<GameEntityInterface> i = clouds.iterator();
        while (i.hasNext())
        {
            GameEntityInterface temp = i.next();
            if (temp.getNeedsRemoval())
                i.remove();
            else temp.update(delta);
        }
    }

    /**
     * Renders the main background of the game
     * @param canvas    Canvas object used to draw images on screen
     */
    public static void render(Canvas canvas)
    {
        //Draws the background first
        background.render(canvas);

        //Draw the clouds
        ArrayList<GameEntityInterface> tempArray = new ArrayList<>(clouds);
        for (GameEntityInterface x : tempArray)
            if (x != null)
                x.render(canvas);

        //Then draw the foreground
        foreground.render(canvas);

        //Lastly draw two black boxes around the left and right edge (due to different screen sizes and to hide the clouds)
        GlobalVariables.paint.setColor(Color.BLACK);
        canvas.drawRect(0, 0, GlobalVariables.backgroundOffsetX, GlobalVariables.screenSizeY, GlobalVariables.paint);
        canvas.drawRect(GlobalVariables.screenSizeX - GlobalVariables.backgroundOffsetX, 0, GlobalVariables.screenSizeX, GlobalVariables.screenSizeY, GlobalVariables.paint);
    }
}
