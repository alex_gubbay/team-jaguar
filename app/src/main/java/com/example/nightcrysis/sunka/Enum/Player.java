package com.example.nightcrysis.sunka.Enum;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Enum values for a player in game.There are two players hence there are two values.
 * NOTE: The value of player can actually take on 3 values, with null included as an
 * official value in game (Such as AI turn checks)
 *
 * Player values are used in the actual game, and player checking for each highs core
 *
 * @see com.example.nightcrysis.sunka.GameEntity.TrayAI
 * @see com.example.nightcrysis.sunka.GameEntity.TrayEasyAI
 * @see com.example.nightcrysis.sunka.GameEntity.TrayHardAI
 * @see com.example.nightcrysis.sunka.GameEntity.Tray
 *
 * @see com.example.nightcrysis.sunka.Screen.GameScreen
 */
public enum Player {
    A, B
}
