package com.example.nightcrysis.sunka.GameEntity;

import com.example.nightcrysis.sunka.Enum.Player;

/**
 * Created by tuffail on 07/11/15.
 * Virtual Pot used to represent a clone of a specific Pot for trying out different game states
 */
public class VirtualStatePot {

    int pebbles;
    public VirtualStatePot nextPot;
    public VirtualStatePot oppositePot;
    Player player;

    /**
     * Initialises the Pot
     */
    public VirtualStatePot(){
        pebbles = 0;
        nextPot = null;
        oppositePot = null;
    }

    /**
     * Sets the player who this pot belongs to
     * @param _player Player who the this pot belongs to
     */
    public void setPlayer(Player _player){
        player = _player;
    }

    /**
     * Returns the play who the pot belongs to
     * @return Player who the pot belongs to
     */
    public Player getPlayer(){
        return player;
    }

    /**
     * Sets the number of pebbles in the pot
     * @param _pebbles number of pebbles to be in the pot
     */
    public void setPebbles(int _pebbles){
        pebbles = _pebbles;
    }

    /**
     * Returns the number of pebble in the pot
     * @return number of pebbles in the pot
     */
    public int getPebbles(){
        return pebbles;
    }

    /**
     * Sets the next pot to this pot in the anti-clockwise direction
     * @param _nextPot next pot to this pot in the anti-clockwise direction
     */
    public void setNextPot(VirtualStatePot _nextPot){
        nextPot = _nextPot;
    }

    /**
     * Returns the next pot to this pot in the anti-clockwise direction
     * @return next pot to this pot in the anti-clockwise direction
     */
    public VirtualStatePot getNextPot(){
        return nextPot;
    }

    /**
     * Sets the pot opposite to this pot (BigPots should not set anything opposite to themselves)
     * @param _oppositePot pot opposite to this pot
     */
    public void setOppositePot (VirtualStatePot _oppositePot){
        oppositePot = _oppositePot;
    }

    /**
     * Returns the pot opposite to this pot (BigPots should return null)
     * @return pot opposite to this pot
     */
    public VirtualStatePot getOppositePot(){
        return oppositePot;
    }

}
