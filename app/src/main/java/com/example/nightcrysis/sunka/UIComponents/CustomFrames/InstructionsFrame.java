package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * Specialized UIFrameWithOverlay class for showing the instructions of the game.
 * Shows the instructions on screen, and buttons for closing the frame.
 * (Planned to lead to a tutorial game)
 */
public class InstructionsFrame extends UIFrameWithOverlay{
    private SpriteEntity title;
    private UIButtonFrame okButton, tutorialButton;
    private boolean frameClosing = false;
    private final float displacement = GlobalVariables.screenSizeY;
    private int color = Color.argb(255, 246, 176, 57);

    private static final String instructions = "Sunka is game that is easy to learn and hard to master. The aim is to collect as many gems \n" +
            "in your home-pot as possible.\n\n" +
            "Players take turns. You can play over the network, against the AI, or by passing the device \n" +
            "back and forth.\n\n" +
            "You can choose any pot from your half of the board to distribute gems from.\n\n" +
            "Gems are distributed anti-clockwise from their starting pot, one gem per pot.\n" +
            "When a gem lands in your home-pot it is captured!\n\n" +
            "If the last gem from your turn lands in your home-pot you get an extra turn. If it lands on an \n" +
            "empty pot that is on your half of the board, all the gems from the opponents opposite pot are \n" +
            "captured and put in your home-pot!\n\n" +
            "The winner is the player who has the most gems in their home pot when both players' side are\n" +
            "completely empty.\n";
    private static String[] shownInstructions = instructions.split("\n");
    private static boolean instructionsSet = false;
    private static float borderOffset = 0, instructionLineSpacing = 0, instructionFontSize = 0;

    /**
     * Constructor for InstructionsFrame
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite Resource ID
     */
    public InstructionsFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        initialize();
    }

    /**
     * Constructor for InstructionsFrame
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    frame sprite Resource ID
     * @param frameSidePadding Sets the side padding of the frame.
     * @deprecated frameSidePadding is never used
     */
    public InstructionsFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }

    private void initialize()
    {
        y -=  displacement; //Move frame off screen for transition

        //Set instructions once
        if (!instructionsSet)
        {
            borderOffset = height * 0.1f;
            instructionFontSize = UIUtilities.findTextSizeByWidth("If the last gem from your turn lands in your home-pot you get an extra turn. If it lands on an ", width * 0.65f);
            Rect rect = new Rect();
            GlobalVariables.paint.setTextSize(instructionFontSize);
            GlobalVariables.paint.getTextBounds("A", 0, 1, rect);
            instructionLineSpacing = rect.height() * 1.5f;
            instructionsSet = true;
        }

        //Initialize variables
        float tempRatio = width * 0.7f / ImageManager.getBitmapWidth(R.drawable.title_instructions),
                tempWidth = ImageManager.getBitmapWidth(R.drawable.title_instructions) * tempRatio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.title_instructions) * tempRatio;

        title = new SpriteEntity(x + (width / 2) - (tempWidth / 2), y - (tempHeight / 2),
                tempWidth, tempHeight, R.drawable.title_instructions);

        tempWidth = width / 5;

        okButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), y + height - (tileHeight),
                tempWidth, tileHeight/2, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "OK");
        okButton.setTextColor(color);
        okButton.setTextSizeAuto(0, okButton.getHeight()/ 4);

        tutorialButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), y + height - (tileHeight),
                tempWidth, tileHeight/2, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Tutorial");
        tutorialButton.setTextColor(color);
        tutorialButton.setTextSizeAuto(0, tutorialButton.getHeight()/ 4);

        uiSubComponents.add(title);
        uiSubComponents.add(okButton);
        uiSubComponents.add(tutorialButton);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(title, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(okButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(tutorialButton, 0, displacement, 0.5f, true, false));
    }

    /**
     * Updates the InstructionsFrame
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        super.update(delta);
    }

    /**
     * Renders the InstructionsFrame and the instruction text
     * @param canvas Canvas object used to draw images on screen
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        //Draw the instruction text
        GlobalVariables.paint.setColor(Color.argb(255, 246, 176, 57));
        GlobalVariables.paint.setTextSize(instructionFontSize);
        for (int counter1 = 0; counter1 < shownInstructions.length; counter1++)
        {
            canvas.drawText(shownInstructions[counter1], x + (borderOffset * 1.25f), y + borderOffset + (instructionLineSpacing * counter1), GlobalVariables.paint);
        }
    }

    /**
     * User input method. Passes the user input to corresponding objects of the game.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrameWithOverlay
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If the frame is closing, ignore the touches and return true
        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (okButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;
        }
        else if (tutorialButton.isPressed())
        {
            ScreenManager.getCurrentScreen().addToOverlay(new MessageFrame("Tutorials coming soon!"));
        }
        if (frameClosed)
        {
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f),
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(title, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(okButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(tutorialButton, 0, velocityY, 0, acceleration, 0.5f, true));
        }

        return true;
    }
}
