package com.example.nightcrysis.sunka.GameEntity;

import com.example.nightcrysis.sunka.Enum.Player;

/**
 * Created by tuffail on 07/11/15.
 * Used to represent a clone of a specific tray in order to try out different game states
 */
public class VirtualStateTray {

    public VirtualStatePot[] smallPots;
    public VirtualStatePot[] bigPots;
    Player currentPlayer;

    /**
     *Takes a reference to the small pots, big pots, and the current player of the tray to be cloned and creates an exact replica
     * @param _smallPots small pots of the tray to be cloned
     * @param _bigPots big pots of the tray to be cloned
     * @param _currentPlayer current player (who's turn it is) of the tray to be cloned
     */
    public VirtualStateTray(SmallPot[] _smallPots, BigPot[] _bigPots, Player _currentPlayer){
        this.smallPots = new VirtualStatePot[_smallPots.length];
        this.bigPots = new VirtualStatePot[_bigPots.length];
        this.currentPlayer = _currentPlayer;

        for (int i = 0; i < this.smallPots.length; ++i){
            this.smallPots[i] = new VirtualStatePot();
            this.smallPots[i].setPebbles(_smallPots[i].getPebbleCount());
        }

        for(int i = 0; i < this.bigPots.length; ++i){
            this.bigPots[i] = new VirtualStatePot();
            this.bigPots[i].setPebbles(_bigPots[i].getPebbleCount());
        }

        AISetOppositeReferences(this.smallPots);
        AISetNextReferences(this.smallPots, this.bigPots);
        AIAssignPotsToPlayers(this.smallPots,this.bigPots);

    }

    /**
     * Takes reference to the Virtual Tray to be cloned and creates an exact replica
     * @param virtualStateTray Virtual Tray to be cloned
     */
    public VirtualStateTray(VirtualStateTray virtualStateTray){
        this.smallPots = new VirtualStatePot[virtualStateTray.smallPots.length];
        this.bigPots = new VirtualStatePot[virtualStateTray.bigPots.length];
        this.currentPlayer = virtualStateTray.currentPlayer;

        for (int i = 0; i < this.smallPots.length; ++i){
            this.smallPots[i] = new VirtualStatePot();
            this.smallPots[i].setPebbles(virtualStateTray.smallPots[i].getPebbles());
        }

        for (int i = 0; i < this.bigPots.length; ++i){
            this.bigPots[i] = new VirtualStatePot();
            this.bigPots[i].setPebbles(virtualStateTray.bigPots[i].getPebbles());
        }

        AISetOppositeReferences(this.smallPots);
        AISetNextReferences(this.smallPots, this.bigPots);
        AIAssignPotsToPlayers(this.smallPots,this.bigPots);
    }

    private void AISetOppositeReferences(VirtualStatePot[] smallPots)
    {
        //setting the 'opposite' reference for each pot
        for(int i = 0; i < smallPots.length; ++i)
        {
            smallPots[i].setOppositePot(smallPots[smallPots.length - 1 - i]);
        }
    }

    private void AISetNextReferences(VirtualStatePot[] smallPots, VirtualStatePot[] bigPots)
    {
        //setting the 'next' reference for each pot
        for(int i = 0; i < smallPots.length/2-1; ++i)
        {
            smallPots[i].setNextPot(smallPots[i + 1]);
        }

        smallPots[smallPots.length/2-1].setNextPot(bigPots[0]);
        bigPots[0].setNextPot(smallPots[smallPots.length / 2]);

        for(int i = smallPots.length/2; i < smallPots.length-1; ++i)
        {
            smallPots[i].setNextPot(smallPots[i + 1]);
        }

        smallPots[smallPots.length-1].setNextPot(bigPots[1]);
        bigPots[1].setNextPot(smallPots[0]);
    }

    private void AIAssignPotsToPlayers(VirtualStatePot[] smallPots, VirtualStatePot[] bigPots)
    {
        for(int i = 0; i < smallPots.length/2; ++i)
        {
            smallPots[i].setPlayer(Player.A);
        }

        for(int i = smallPots.length/2; i < smallPots.length; ++i)
        {
            smallPots[i].setPlayer(Player.B);
        }

        bigPots[0].setPlayer(Player.A);
        bigPots[1].setPlayer(Player.B);
    }

    /**
     * Distributes pebbles in the selected pot, according to the rules of Sunka
     * @param selectedPot pot to have pebbles distributed from
     */
    public void AIDistributePebbles(VirtualStatePot selectedPot)
    {
        //get current pot's pebbles
        final int pebbleCount = selectedPot.getPebbles();
        int pebblesRemaining = selectedPot.getPebbles();
        selectedPot.setPebbles(0);
        VirtualStatePot tempPot = selectedPot;

        for (int i = 0; i < pebbleCount && pebblesRemaining > 0; ++i)
        {
            tempPot = tempPot.getNextPot();
            //if the next pot is the opponent's big pot, skip it
            if ((tempPot.getOppositePot() == null) && !(tempPot.getPlayer() == selectedPot.getPlayer()))
            {
                tempPot = tempPot.getNextPot();
            }
            //add one pebble to next pot then move on to following pot
            if(pebblesRemaining > 1){
                --pebblesRemaining;
                tempPot.setPebbles(tempPot.getPebbles()+1);
            }else{
                if(tempPot.getPebbles() == 0 && tempPot.getPlayer() == selectedPot.getPlayer() && tempPot.getOppositePot() != null){
                    VirtualStatePot oppositePot = tempPot.getOppositePot();
                    int oppositePotPebbleCount = oppositePot.getPebbles();
                    oppositePot.setPebbles(0);
                    if(selectedPot.getPlayer() == Player.A){
                        bigPots[0].setPebbles(bigPots[0].getPebbles()+oppositePotPebbleCount+1);
                        --pebblesRemaining;
                    }else{
                        bigPots[1].setPebbles(bigPots[1].getPebbles()+oppositePotPebbleCount+1);
                        --pebblesRemaining;
                    }
                }else{
                    --pebblesRemaining;
                    tempPot.setPebbles(tempPot.getPebbles()+1);
                }
                if (!(tempPot.getOppositePot() == null && tempPot.getPlayer() == selectedPot.getPlayer())){
                    this.currentPlayer = this.currentPlayer == Player.A? Player.B : Player.A;
                }
            }
        }
    }

}