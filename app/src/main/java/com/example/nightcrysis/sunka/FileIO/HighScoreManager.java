package com.example.nightcrysis.sunka.FileIO;

import android.content.SharedPreferences;

import com.example.nightcrysis.sunka.GlobalVariables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Alex on 21/10/2015.
 */
public class HighScoreManager {

    SharedPreferences scores;
    Set<String> scoreSet;
    String PREFS_NAME = "HighscoreFile";
    //boolean firstStart;

    /**
     * HighScoreManager constructor.
     * */
    public HighScoreManager() {
        scores = GlobalVariables.currentActivity.getSharedPreferences(PREFS_NAME, 0);
        scoreSet = scores.getStringSet("scores", null);
    }

    /**
     *
     * @return an arrayList of the scores, sorted using the custom ordering from highest to lowest.
     */

    public ArrayList<HighScore> getScores() {

        ArrayList<HighScore> scores = new ArrayList<HighScore>();

        if (scoreSet != null) {

            for (String aScoreSet : scoreSet) {
                String[] temps =
                        aScoreSet.split(",");

                scores.add(new HighScore(temps[0], Integer.parseInt(temps[1])));
            }
        }
        Collections.sort(scores);
        return scores;


    }


    /**
     * Adds a new highscore to the persistent storage and the live list at runtime
     * @param name name of player to whom the highscore belongs.
     * @param score score of player
     */
    public void addScore(String name, int score) {
        HighScore newScore = new HighScore(name, score);
        SharedPreferences.Editor editor = scores.edit();

        if (scoreSet == null) {

            scoreSet = new HashSet<String>();
            scoreSet.add(newScore.toString());


        } else {
            //Since a set does not allow duplicate entries,
            // so add a unique value, in which is the size of the set at each add operation
            scoreSet = new HashSet<String>(scoreSet);
            scoreSet.add(newScore.toString() + "," + scoreSet.size());
        }
        editor.putStringSet("scores", scoreSet);
        editor.commit();
    }


    /**
     * Permanently erases the scores from the live storage and the persistent storage.
     */
    public void clearScores() {
        SharedPreferences.Editor editor = scores.edit();
        if (scoreSet != null)
            scoreSet.clear();
        editor.remove("scores");
        editor.commit();
    }
}
