package com.example.nightcrysis.sunka.GameEntity;

import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.GlobalVariables;

/**
 * Created by florianaucomte on 21/10/15.
 * Governs the moves and logic of the medium AI player.
 *
 * Extends {@link Tray}.
 *
 * @author florianaucomte
 */
public class TrayAI extends Tray
{
    private int chosenPotIndex = -1;
    private int waitTime = 30;
    private String message = "AI is thinking";

    /**
     * Creates a new TrayAI object. Sets the starting configuration of the single player game.
     * @param _x X coordinate of the tray.
     * @param _y Y coordinate of the tray.
     * @param _width Width of the tray.
     * @param _height Height of the tray.
     */
    public TrayAI(float _x, float _y, float _width, float _height)
    {
        super(_x, _y, _width, _height);

        toggleLockedPots();
        isFirstTouch = false;
        notPlayerTurn = "Please select pots from the other side!";
    }

    private int getSmallPotPebbles(int potIndex)
    {
        return smallPots[potIndex].getPebbleCount();
    }

    private int getOppositePotPebbles(int potIndex)
    {
        SmallPot oppositePot = smallPots[potIndex].getOppositePot();
        return oppositePot.getPebbleCount();
    }

    private void distributePebblesAI(int potIndex)
    {
        distributePebbles(smallPots[potIndex]);
    }

    /**
     * Overrides the {@link Tray}'s update method. This looks for the different moves the AI
     * player can make. The moves are prioritised in an algorithm, so that the AI will perform
     * the best move.
     *
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta)
    {
        super.update(delta);

        //if it is AI's turn
        if (isTurnAI())
        {
            //if chosenPotIndex has already been computed
            if (chosenPotIndex > 0)
            {
                if (waitTime > 0)
                {
                    waitTime -= delta;
                    if(waitTime%20.0f == 0)
                    {
                        if(message.endsWith("..."))
                            message = "AI is thinking";
                        else
                            message += ".";
                    }
                    showMessage(message, 0.5f);
                }
                else
                {
                    if(!(getSmallPotPebbles(chosenPotIndex) == 0))
                    {
                        highlightOpponentConsecutivePots(chosenPotIndex);
                        distributePebblesAI(chosenPotIndex);
                        toggleLockedPots();
                        waitTime = 150;
                    }
                    //Setting chosenPotIndex to -1 to indicate that it needs to be calculated
                    chosenPotIndex = -1;
                }
            }
            //If chosenPotIndex is not computed yet!
            else
            {
                //If none of the goals are satisfied
                //the order of the conditions is crucial here, ai checks what moves it can do, from best move to worst
                if (!(canGetExtraTurnAI() || canStealOpponentPebbles() || canFillOpponentEmptyPots() || canMoveToOpponentSide()))
                {
                    //Then a pot is chosen at random
                    chosenPotIndex = GlobalVariables.random.nextInt(7) + 7;
                }
            }
        }
    }

    /**
     * Overrides the {@link Tray}'s toggleLockedPots method, so that the AI's pots are always
     * locked.
     */
    @Override
    public void toggleLockedPots()
    {
        for(int i = 7; i < smallPots.length; ++i)
            smallPots[i].setLocked(true);
        bigPots[1].setLocked(true);

        if(isGameOver())
        {
            for(SmallPot sp : smallPots)
                sp.setLocked(true);

            bigPots[0].setLocked(true);
            bigPots[1].setLocked(true);

            showMessage(gameIsOver, gameIsOverDuration);
        }
        else
        {
            boolean toggleLocked;

            if(currentPlayer == Player.A)
                toggleLocked = false;
            else
                toggleLocked = true;

            for(int i = 0; i < smallPots.length/2; ++i)
                smallPots[i].setLocked(toggleLocked);

            bigPots[0].setLocked(toggleLocked);
        }
    }

    private boolean canGetExtraTurnAI()
    {
        //goes through all of ai player's pots
        for(int i = 13; i > 6; --i)
        {
            //checks if it has the exact amount of pebbles to land on the big pot
            if(getSmallPotPebbles(i) == 14 - i)
            {
                chosenPotIndex = i;
                return true;
            }
        }
        return false;
    }

    private boolean canStealOpponentPebbles()
    {
        int mostPebblesOpposite = 0;

        //Goes through all AI player's pot for empty pots
        for (int i = 13; i > 6; --i)
        {
            //Empty pot is found on AI player's side!
            if (getSmallPotPebbles(i) == 0)
            {
                //Go through AI player's pots again
                for (int j = i - 1; j > 6; --j)
                {
                    //Check if the number of pebbles in smallPot[j] can reach the empty pot
                    if (i == (getSmallPotPebbles(j) + j) % 15)
                    {
                        //check if it has more pebbles opposite it than previous empty pots
                        if (getOppositePotPebbles(i) > mostPebblesOpposite)
                        {
                            mostPebblesOpposite = getOppositePotPebbles(i);
                            chosenPotIndex = j;
                        }
                    }
                }
            }
        }

        if(mostPebblesOpposite > 0)
            return true;
        return false;
    }

    private boolean canMoveToCertainPot(int toPotIndex, int fromPotIndex)
    {
        //if index of the 'to' pot is equal to the index of the 'from' pot + its number of pebbles
        //%15 is used to go around the board, 15 for all 14 small pots + ai's big pot.
        if(toPotIndex >= (fromPotIndex + getSmallPotPebbles(fromPotIndex))%15)
            return true;
        else
            return false;
    }

    private boolean canFillOpponentEmptyPots()
    {
        //for all of the opponent's pots
        for(int i = 6; i > 0; --i)
        {
            //if the pot is empty and the opposite pot contains some pebbles
            if(getSmallPotPebbles(i) == 0 && getOppositePotPebbles(i) > 0)
            {
                //go through all of ai's pots
                for(int j = 13; j > 6; --j)
                {
                    if(canMoveToCertainPot(i, j))
                    {
                        chosenPotIndex = j;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean canMoveToOpponentSide()
    {
        //go through all of ai's pots
        for(int i = 13; i > 6; --i)
        {
            //if the amount of pebbles in the small pot goes beyond the big pot
            if(getSmallPotPebbles(i) > 14 - i)
            {
                chosenPotIndex = i;
                return true;
            }
        }
        return false;
    }
}