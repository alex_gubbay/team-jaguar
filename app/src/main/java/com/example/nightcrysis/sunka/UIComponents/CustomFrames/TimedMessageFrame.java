package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Specialized GameMessageFrame that has a duration value and will delete itself
 * upon duration reaching <= 0.
 * Used for in game message handling to notify the players of what has happened
 * @see com.example.nightcrysis.sunka.GameEntity.Tray
 * @see com.example.nightcrysis.sunka.GameEntity.TrayEasyAI
 * @see com.example.nightcrysis.sunka.GameEntity.TrayAI
 * @see com.example.nightcrysis.sunka.GameEntity.TrayHardAI
 */
public class TimedMessageFrame extends GameMessageFrame {
    private float duration = 0;

    /**
     * Constructor for TimedMessageFrame
     * @param _message
     */
    public TimedMessageFrame(String _message, float _duration) {
        super(_message);
        duration = _duration;
    }

    /**
     * Setter method for setting the duration (or refreshing to a certain extent)
     * @param duration
     */
    public void setDuration(float duration) {
        this.duration = duration;
    }

    /**
     * Updates the TimedMessageFrame.
     * Duration left is reduced by variable time delta. Upon reaching <= 0 for duration,
     * the frame will be removed by setting needsRemoval to true.
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        super.update(delta);
        duration -= delta;
        if (duration <= 0)
            needsRemoval = true;
    }
}
