package com.example.nightcrysis.sunka.Screen;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * Very similar structure to GameEntity, and that it is intended this way.
 *
 * All instantiation of objects such as pots, pebbles, references etc, should be done so in the
 * derived classes' constructor.
 *
 * NOTE: MAY BE SUBJECTED TO FURTHER CHANGES IF THE TEAM DECIDES THAT IT IS APPROPRIATE
 */
public abstract class Screen {
    // The order of rendering goes exactly as it presented below, namely:
    // underlay (Background) -> gameEntities (Pots, Pebbles, etc.) -> userInterface -> overlay
    // 4 array lists may seem too much, but as long as it works and can help every separate between different layers of drawing
    ArrayList<GameEntityInterface> underlay = new ArrayList<>(), // Background and Back-backgrounds
                          gameEntities = new ArrayList<>(), // Pots, Pebbles, etc.
                          userInterface = new ArrayList<>(), // Heads up display, Buttons, Menu Frames, temporary string texts, etc.
                          overlay = new ArrayList<>(); // Temporary String texts, overlay images for hints and highlights

    ArrayList<GameEntityInterface> addToUnderlay = new ArrayList<>(),
                                addToGameEntities = new ArrayList<>(),
                                addToUserInterface = new ArrayList<>(),
                                addToOverlay = new ArrayList<>();

    /** The following methods corresponds to add(object) and add(index, object) */
    public void addToUnderlay(GameEntityInterface x)
    {
        addToUnderlay.add(x);
    }

    public void addToGameEntities(GameEntityInterface x)
    {
        addToGameEntities.add(x);
    }

    public void addToUserInterface(GameEntityInterface x)
    {
        addToUserInterface.add(x);
    }

    public void addToOverlay(GameEntityInterface x)
    {
        addToOverlay.add(x);
    }

    /**
     * Update method, must be called. Updates entities in the order of each array list
     * @param delta    time passed since last update cycle
     */
    public void update(float delta)
    {
        Iterator<GameEntityInterface> i;

        //First remove all entities that needs removal using iterators, which throws no exceptions
        i = underlay.iterator();
        removeFromArrayList(i);
        i = gameEntities.iterator();
        removeFromArrayList(i);
        i = userInterface.iterator();
        removeFromArrayList(i);
        i = overlay.iterator();
        removeFromArrayList(i);

        //Then copy all reference in existing array list to new array list to prevent
        //ConcurrentModificationException occurring during updating
        ArrayList temp = new ArrayList(underlay);
        i = temp.iterator();
        updateArrayList(delta, i);

        temp = new ArrayList(gameEntities);
        i = temp.iterator();
        updateArrayList(delta, i);

        temp = new ArrayList(userInterface);
        i = temp.iterator();
        updateArrayList(delta, i);

        temp = new ArrayList(overlay);
        i = temp.iterator();
        updateArrayList(delta, i);

        underlay.addAll(addToUnderlay);
        addToUnderlay.clear();
        gameEntities.addAll(addToGameEntities);
        addToGameEntities.clear();
        userInterface.addAll(addToUserInterface);
        addToUserInterface.clear();
        overlay.addAll(addToOverlay);
        addToOverlay.clear();
    }

    private void removeFromArrayList(Iterator<GameEntityInterface> i)
    {
        GameEntityInterface x;
        while (i.hasNext())
        {
            x = i.next();
            if (x.getNeedsRemoval())
                i.remove();
        }
    }

    /** The actual method that calls object.update(float).
     * It checks if the object needs removal first, if true then the object is removed,
     * if false, the object is updated */
    private void updateArrayList(float delta, Iterator<GameEntityInterface> i)
    {
        GameEntityInterface x;
        while (i.hasNext())
        {
            x = i.next();
            x.update(delta);
        }
    }

    /**
     * Rendering method, must be called. Draws in order of underlay, gameEntities, userInterfaces,
     * overlay. Reverse order needed as last drawing call overwrites the drawing pixels of
     * previous calls
     * @param canvas    Canvas object used to draw images on screen
     */
    public void render(Canvas canvas)
    {
        Iterator<GameEntityInterface> i;

        ArrayList temp = new ArrayList(underlay);
        i = temp.iterator();
        renderArrayList(canvas, i);

        temp = new ArrayList(gameEntities);
        i = temp.iterator();
        renderArrayList(canvas, i);

        temp = new ArrayList(userInterface);
        i = temp.iterator();
        renderArrayList(canvas, i);

        temp = new ArrayList(overlay);
        i = temp.iterator();
        renderArrayList(canvas, i);
    }

    /** The actual method that calls object.render(canvas).
     * Same implementation as updateArrayList */
    private void renderArrayList(Canvas canvas, Iterator<GameEntityInterface> i)
    {
        GameEntityInterface x;
        while (i.hasNext())
        {
            x = i.next();
            if (x != null)
            {
                if (x.getNeedsRemoval())
                    i.remove();
                else x.render(canvas);
            }
        }
    }

    /**
     * Sends the MotionEvent to all arrays in REVERSE order (because the overlay array list,
     * or last entity added, is shown on top of the screen).
     * @param event    The MotionEvent that the device has detected
     * @see Screen update method for array ordering.
     */
    public void onTouchEvent(MotionEvent event)
    {
        boolean touched = false;

        for (int i = overlay.size() - 1; i >= 0; i--)
            if (overlay.get(i).onTouchEvent(event))
            {
                touched = true;
                break;
            }

        if (!touched)
            for (int i = userInterface.size() - 1; i >= 0; i--)
                if (userInterface.get(i).onTouchEvent(event))
                {
                    touched = true;
                    break;
                }

        if (!touched)
            for (int i = gameEntities.size() - 1; i >= 0; i--)
                if (gameEntities.get(i).onTouchEvent(event))
                {
                    touched = true;
                    break;
                }

        if (!touched)
            for (int i = underlay.size() - 1; i >= 0; i--)
                if (underlay.get(i).onTouchEvent(event))
                {
                    //touched = true;
                    break;
                }
    }

}
