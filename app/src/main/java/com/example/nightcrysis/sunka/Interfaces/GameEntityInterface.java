package com.example.nightcrysis.sunka.Interfaces;

import android.graphics.Canvas;
import android.view.MotionEvent;

/**
 * Created by NIghtCrysIs on 2015/10/21.
 * Interface for GameEntity, used for UIComponents as they are customized GameEntities, and
 * does not follow the common convention of the usage of GameEntity class.
 * Other classes can implement this interface, however you must write the implementation details yourself.
 * IMPLEMENT THIS CLASS AT YOUR OWN RISK! Contact Johnson about any questions of using this class.
 */
public interface GameEntityInterface {

    /** Update method
     * @param delta The time elapsed since the last update cycle*/
    public void update(float delta);

    /** Rendering method
    * @param canvas Canvas object used to draw images on screen */
    public void render(Canvas canvas);

    /** User input method for touches on the screen.
    * Return true if the user's touch position collides with this object, doing so will break the parent (Screen) loop
    * Return false otherwise
    * @param event The MotionEvent that the device has detected */
    public boolean onTouchEvent(MotionEvent event);

    /** Getter method - Returns a boolean if needsRemoval is true
     * NOTE: If you implement this class you must create the needsRemoval variable yourself!! */
    public boolean getNeedsRemoval();
}
