package com.example.nightcrysis.sunka.Screen.TransitionEffect;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Simple class made for screen transition after a given duration
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class ChangeScreenTimer implements GameEntityInterface {
    private boolean needsRemoval = false, hasBackgroundColor = false;
    private float duration;
    private ScreenState changeTo = null;
    private int backgroundColor = -1;

    /**
     * Constructor for ChangeScreenTimer
     * @param _duration    Duration to wait before transition
     * @param _changeTo    ScreenState to transition to
     * @see ScreenState
     */
    public ChangeScreenTimer(float _duration, ScreenState _changeTo)
    {
        duration = _duration;

        if (_changeTo == null)
            needsRemoval = true;
        else changeTo = _changeTo;
    }

    /**
     * Specialized constructor provided for fading in/out screen transition
     * @param _duration    Duration to wait before transition
     * @param _changeTo    ScreenState to transition to
     * @param _backgroundColor    The background color draw whilst this object is being rendered
     * @see ScreenState
     */
    public ChangeScreenTimer(float _duration, ScreenState _changeTo, int _backgroundColor)
    {
        duration = _duration;
        backgroundColor = _backgroundColor;
        hasBackgroundColor = true;

        if (_changeTo == null)
            needsRemoval = true;
        else changeTo = _changeTo;
    }

    /**
     * update method. Updates the duration, and removes if it is <= 0
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta) {
        duration -= delta;

        if (duration <= 0)
        {
            ScreenManager.changeScreen(changeTo);

            needsRemoval = true;
        }
    }

    /**
     * Rendering method. If background color is set (via the specialized constructor)
     * a background color is drawn to the entire screen.
     * @param canvas Canvas object used to draw images on screen
     */
    @Override
    public void render(Canvas canvas) {
        if (hasBackgroundColor)
            canvas.drawColor(backgroundColor);
    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return always false
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
