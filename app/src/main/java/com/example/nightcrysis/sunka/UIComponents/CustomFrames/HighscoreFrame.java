package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.FileIO.DataManager;
import com.example.nightcrysis.sunka.FileIO.GamePlayer;
import com.example.nightcrysis.sunka.FileIO.HighScore;
import com.example.nightcrysis.sunka.FileIO.HighScoreManager;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.HighScoreEntry;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by NIghtCrysIs on 2015/11/11.
 * Specialized HighscoreFrame for displaying the high score entries.
 *
 * Extends UIFrameWithOverlay
 * @see UIFrameWithOverlay
 */
public class HighscoreFrame extends UIFrameWithOverlay{
    private SpriteEntity title;
    private UIButtonFrame doneButton, nextButton, prevButton;
    private boolean frameClosing = false;
    private ArrayList<HighScoreEntry> highScoreEntries;
    private int highScoreCounter = 0;
    private float entryHeight, spacing;
    private static int entriesPerScreen = -1;

    private HighScoreManager highScoreManager = new HighScoreManager();
    private DataManager dataManager = new DataManager();

    /**
     * Constructor for HighScoreFrame. Sets the position and size, and instantiates
     * the HighScoreEntries and other UIComponents to be shown on screen
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     * @see UIFrameWithOverlay
     */
    public HighscoreFrame(float _x, float _y, float _width, float _height, int frameSpriteID) {
        super(_x, _y, _width, _height, frameSpriteID);
        initialize();
    }

    /**
     * Constructor for HighScoreFrame. Sets the position and size, and instantiates
     * the HighScoreEntries and other UIComponents to be shown on screen
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     * @param frameSidePadding Side padding of the frame
     * @deprecated frameSidePadding is unused
     * @see UIFrameWithOverlay
     */
    public HighscoreFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding) {
        super(_x, _y, _width, _height, frameSpriteID, frameSidePadding);
        initialize();
    }


    private void initialize()
    {
        entryHeight = height / 18;
        spacing = height / 10;

        //Initialize high score entries
        highScoreEntries = new ArrayList<>();

        float yOffset = y;
        y -=  y + height; //Move frame off screen for transition

        //Initialize variables
        float tempRatio = width * 0.75f / ImageManager.getBitmapWidth(R.drawable.title_highscores),
                tempWidth = ImageManager.getBitmapWidth(R.drawable.title_highscores) * tempRatio,
                tempHeight = ImageManager.getBitmapHeight(R.drawable.title_highscores) * tempRatio;


        title = new SpriteEntity(x + (width / 2) - (tempWidth / 2), y - (tempHeight * 3 / 4),
                tempWidth, tempHeight, R.drawable.title_highscores);

        tempHeight = height / 15;
        float tempY = y + height - tempHeight * 2;
        tempWidth = width * 2 / 10;

        int colorValue = Color.argb(255, 246, 176, 57);

        doneButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), tempY,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Done");
        doneButton.setTextColor(colorValue);
        doneButton.setTextSizeAuto(0, doneButton.getHeight() / 4);

        prevButton = new UIButtonFrame(x + (width / 2) - (tempWidth / 2), tempY,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Prev");
        prevButton.setTextColor(colorValue);
        prevButton.setTextSizeAuto(0, prevButton.getHeight() / 4);

        nextButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), tempY,
                tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed,
                R.drawable.frame1_overlay, "Next");
        nextButton.setTextColor(colorValue);
        nextButton.setTextSizeAuto(0, nextButton.getHeight() / 4);

        uiSubComponents.add(title);
        uiSubComponents.add(doneButton);
        uiSubComponents.add(nextButton);
        uiSubComponents.add(prevButton);

        //Create high score objects
        addHighScoreEntries(yOffset);

        //and create event that move them down
        uiSubComponents.add(new PositionEvent(this, 0, height + yOffset, 0.5f, true, false)); //0.5f is the overlay duration
        uiSubComponents.add(new PositionEvent(title, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(doneButton, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(nextButton, 0, height + yOffset, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(prevButton, 0, height + yOffset, 0.5f, true, false));
    }

    private void addHighScoreEntries(float yOffset)
    {
        //Calculate the number of entries that can be displayed on screen
        if (entriesPerScreen < 0)
        {
            float sizeDifference = doneButton.getY() - y;
            entriesPerScreen = (int) (sizeDifference / (entryHeight * 1.25f)) - 1;
        }

        float startingYPos = -height;

        ArrayList<HighScore> tempHighScoreEntries = highScoreManager.getScores();

        Log.e("A",highScoreManager.getScores().size() + " " + dataManager.getAllPlayers().size());

        for (int i = 0; i < entriesPerScreen; i++)
        {
            if (i < tempHighScoreEntries.size())
            {
                HighScore tempHighScore = tempHighScoreEntries.get(i);
                HighScoreEntry entry = new HighScoreEntry(x + tileWidth / 2, startingYPos + entryHeight + (i * entryHeight * 1.25f),
                        width - tileWidth, tileHeight/2, i + 1, tempHighScore.getScoreValue(), dataManager.getPlayer(tempHighScore.getName()));
                highScoreEntries.add(entry);
                uiSubComponents.add(new PositionEvent(entry, 0, height + yOffset, 0.5f, true, false));
            }
            else
            {
                HighScoreEntry entry = new HighScoreEntry(x + tileWidth / 2, startingYPos + entryHeight + (i * entryHeight * 1.25f),
                        width - tileWidth, tileHeight/2, i + 1, 0, new GamePlayer("", 0, 0, 0, 0, 0, 0));
                entry.setVisible(false);
            }
        }
    }

    /** Shows the next 8 entries in high scores */
    private void nextPressed()
    {
        ArrayList<HighScore> tempHighScoreEntries = highScoreManager.getScores();
        int highScoreArraySize = tempHighScoreEntries.size();
        if (highScoreCounter + entriesPerScreen < highScoreArraySize)
        {
            highScoreCounter += entriesPerScreen;
            for (int i = 0; i < entriesPerScreen; i++)
            {
                if (highScoreCounter + i >= highScoreArraySize)
                {
                    highScoreEntries.get(i).setVisible(false);
                }
                else
                {
                    HighScore tempHighScore = tempHighScoreEntries.get(i + highScoreCounter);
                    HighScoreEntry tempEntry = highScoreEntries.get(i);
                    tempEntry = new HighScoreEntry(tempEntry.getX(), tempEntry.getY(), tempEntry.getWidth(), tempEntry.getHeight(),
                            i + highScoreCounter + 1, tempHighScore.getScoreValue(), dataManager.getPlayer(tempHighScore.getName()));
                    highScoreEntries.set(i, tempEntry);
                }
            }
        }
    }

    /** Shows the previous 8 entries in high scores */
    private void previousPressed()
    {
        if (highScoreCounter > 0)
        {
            highScoreCounter -= entriesPerScreen;
            ArrayList<HighScore> tempHighScoreEntries = highScoreManager.getScores();

            for (int i = 0; i < entriesPerScreen; i++)
            {
                HighScore tempHighScore = tempHighScoreEntries.get(i + highScoreCounter);
                HighScoreEntry tempEntry = highScoreEntries.get(i);
                tempEntry = new HighScoreEntry(tempEntry.getX(), tempEntry.getY(), tempEntry.getWidth(), tempEntry.getHeight(),
                        i + highScoreCounter + 1, tempHighScore.getScoreValue(), dataManager.getPlayer(tempHighScore.getName()));
                highScoreEntries.set(i, tempEntry);
            }
        }
    }

    /** Moves all components related to the frame upwards outside the screen */
    private void changeScreen()
    {
        //Accelerate everything upwards :D
        float initialVelocityY = height / 2,
                accelerationValue = -AccelerationEvent.accelYOffScreenValueUp(y, height, initialVelocityY, 0.5f);

        uiSubComponents.add(new AccelerationEvent(this, 0, initialVelocityY, 0, accelerationValue, 0.5f, true)); //0.5f is the overlay duration
        uiSubComponents.add(new AccelerationEvent(title, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(doneButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(nextButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
        uiSubComponents.add(new AccelerationEvent(prevButton, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));

        for (HighScoreEntry tempEntry : highScoreEntries)
            uiSubComponents.add(new AccelerationEvent(tempEntry, 0, initialVelocityY, 0, accelerationValue, 0.5f, true));
    }

    /**
     * Updates the HighscoreFrame and its corresponding high score entries shown on screen
     * @param delta The time elapsed since the last update cycle
     * @see UIFrameWithOverlay
     */
    @Override
    public void update(float delta) {
        super.update(delta);

        /* Remove objects using the iterator that is exception free*/
        Iterator<HighScoreEntry> i = highScoreEntries.iterator();
        while (i.hasNext())
        {
            HighScoreEntry tempEntry = i.next();
            if (tempEntry.getNeedsRemoval())
                i.remove();
        }

        /*Copies reference, because array can be cleared by an interrupt, that in turn may cause
        ConcurrentModiicationException*/
        ArrayList<HighScoreEntry> tempArray = new ArrayList<>(highScoreEntries);
        for (HighScoreEntry tempObj : tempArray)
            tempObj.update(delta);
    }

    /**
     * Renders the HighscoreFrame and its corresponding high score entries shown on screen
     * @param canvas Canvas object used to draw images on screen
     * @see UIFrameWithOverlay
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        /*Copies reference, because array can be cleared by an interrupt, that in turn may cause
        ConcurrentModiicationException*/
        ArrayList<HighScoreEntry> tempArray = new ArrayList<>(highScoreEntries);
        for (HighScoreEntry tempObj : tempArray)
            tempObj.render(canvas);

        GlobalVariables.paint.setTextSize(HighScoreEntry.getFontSize());
        GlobalVariables.paint.setColor(Color.argb(255, 246, 176, 57));

        //Ranking
        canvas.drawText("Rank", x + width / 32, y + spacing / 2, GlobalVariables.paint);

        //Score
        canvas.drawText("Score", x + width * 2f / 16, y + spacing / 2, GlobalVariables.paint);

        //Name
        canvas.drawText("Name", x + width * 3.5f / 16, y + spacing / 2, GlobalVariables.paint);

        //Average score
        canvas.drawText("Avg. score", x + width * 6f / 16, y + spacing / 2, GlobalVariables.paint);

        //Win/Draw/Loss
        canvas.drawText("Win/Draw/Loss", x + width * 9f / 16, y + spacing / 2, GlobalVariables.paint);

        //Total time played
        canvas.drawText("Play Time", x + width * 13f / 16, y + spacing / 2, GlobalVariables.paint);
    }

    /**
     * Checks if the user input has touched the frame, and passes the input
     * event to other objects for further user input handling.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false otherwise
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //If the frame is closing, ignore the touches and return true
        if (frameClosed | frameClosing) return true;
        super.onTouchEvent(event);

        if (doneButton.isPressed())
        {
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
            frameClosed = true;
        }
        else if (nextButton.isPressed())
        {
            nextPressed();
        }
        else if (prevButton.isPressed())
        {
            previousPressed();
        }
        if (frameClosed)
        {
            changeScreen();
        }

        return true;
    }
}
