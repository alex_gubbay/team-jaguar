package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/11/09.
 * UICheckBox class. A CheckBox class
 *
 * UIComponent: UIFrame class. Displays a checkbox that can be pressed to change a boolean value.
 * The boolean value can also be set and get programmatically.
 * (Please note that this is a custom implementation. Do not use this class as a reference unless you understand what is going on.)
 * Currently this class makes an assumption for the sprite images that they all follow the same format. Please refer to the frame1.png file in the drawables folder in res for more information
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class UICheckBox extends UIButtonFrame{
    private SpriteEntity trueImage, falseImage;
    private boolean value = false;

    /**
     * UICheckBox constructor. Sets the position and size.
     * @param _x         x position
     * @param _y         y position
     * @param _width     width
     * @param _height    height
     */
    public UICheckBox(float _x, float _y, float _width, float _height) {
        super(_x, _y, _width, _height, R.drawable.button1_unpressed, R.drawable.button1_pressed, R.drawable.button1_overlay);
        trueImage = new SpriteEntity(x, y, width, height, R.drawable.tick);
        falseImage = new SpriteEntity(x, y, width, height, R.drawable.cross);
    }

    /**
     * Sets the value of the boolean that this check box holds
     * @param _value    New boolean value to be set
     */
    public void setValue(boolean _value)
    {
        value = _value;
    }

    /**
     * Update method. Calls parent's update method.
     * Updates the true and false images.
     * @param delta    time passed since last update cycle
     * @see UIButtonFrame
     * @see SpriteEntity
     */
    @Override
    public void update(float delta) {
        super.update(delta);
        trueImage.update(delta);
        falseImage.update(delta);
    }

    public boolean getValue()
    {
        return value;
    }

    @Override
    public void changePosition(float changeInX, float changeInY) {
        super.changePosition(changeInX, changeInY);
        trueImage.changePosition(changeInX, changeInY);
        falseImage.changePosition(changeInX, changeInY);
    }

    @Override
    public void setPosition(float _x, float _y) {
        super.setPosition(_x, _y);
        trueImage.setPosition(_x, _y);
        falseImage.setPosition(_x, _y);
    }

    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        if (value) trueImage.render(canvas);
        else falseImage.render(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        if (pressed)
        {
            value = !value;
            if (event.getAction() == MotionEvent.ACTION_UP)
                return true;
        }
        return false;
    }
}
