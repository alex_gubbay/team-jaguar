package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Graphics.Animation;
import com.example.nightcrysis.sunka.Graphics.Frame;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Class for entities that has an Animation, and requires the functionality of class GameEntity
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class AnimationEntity extends GameEntity{
    /**
     * Constructor for AnimationEntity
     * @param _x x position on screen
     * @param _y y position on screen
     * @param _width width (in pixels) of entity
     * @param _height width (in pixels) of entity
     * @param animationID The resource id of the animation sprite sheet
     * @see GameEntity
     * */
    public AnimationEntity(float _x, float _y, float _width, float _height, int frameWidth, int frameHeight, Frame[] frames, float durationPerFrame, int animationID)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        animation = new Animation(x, y, width, height, frameWidth, frameHeight, frames, durationPerFrame, animationID);
    }

    /** Updates the position of the animation object, and its frame delay
     * @param delta Time passed since last update cycle took place.
     * @see GameEntity*/
    @Override
    public void update(float delta) {
        animation.setPosition(x, y);
        animation.update(delta);
    }

    /**
     * Renders the animation
     * @param canvas Canvas object that is passed for drawing calls
     * @see GameEntity
     */
    @Override
    public void render(Canvas canvas) {
        animation.render(canvas);
    }

    /** Method for setting needsRemoval to true, used for ArrayList iterations in the application */
    public void remove()
    {
        needsRemoval = true;
    }

    /**
     * @return false because nothing needs to be done
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface*/
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
