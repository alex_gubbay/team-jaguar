package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/11/14.
 * Event made specifically for pebble movement.
 * Changes a component's size to a certain ratio, then back again smoothly.
 *
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class TempSizeEvent implements GameEntityInterface {
    private GameEntity puppet;
    private boolean needsRemoval = false;
    private float duration, centerX, centerY, originalWidth, originalHeight,
            currentWidth, currentHeight, changeInWidth, changeInHeight,
            changeOfChangeInWidth, changeOfChangeInHeight;

    /** Constructor of making pebbles change its size, then back again.
     * It will reach its maximum size at half the duration, then back to its original size at
     * the end of the duration.
     * @param _puppet    sets the GameEntity to be controlled
     * @param _duration Total duration of event
     * @param _ratio      Floating-point value: The ratio of size the GameEntity changes to and back fro divided by 10
     * */
    public TempSizeEvent(GameEntity _puppet, float _ratio, float _duration)
    {
        puppet = _puppet;
        duration = _duration;

        //Initializing variables
        originalWidth = puppet.getWidth();
        originalHeight = puppet.getHeight();
        currentWidth =  originalWidth;
        currentHeight = originalHeight;

        //Calculating values for changes in size, and changes of changes in size
        //Formula is produced by mathematical integration and quadratics
        changeOfChangeInWidth = (float)(0.25f + Math.sqrt(Math.abs((32 * originalWidth * (_ratio - 1)) / (duration * duration))));
        changeOfChangeInHeight = (float)(0.25f + Math.sqrt(Math.abs((32 * originalHeight * (_ratio - 1)) / (duration * duration))));

        //Initial changes
        changeInWidth = (duration * changeOfChangeInWidth) / 2;
        changeInHeight = (duration * changeOfChangeInHeight) / 2;
    }

    /**
     * Update method. At each call, it sets changes the width and height of the controlled
     * object by a certain amount, then also changes the amount of change of width and height
     * Lastly the position of the object is set relative to its center.
     *
     * Very computationally and mathematically intense method. Fancy animations should be set
     * to false if the android device cannot cope with the computational requirement.
     * @param delta The time elapsed since the last update cycle
     * @see GameEntityInterface
     */
    @Override
    public void update(float delta) {

        //Setting the center position of the object
        centerX = puppet.getX() + puppet.getWidth() / 2;
        centerY = puppet.getY() + puppet.getHeight() / 2;

        //Changing size of object
        currentWidth += changeInWidth * delta;
        currentHeight += changeInHeight * delta;

        if (duration <= 0)
        {
            puppet.remove();
            puppet.setSize(originalWidth, originalHeight);
            puppet.setPosition(centerX - originalWidth / 2, centerY - originalHeight / 2);
            needsRemoval = true;
            return;
        }
        duration -= delta;

        //Changing the change of size of object
        changeInWidth -= changeOfChangeInWidth * delta;
        changeInHeight -= changeOfChangeInHeight * delta;

        //Setting its size and its position
        puppet.setSize(currentWidth, currentHeight);
        puppet.setPosition(centerX - currentWidth/2, centerY - currentHeight/2);

    }

    /**
     * Rendering method. Does nothing
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {

    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
