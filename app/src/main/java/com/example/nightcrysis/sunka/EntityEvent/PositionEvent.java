package com.example.nightcrysis.sunka.EntityEvent;

import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Specialized class used to control the position of an instance of GameEntityInterface
 * programmatically.
 * Extends GameEntityInterface for update, render, onTouchEvent and getNeedsRemoval methods
 */
public class PositionEvent implements GameEntityInterface {
    private GameEntity puppet;
    private boolean needsRemoval = false, removeAtEnd;
    private float destinationX, destinationY, velocityX, velocityY, duration;

    /** Contructor for PositionEvent. Using a given destination it automatically calculates the
     * speed using the total duration available
     * @param _duration Sets the duration of how long the moving should take
     * @param _puppet Sets the object that is wanted to be moved
     * @param isRelativePosition if the positions stated in toX and toY are relative to the puppet.
     *                           If true it will move toX and toY from the puppet's location,
     *                           if false it will move to absolute position (toX, toY) on screen
     * @param toX
     * @param toY
     * @param _removeAtEnd Sets when at the end of movement duration, the puppet should be removed (set needsRemoval to true)
     * */
    public PositionEvent(GameEntity _puppet, float toX, float toY, float _duration, boolean isRelativePosition, boolean _removeAtEnd)
    {
        removeAtEnd = _removeAtEnd;
        duration = _duration;
        puppet = _puppet;
        if (isRelativePosition)
        {
            destinationX = puppet.getX() + toX;
            destinationY = puppet.getY() + toY;
        }
        else
        {
            destinationX = toX;
            destinationY = toY;
        }
        velocityX = (destinationX - puppet.getX()) / duration;
        velocityY = (destinationY - puppet.getY()) / duration;
    }

    /**
     * Update method, Changes the position of the controlling GameEntity object
     * by a fixed amount of X and Y at each variable time passed.
     * @param delta The time elapsed since the last update cycle
     * @see GameEntityInterface
     */
    @Override
    public void update(float delta) {
        puppet.changePosition(velocityX * delta, velocityY * delta);
        duration -= delta;
        if (duration < 0)
        {
            puppet.setPosition(destinationX, destinationY);
            if (removeAtEnd)
                puppet.remove();
            needsRemoval = true;
        }
    }

    /**
     * Rendering method. Does nothing
     * @param canvas Canvas object used to draw images on screen
     * @see GameEntityInterface
     */
    @Override
    public void render(Canvas canvas) {

    }

    /**
     * User input method. Always returns false (nothing happens)
     * @param event The MotionEvent that the device has detected
     * @return always false
     * @see GameEntityInterface
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntityInterface
     */
    @Override
    public boolean getNeedsRemoval() {
        return needsRemoval;
    }
}
