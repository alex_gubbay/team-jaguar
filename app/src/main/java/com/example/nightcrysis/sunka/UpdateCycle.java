package com.example.nightcrysis.sunka;

import android.util.Log;

import com.example.nightcrysis.sunka.Screen.ScreenManager;

/**
 * Created by NIghtCrysIs on 2015/10/19.
 * UpdateCycle class, extends Thread to perform updates and drawing on a completely separate thread
 * Handles both update and rendering calls to ScreenManager and GameDisplay respectively
 * The game update cycle method used for the entire game architecture is placed here. This class
 * is part of the core game framework.
 * @see ScreenManager
 * @see GameDisplay
 */
public class UpdateCycle extends Thread{
    public boolean gameRunning = false; // Volatile because it is accessed by two threads (this and MainActivity)

    private Object pauseObject; //Object used for synchronization of threads when pausing and resuming threads
    private Thread renderThread = new Thread(){
        @Override
        public void run() {
            GlobalVariables.gameDisplay.render();
        }
    };

    private static final int fps = 60, //Updates per second
                                sleepTimeInterval = 1000 / fps; //In milliseconds
    private long lastUpdateTime = 0;
    private float delta = 0, lastFrameRate = 0;

    /**
     * Constructor for the UpdateCycle.
     * Default frame rate is set to 60 frames per second.
     */
    public UpdateCycle()
    {
        pauseObject = new Object();
        gameRunning = true;
        lastUpdateTime = System.nanoTime(); //Accounts system startup and setup time
    }

    /**
     * Pauses the thread
     */
    public void onPause()
    {
        synchronized (pauseObject) {
            gameRunning = false;
        }
    }

    /**
     * Resumes the thread
     */
    public void onResume() {
        synchronized (pauseObject) {
            gameRunning = true;
            pauseObject.notifyAll();
        }
    }

    /**
     * Called by start(), and runs this method in a separate thread
     * This method loops by default, and will not end until the application sleeps or closes.
     * Update loop is within this method. Where the loop will sleep for approximately 1/60 per
     * second to prevent the program from running the loop infinitely.
     */
    @Override
    public void run()
    {
        while (gameRunning) //Exits when gameRunning is false
        {
            //Start of loop

            // Sleep method, avoids CPU clogging up with no upper-bounds in looping through the game cycle.
            // Currently is redundant, as the game runs at unstable frame rates between 12 to 28fps, so sleep is not called at all.
            try
            {
                sleep(sleepTimeInterval); //Takes milli seconds as input
            }
            catch (Exception e)
            {
                Log.e("UpdateCycle", "Error in sleep() method in UpdateCycle class");
            }

            // Sets delta, time elapsed since last cycle
            // Nanoseconds/1000000000 = seconds -> For many purposes such as animation frame duration
            delta = (System.nanoTime() - lastUpdateTime) / 1000000000.0f;
            lastUpdateTime = System.nanoTime(); // Sets last time the loop cycle has been performed
            lastFrameRate = 1/delta;

            //Shows the actual fps of game (60 FPS OMG)
            //Log.e("FPS", String.valueOf(lastFrameRate));

            //Rendering the game
            GlobalVariables.currentActivity.runOnUiThread(renderThread);

            //Update: Passes the time elapsed since last cycle
            ScreenManager.update(delta);

            synchronized (pauseObject)
            {
                while (!gameRunning)
                {
                    try {
                        pauseObject.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            //End of loop
        }
    }
}
