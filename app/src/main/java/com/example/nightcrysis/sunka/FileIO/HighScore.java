package com.example.nightcrysis.sunka.FileIO;

/**
 * Created by Alex on 21/10/2015.
 * Maintains a single highscore object
 */
public class HighScore implements Comparable<HighScore> {

    String name;
    int scoreValue;

    /**
     * Creates a new highscore object
     *
     * @param name       name of player with highscore
     * @param scoreValue scorevalue of highscore
     */
    public HighScore(String name, int scoreValue){
        this.name=name;
        this.scoreValue=scoreValue;
    }

    /**
     * @return name of player with highscore
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return returns score value of highscore
     */
    public int getScoreValue() {
        return scoreValue;
    }

    /**
     *
     * @return player object in format name,score
     */
    @Override
    public String toString(){
        return name+","+scoreValue;

    }


    /**
     * Custom sorting camparison logic
     *
     * @param another object to compare to.
     * @return int return value
     */
    @Override
    public int compareTo(HighScore another) {
        if (another.getScoreValue() == this.scoreValue) {
            return 0;
        } else if (another.getScoreValue() > this.getScoreValue()) {
            return 1;
        } else {
            return -1;
        }
    }
}
