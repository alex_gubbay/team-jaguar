package com.example.nightcrysis.sunka.GameEntity;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/11/08.
 * Class for cloud objects used in the main menu backgrounds
 */
public class Cloud extends GameEntity{
    private static final float absoluteSpeed = 20; //Defines the absolute speed to calculate speed upon, per second

    private float speed; //Speed is calculated relative to the cloud's size

    /** Constructor for Cloud, all variables are automatically and randomly generated */
    public Cloud()
    {
        //Randomly generate the cloud's size
        float sizeRatio = GlobalVariables.random.nextInt(1) + GlobalVariables.random.nextFloat() + GlobalVariables.random.nextFloat();
        int imageID = 0;

        //Randomly choose an image of a cloud
        switch (GlobalVariables.random.nextInt(7))
        {
            case 0: imageID = R.drawable.cloud1; break;
            case 1: imageID = R.drawable.cloud2; break;
            case 2: imageID = R.drawable.cloud3; break;
            case 3: imageID = R.drawable.cloud4; break;
            case 4: imageID = R.drawable.cloud5; break;
            case 5: imageID = R.drawable.cloud6; break;
            case 6: imageID = R.drawable.cloud7; break;
        }

        //Set the cloud width, height and instantiate the cloud sprite
        width = ImageManager.getBitmapWidth(imageID) * sizeRatio * GlobalVariables.backgroundRatio;
        height = ImageManager.getBitmapHeight(imageID) * sizeRatio * GlobalVariables.backgroundRatio;

        //Setting the cloud's position at the utmost right hand side
        x = GlobalVariables.backgroundOffsetX + GlobalVariables.backgroundWidth;
        y = GlobalVariables.random.nextInt(400) * GlobalVariables.backgroundRatio; //400 is just under half the image size

        //Set the cloud width, height and instantiate the cloud sprite
        sprite = new Sprite(x, y, width, height, imageID);

        //Setting the speed using exponential function
        speed = absoluteSpeed * (float) Math.pow(sizeRatio, 1.2) * GlobalVariables.backgroundRatio;
    }

    /**
     * Update method. Updates the cloud's x position and checks if it is off screen (in
     * which it will be removed, by setting needsRemoval to true)
     * @param delta The time elapsed since the last update cycle
     * @see GameEntity
     */
    @Override
    public void update(float delta) {
        x -= speed * delta;
        if (x < GlobalVariables.backgroundOffsetX - width)
            needsRemoval = true;
        super.update(delta);
    }

    /**
     * Render method. Renders the cloud.
     * @param canvas Canvas object that is passed for drawing calls
     * @see GameEntity
     */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);
    }

    /**
     * Getter method for needsRemoval
     * @return needsRemoval as a boolean
     * @see GameEntity
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
