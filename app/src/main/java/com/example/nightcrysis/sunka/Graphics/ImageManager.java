package com.example.nightcrysis.sunka.Graphics;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;

import java.util.ArrayList;

/**
 * Created by NIghtCrysIs on 2015/10/20.
 * Handles references to each sprite loaded.
 * Not completely efficient, will be developed in the future. Changes made will not affect other
 * modules or aspects of the entire application.
 *
 * This class is needed to prevent the game from creating new instances of an image for
 * objects that uses the same image. E.g. Small pot sprites may be loaded
 * to RAM 14 times, and this is not efficient.
 */
public class ImageManager {
    private static ArrayList<Bitmap> loadedBitmaps = new ArrayList<>();
    private static ArrayList<Integer> loadedBitmapIds = new ArrayList<>();
    private static BitmapFactory.Options decodeDimensionsOnly= new BitmapFactory.Options(); //For decoding bitmap size
    private static float imageScale = 1f; //Global scaling value for all images in game.

    /**
     * Initialization method, called at start of application
     */
    public static void initialize()
    {
        decodeDimensionsOnly.inJustDecodeBounds = true;

        //If any of the dimension is greater than the screen size
        if (1500 > GlobalVariables.screenSizeX | 1000 > GlobalVariables.screenSizeY)
        {
            float scaleX = GlobalVariables.screenSizeX / 1500f,
                    scaleY = GlobalVariables.screenSizeY / 1000f;

            //Set new scaled down value
            if (scaleX < scaleY) imageScale = scaleX;
            else imageScale = scaleY;
        }

        //Preload all images before game starts, so no lag issues with loading bitmap at run time.
        preloadImages();
    }

    /** Preloads all images there is for the game. This is called at the beginning, and only called
     * once in the entire life time of the game. */
    private static void preloadImages()
    {
        getBitmap(R.drawable.cloud3);
        getBitmap(R.drawable.cloud4);
        getBitmap(R.drawable.cloud5);
        getBitmap(R.drawable.cloud6);
        getBitmap(R.drawable.cloud7);
        getBitmap(R.drawable.cross);
        getBitmap(R.drawable.frame1_overlay);
        getBitmap(R.drawable.frame1_pressed);
        getBitmap(R.drawable.frame1_unpressed);
        getBitmap(R.drawable.frame2_overlay);
        getBitmap(R.drawable.frame2_pressed);
        getBitmap(R.drawable.frame2_unpressed);
        getBitmap(R.drawable.game_background);
        getBitmap(R.drawable.loading_animation);
        getBitmap(R.drawable.main_background);
        getBitmap(R.drawable.main_foreground);
        getBitmap(R.drawable.main_menu_back);
        getBitmap(R.drawable.main_menu_back_pressed);
        getBitmap(R.drawable.main_menu_exit);
        getBitmap(R.drawable.main_menu_exit_pressed);
        getBitmap(R.drawable.main_menu_highscores);
        getBitmap(R.drawable.main_menu_highscores_pressed);
        getBitmap(R.drawable.main_menu_instructions);
        getBitmap(R.drawable.main_menu_instructions_pressed);
        getBitmap(R.drawable.main_menu_settings);
        getBitmap(R.drawable.main_menu_settings_pressed);
        getBitmap(R.drawable.main_menu_start_game);
        getBitmap(R.drawable.main_menu_start_game_pressed);
        getBitmap(R.drawable.menu_button_overlay);
        getBitmap(R.drawable.menu_button_pressed);
        getBitmap(R.drawable.menu_button_unpressed);
        getBitmap(R.drawable.online_menu_host);
        getBitmap(R.drawable.online_menu_host_pressed);
        getBitmap(R.drawable.online_menu_join);
        getBitmap(R.drawable.online_menu_join_pressed);
        getBitmap(R.drawable.play_menu_multiplayer);
        getBitmap(R.drawable.play_menu_multiplayer_pressed);
        getBitmap(R.drawable.play_menu_online);
        getBitmap(R.drawable.play_menu_online_pressed);
        getBitmap(R.drawable.play_menu_single_player);
        getBitmap(R.drawable.play_menu_single_player_pressed);
        getBitmap(R.drawable.rope);
        getBitmap(R.drawable.rope2);
        getBitmap(R.drawable.rope3);
        getBitmap(R.drawable.slider_bar_blue);
        getBitmap(R.drawable.slider_bar_empty);
        getBitmap(R.drawable.slider_bar_green);
        getBitmap(R.drawable.slider_bar_red);
        getBitmap(R.drawable.slider_button);
        getBitmap(R.drawable.slider_button_pressed);
        getBitmap(R.drawable.slider_frame);
        getBitmap(R.drawable.small_hole);
        getBitmap(R.drawable.sunka_title);
        getBitmap(R.drawable.tick);
        getBitmap(R.drawable.title_game_selection);
        getBitmap(R.drawable.title_highscores);
        getBitmap(R.drawable.title_instructions);
        getBitmap(R.drawable.title_online_game);
        getBitmap(R.drawable.title_select_difficulty);
        getBitmap(R.drawable.title_settings);
        getBitmap(R.drawable.tray_image);
        getBitmap(R.drawable.volume_off);
        getBitmap(R.drawable.volume_off_pressed);
        getBitmap(R.drawable.volume_on);
        getBitmap(R.drawable.volume_on_pressed);
        getBitmap(R.drawable.big_hole);
        getBitmap(R.drawable.button1_overlay);
        getBitmap(R.drawable.button1_pressed);
        getBitmap(R.drawable.button1_unpressed);
        getBitmap(R.drawable.cloud1);
        getBitmap(R.drawable.cloud2);
        getBitmap(R.drawable.highlight_overlay_1);
        getBitmap(R.drawable.highlight_overlay_2);
        getBitmap(R.drawable.highlight_overlay_3);
        getBitmap(R.drawable.highlight_overlay_4);

        //These last four  are very small so doesn't matter
        loadNewBitmapOriginalSize(R.drawable.highscore_first);
        loadNewBitmapOriginalSize(R.drawable.highscore_rest);
        loadNewBitmapOriginalSize(R.drawable.highscore_second);
        loadNewBitmapOriginalSize(R.drawable.highscore_third);
    }

    /**
     * Static method for retrieving the bitmap required
     * @param id    Resource ID for the image file (currently all in png format)
     * @return The bitmap found in the existing preloaded array.Or a newly loaded bitmap.
     * @see Bitmap
     */
    public static Bitmap getBitmap(int id)
    {
        for (int i = loadedBitmapIds.size() - 1; i >= 0; i--)
        {
            int x = loadedBitmapIds.get(i);
            if (x == id) return loadedBitmaps.get(i);
        }

        //If not found loads the image, and returns it
        return loadBitmap(id);
    }

    /**
     * Used to get a Bitmap file for Sprites and Animation.
     * @id integer value of the image file. File found by typing R.drawable.filename)
     * @return Bitmap object
     * @see Bitmap
     * */
    private static Bitmap loadBitmap(int id)
    {
        //First check the dimension of the image
        BitmapFactory.decodeResource(GlobalVariables.currentActivity.getResources(), id, decodeDimensionsOnly);

        //Scale value created for scaling down image (if needed)
        float imageWidth = decodeDimensionsOnly.outWidth * imageScale, imageHeight = decodeDimensionsOnly.outHeight * imageScale;

        //if it is still larger than any direction of the screen size
        if (imageWidth > GlobalVariables.screenSizeX | imageHeight > GlobalVariables.screenSizeY)
        {
            //Create scale values
            float scaleX = GlobalVariables.screenSizeX / imageWidth,
                    scaleY = GlobalVariables.screenSizeY / imageHeight;

            //Create scaled down value according to the smallest of them all.
            if (scaleX < scaleY)
            {
                imageWidth *= scaleX;
                imageHeight *= scaleX;
            }
            else
            {
                imageWidth *= scaleY;
                imageHeight *= scaleY;
            }
        }

        //Load bitmap according to the new scale, if applied
        Bitmap tempBitmap = BitmapFactory.decodeResource(GlobalVariables.currentActivity.getResources(), id),
        scaledBitmap = Bitmap.createScaledBitmap(tempBitmap, (int)imageWidth, (int)imageHeight, false); //bilinear filtering false, pixelized images looks fine.
        tempBitmap.recycle();
        loadedBitmaps.add(scaledBitmap);
        loadedBitmapIds.add(id);
        return scaledBitmap;
    }

    private static void loadNewBitmapOriginalSize(int id)
    {
        loadedBitmapIds.add(id);
        loadedBitmaps.add(BitmapFactory.decodeResource(GlobalVariables.currentActivity.getResources(), id));
    }

    /**
     * Getting the width of image specified programmatically
     * (Note that the image may be resized in preload, depending on device screen resolution)
     * @param id The Resource id of the image
     * */
    public static int getBitmapWidth(int id)
    {
        for (int i = loadedBitmapIds.size() - 1; i >= 0; i--)
        {
            int x = loadedBitmapIds.get(i);
            if (x == id) return loadedBitmaps.get(i).getWidth();
        }

        //If not found. Loads the image, add it to the array list then return its width
        return loadBitmap(id).getWidth();
    }

    /**
     * Getting the height of image specified programmatically
     * (Note that the image may be resized in preload, depending on device screen resolution)
     * @param id The Resource id of the image
     * */
    public static int getBitmapHeight(int id)
    {
        for (int i = loadedBitmapIds.size() - 1; i >= 0; i--)
        {
            int x = loadedBitmapIds.get(i);
            if (x == id) return loadedBitmaps.get(i).getHeight();
        }

        //If not found. Loads the image, add it to the array list then return its height
        return loadBitmap(id).getHeight();
    }
}
