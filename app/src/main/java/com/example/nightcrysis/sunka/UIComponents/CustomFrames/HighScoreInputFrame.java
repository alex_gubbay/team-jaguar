package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.text.InputType;
import android.view.MotionEvent;
import android.widget.EditText;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.FileIO.DataManager;
import com.example.nightcrysis.sunka.FileIO.GamePlayer;
import com.example.nightcrysis.sunka.FileIO.HighScoreManager;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/13.
 * Specialized UIFrameWithOverlay for high score input
 * Prompts the user for high score input through the use of Android AlertDialog
 */
public class HighScoreInputFrame extends UIFrameWithOverlay{
    private UIButtonFrame okButton, cancelButton, nameInputButton;
    private int textColor = Color.argb(255, 246, 176, 57), win, score;
    private float sidePadding, textWidth, textHeight;
    private long gameTime;
    private String message = "Please enter your name";

    private static float textSize = -1;

    /**
     * Constructor for LeaveGameFrame
     * @param _win         If the player has won (1 = win, 0 = draw, -1 = loss)
     * @param _score       Score of the player
     * @param _gameTime    Total game duration of the game
     */
    public HighScoreInputFrame(int _win, int _score, long _gameTime) {
        super(0, 0, 0, 0, R.drawable.frame2_unpressed);
        win = _win;
        score = _score;
        gameTime = _gameTime;
        initialize();
    }

    /** Initialization method for instantiating objects and settings different variables */
    private void initialize()
    {
        if (textSize < 0)
        {
            textSize = UIUtilities.findTextSizeByHeight("A", GlobalVariables.backgroundHeight / 32);
        }
        sidePadding = GlobalVariables.backgroundHeight / 16;

        //Set the text size, as well as its width and height
        setTextSize();

        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 6);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), -height);

        float tempWidth = GlobalVariables.backgroundWidth / 5,
                tempHeight = height / 8;

        okButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        okButton.setText("Ok");
        okButton.setTextColor(textColor);

        cancelButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        cancelButton.setText("Cancel");
        cancelButton.setTextColor(textColor);

        nameInputButton = new UIButtonFrame(x + (tempWidth / 2), y + height - (tempHeight * 5f), width - tempWidth, tempHeight * 1.5f, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        nameInputButton.setText("Your name here");
        nameInputButton.setTextColor(textColor);

        uiSubComponents.add(okButton);
        uiSubComponents.add(cancelButton);
        uiSubComponents.add(nameInputButton);

        float displacement = (GlobalVariables.screenSizeY / 2) + (height * 0.5f);
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(okButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(cancelButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(nameInputButton, 0, displacement, 0.5f, true, false));
    }

    /** Setter method for text Color
     * @param color Color of text, passed as an integer */
    public void setTextColor(int color)
    {
        textColor = color;
    }

    /** Setter method for text size. Called internally */
    private void setTextSize()
    {
        GlobalVariables.paint.setTextSize(textSize);
        Rect rect = new Rect();
        GlobalVariables.paint.getTextBounds(message, 0, message.length(), rect);
        textWidth = rect.width();
        textHeight = rect.height();
    }

    /** Method for showing a Dialog with text input for the user name */
    private void editName()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(GlobalVariables.currentActivity);
        builder.setTitle("Enter your name:");

        final EditText input = new EditText(GlobalVariables.currentActivity);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                nameInputButton.setText(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /** Render method
     * @param canvas Canvas object for drawing
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(message, x + (width / 2) - (textWidth / 2), y + (height / 3) - (textHeight / 2), GlobalVariables.paint);
    }

    /** Touch event method.
     * Checks if buttons are pressed and executes certain actions if true
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface
     * @return true if user input coordinates collides with this object, false otherwise and closes */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (super.onTouchEvent(event))
        {
            if (okButton.isPressed())
            {
                frameClosed = true;

                // Save player results
                HighScoreManager highScoreManager = new HighScoreManager();
                highScoreManager.addScore(nameInputButton.getText(), score);
                DataManager dataManager = new DataManager();
                GamePlayer tempPlayer = dataManager.getPlayer(nameInputButton.getText());
                tempPlayer.addGame(score, win, gameTime);
                dataManager.addPlayer(tempPlayer);
            }
            else if (cancelButton.isPressed())
            {
                frameClosed = true;
            }
            else if (nameInputButton.isPressed())
            {
                editName();
            }
        }

        if (frameClosed & !needsRemoval)
        {
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f) * 2,
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(okButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(cancelButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(nameInputButton, 0, velocityY, 0, acceleration, 0.5f, true));

            okButton.setEnabled(false);
            cancelButton.setEnabled(false);
            nameInputButton.setEnabled(false);
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
        }

        return true;
    }
}
