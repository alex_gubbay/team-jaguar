package com.example.nightcrysis.sunka.UIComponents.CustomFrames;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.Screen.ScreenManager;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ColorFade;
import com.example.nightcrysis.sunka.UIComponents.UIButtonFrame;
import com.example.nightcrysis.sunka.UIComponents.UIFrameWithOverlay;
import com.example.nightcrysis.sunka.UIComponents.UIUtilities;

/**
 * Created by NIghtCrysIs on 2015/11/14.
 * Allows the player to select to go first or last. Specialized UIFrameWithOverlay
 */
public class SelectTurnFrame extends UIFrameWithOverlay{
    private UIButtonFrame firstButton, secondButton;
    private int textColor = Color.argb(255, 246, 176, 57);
    private float sidePadding, textWidth, textHeight;
    private String message = "Do you want to go first or second?";
    private boolean goFirst = false;

    private static float textSize = -1;

    /** Constructor for LeaveGameFrame */
    public SelectTurnFrame() {
        super(0, 0, 0, 0, R.drawable.frame2_unpressed);
        initialize();
    }

    /** Initialization method for instantiating objects and settings different variables */
    private void initialize()
    {
        if (textSize < 0)
        {
            textSize = UIUtilities.findTextSizeByHeight("A", GlobalVariables.backgroundHeight / 32);
        }
        sidePadding = GlobalVariables.backgroundHeight / 16;

        //Set the text size, as well as its width and height
        setTextSize();

        setSize(textWidth + sidePadding * 2, textHeight + sidePadding * 6);
        setPosition((GlobalVariables.screenSizeX / 2) - (width / 2), -height);

        float tempWidth = GlobalVariables.backgroundWidth / 4,
                tempHeight = height / 6;

        firstButton = new UIButtonFrame(x + (width / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        firstButton.setText("First");
        firstButton.setTextColor(textColor);

        secondButton = new UIButtonFrame(x + (width * 3 / 4) - (tempWidth / 2), y + height - (tempHeight * 1.5f), tempWidth, tempHeight, R.drawable.frame1_unpressed, R.drawable.frame1_pressed);
        secondButton.setText("Second");
        secondButton.setTextColor(textColor);

        uiSubComponents.add(firstButton);
        uiSubComponents.add(secondButton);

        float displacement = (GlobalVariables.screenSizeY / 2) + (height * 0.5f);
        uiSubComponents.add(new PositionEvent(this, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(firstButton, 0, displacement, 0.5f, true, false));
        uiSubComponents.add(new PositionEvent(secondButton, 0, displacement, 0.5f, true, false));
    }

    /** Setter method for text Color
     * @param color Color of text, passed as an integer */
    public void setTextColor(int color)
    {
        textColor = color;
    }

    /**
     * Getter method if the player wants to go first
     * @return goFirst as a boolean value
     */
    public boolean isGoFirst() {
        return goFirst;
    }

    /** Setter method for text size. Called internally */
    private void setTextSize()
    {
        GlobalVariables.paint.setTextSize(textSize);
        Rect rect = new Rect();
        GlobalVariables.paint.getTextBounds(message, 0, message.length(), rect);
        textWidth = rect.width();
        textHeight = rect.height();
    }

    /** Render method
     * @param canvas Canvas object for drawing
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        super.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(message, x + (width / 2) - (textWidth / 2), y + (height / 2) - (textHeight / 2), GlobalVariables.paint);
    }

    /** Touch event method. Completely overrides super.onTouchEvent to block user from cancelling
     * the selection of turns.
     * Checks if the user input has touched the frame, and passes the input
     * event to other objects for further user input handling.
     * Checks if buttons are pressed and executes certain actions if true
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates are within the frame, false if otherwise and closes
     * @see UIFrameWithOverlay*/
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
        {
            for (GameEntityInterface x : uiSubComponents)
                x.onTouchEvent(event);

            if (firstButton.isPressed())
            {
                goFirst = true;
                frameClosed = true;
            }
            else if (secondButton.isPressed())
            {
                goFirst = false;
                frameClosed = true;
            }
        }

        if (frameClosed & !needsRemoval)
        {
            outsideFrameTouched = true;
            float acceleration = -AccelerationEvent.accelYOffScreenValueUp(y, height, height / 2, 0.5f) * 2,
                    velocityY = height/2;
            uiSubComponents.add(new AccelerationEvent(this, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(firstButton, 0, velocityY, 0, acceleration, 0.5f, true));
            uiSubComponents.add(new AccelerationEvent(secondButton, 0, velocityY, 0, acceleration, 0.5f, true));

            firstButton.setEnabled(false);
            secondButton.setEnabled(false);
            darkOverlay = new ColorFade(false, 0.5f, 180, 0, 0, 0, true);
        }

        // Always returns true because this UIFrame take precedence over all other components on screen.
        return true;
    }
}
