package com.example.nightcrysis.sunka.FileIO;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.NoSuchElementException;

/**
 * Class for client side communication in sockets. Internal class, never directly interacted with
 * by the game. All communication done through static {@link DataTransmissionManager}
 * @Author Alex
 */
public class client {

    private BufferedReader incomingReader;
    private PrintWriter outgoingSender;

    /**
     * Creates a new sockets client on the ip and port supplied.
     *
     * @param ip ip of server to connect to.
     * @param port port of server to connect to.
     *
     */
    public client(String ip, int port) {

        //Create an instance of AsyncTask
        ClientAsyncTask clientControl = new ClientAsyncTask(ip, port);
        clientControl.start();
        //Pass the server ip, port and client message to the AsyncTask

    }


    /**
     * Async class for all remote connection IO. Interacts with static {@link DataTransmissionManager}
     * @Author Alex
     *
     */
    class ClientAsyncTask extends Thread{
        String ip;
        int port;
        public ClientAsyncTask(String _ip, int _port)
        {
            ip = _ip;
            port = _port;

        }
        @Override
        /**
         * Threaded task for client.
         * @param values dtata from AsyncTask input
         */
       public void run() {
            String result = null;
            try {
                //socket created with ip and port from socketData
                Socket socket = new Socket(ip, port);
                //DataTransmissionManager will be updated with current connection state.
                Log.d("CONNECTED", "READERS AND INPUTS ESTABLISHED");
                DataTransmissionManager.setConnected(true);

                //loop control. Will become true when connection is to be closed.
                boolean stop = false;

                while (!stop) {
                    //check if connection should close
                    stop = DataTransmissionManager.connectionShouldClose();
                    // get new messages to send
                    try {

                        String transmit = DataTransmissionManager.getMessagesToSend().removeLast();
                        if(transmit!=""){
                        outgoingSender = new PrintWriter(socket.getOutputStream(), true);
                        Log.d("WARN", "MESSAGE SENT SUCCESSFULLY: "+transmit);}
                        else{
                            throw new NoSuchElementException();
                        }
                        outgoingSender.println(transmit);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NoSuchElementException e) {
                        Log.d("WARN", "TRIED TO SEND MESSAGE, NONE FOUND (THIS IS NORMAL)");
                    }


                    try {
                        //creates inputStreamReader, and then a buffered reader from it.
                        InputStreamReader inputStream = new InputStreamReader(socket.getInputStream());
                        incomingReader = new BufferedReader(inputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    String message;

                    try {
                        if (incomingReader.ready()) {
                            message = incomingReader.readLine();
                            //remote has sent a message instructing connection to close.
                            if (message.equals("close")) {
                                //return close instruction
                                DataTransmissionManager.queueMessageForSending("close");
                                //exit loop, jump to close instructions.
                                break;

                            }
                            DataTransmissionManager.receiveMessage(message);
                        }
                    } catch (IOException e) {

                        Log.d("ERROR", "IO ERROR");
                        Log.e("ERROR", e.toString());
                    }
                    //reduces cpu usage
                    Thread.sleep(1000);

                }
                //close readers and connections.

                incomingReader.close();
                if (outgoingSender != null)
                {
                    outgoingSender.flush();
                    outgoingSender.close();
                    DataTransmissionManager.setConnected(false);
                    Log.d("Test", "Connection Closed");
                }

                //Close the client socket
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return result;
        }

    }
}
