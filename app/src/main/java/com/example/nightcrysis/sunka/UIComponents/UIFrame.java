package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.Physics;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by NIghtCrysIs on 2015/10/21.
 * Frame class. Functions similar to a window in Windows, Mac or Linux. Programmatically
 * resizable.
 * UIComponent: UIFrame class. Displays a frame to the window, there will be two derived class from the UIFrame class.
 * (Please note that this is a custom implementation. Do not use this class as a reference unless you understand what is going on.)
 * Currently this class makes an assumption for the sprite images that they all follow the same format. Please refer to the frame1.png file in the drawables folder in res for more information
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */

public class UIFrame extends GameEntity
{
    protected Bitmap frameSprite = null;
    protected float padding = 0, scale = 1.0f;
    protected int tileWidth = 0, tileHeight = 0;
    protected ArrayList<GameEntityInterface> uiSubComponents = null;

    /**
     * Constructor for UIFrame. Sets the position and size of the frame.
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     */
    public UIFrame(float _x, float _y, float _width, float _height, int frameSpriteID)
    {
        initialize(_x, _y, _width, _height, frameSpriteID);
    }

    /**
     * Constructor for UIFrame but with frameSide padding added. Currently does nothing and is not used
     * @param _x               x position
     * @param _y               y position
     * @param _width           width
     * @param _height          height
     * @param frameSpriteID    Resource ID for the frame sprite
     * @param frameSpriteID
     * @param frameSidePadding
     * @deprecated because it does nothing and is not really used.
     */
    public UIFrame(float _x, float _y, float _width, float _height, int frameSpriteID, float frameSidePadding)
    {
        padding = frameSidePadding;
        initialize(_x, _y, _width, _height, frameSpriteID);
    }

    private void initialize(float _x, float _y, float _width, float _height, int frameSpriteID)
    {
        x = (int)_x;
        y = (int)_y;
        width = (int)_width;
        height = (int)_height;

        //Setups the frame for the frame
        frameSprite = ImageManager.getBitmap(frameSpriteID);

        //Setups the tileWidth and tileHeight fo the rendering method.
        tileWidth = frameSprite.getWidth()/4;
        tileHeight = frameSprite.getHeight()/4;

        uiSubComponents = new ArrayList<>();
    }

    /**
     * Adds an GameEntity object to subComponent array list. So that it will be rendered,
     * updated and be passed a user input whenever the same is done to the UIFrame.
     * @param subComponent        GameEntity object to be added
     * @param relativePosition    Relative If the object's position is relative to the UIFrame
     */
    public void addUiSubComponent(GameEntity subComponent, boolean relativePosition)
    {
        if (relativePosition)
            subComponent.changePosition(x, y);
        uiSubComponents.add(subComponent);
    }

    /**
     * Sets the scale of the frame sprite itself
     * @param _scale    scale of sprite
     */
    public void setScale(float _scale)
    {
        scale = _scale;
    }

    /**
     * Update method. Updates the GameEntityInterface objects added to uiSubComponents.
     * It also handles the removal of such object from uiSubComponents
     * @param delta The time elapsed since the last update cycle
     */
    @Override
    public void update(float delta)
    {
        Iterator<GameEntityInterface> i = uiSubComponents.iterator();

        while (i.hasNext())
        {
            GameEntityInterface temp = i.next();
            if (temp.getNeedsRemoval())
                i.remove();
            else temp.update(delta);
        }
    }

    /**
     * Render method. Renders the frame itself.
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas)
    {
        //Drawing stuff, very complicated algorithm!
        //Handling corner sublayer renderings
        canvas.drawBitmap(frameSprite, new Rect(0, tileHeight * 2, tileWidth, tileHeight * 3), new RectF(x, y, x + tileWidth * scale, y + tileHeight * scale), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(tileWidth, tileHeight * 2, tileWidth * 2, tileHeight * 3), new RectF(x + width - (tileWidth * scale), y, x + width, y + (tileHeight * scale)), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(0, tileHeight * 3, tileWidth, tileHeight * 4), new RectF(x, y + height - (tileHeight * scale), x + (tileWidth * scale), y + height), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(tileWidth, tileHeight * 3, tileWidth * 2, tileHeight * 4), new RectF(x + width - (tileWidth * scale), y + height - (tileHeight * scale), x + width, y + height), GlobalVariables.paint);

        //Handling the side sublayer renderings
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 2, tileHeight * 2, tileWidth * 3, tileHeight * 3), new RectF(x + (tileWidth * scale), y, x + width - (tileWidth * scale), y + (tileHeight * scale)), GlobalVariables.paint); //Top
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 3, tileHeight * 2, tileWidth * 4, tileHeight * 3), new RectF(x + (tileWidth * scale), y + height - (tileHeight * scale), x + width - (tileWidth * scale), y + height), GlobalVariables.paint); //Bottom
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 2, tileHeight * 3, tileWidth * 3, tileHeight * 4), new RectF(x, y + (tileHeight * scale), x + (tileWidth * scale), y + height - (tileHeight * scale)), GlobalVariables.paint); //Left
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 3, tileHeight * 3, tileWidth * 4, tileHeight * 4), new RectF(x + width - (tileWidth * scale), y + (tileHeight * scale), x + width, y + height - (tileHeight * scale)), GlobalVariables.paint); //Right
//
//        //Handling the middle sublayer rendering
        canvas.drawBitmap(frameSprite, new Rect((int)(tileWidth * 0.5), (int)(tileHeight * 2.5), (int)(tileWidth * 1.5), (int)(tileHeight * 3.5)), new RectF(x + (tileWidth * scale), y + (tileHeight * scale) - 1, x + width - (tileWidth * scale), y + height - (tileHeight * scale) + 1), GlobalVariables.paint);

//        //Handling the corner renderings
        canvas.drawBitmap(frameSprite, new Rect(0, 0, tileWidth, tileHeight), new RectF(x, y, x + tileWidth * scale, y + tileHeight * scale), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(tileWidth, 0, tileWidth * 2, tileHeight), new Rect((int)((x + width) - (tileWidth * scale)), (int)y, (int)(x + width), (int)(y + (tileHeight * scale))), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(0, tileHeight, tileWidth, tileHeight * 2), new RectF(x, y + height - (tileHeight * scale), x + (tileWidth * scale), y + height), GlobalVariables.paint);
        canvas.drawBitmap(frameSprite, new Rect(tileWidth, tileHeight, tileWidth * 2, tileHeight * 2), new RectF(x + width - (tileWidth * scale), y + height - (tileHeight * scale), x + width, y + height), GlobalVariables.paint);

//        //Handling the side renderings
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 2, 0, tileWidth * 3, tileHeight), new RectF(x + (tileWidth * scale), y, x + width - (tileWidth * scale), y + (tileHeight * scale)), GlobalVariables.paint); //Top
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 3, 0, tileWidth * 4, tileHeight), new RectF(x + (tileWidth * scale), y + height - (tileHeight * scale), x + width - (tileWidth * scale), y + height), GlobalVariables.paint); //Bottom
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 2, tileHeight, tileWidth * 3, tileHeight * 2), new RectF(x, y + (tileHeight * scale), x + (tileWidth * scale), y + height - (tileHeight * scale)), GlobalVariables.paint); //Left
        canvas.drawBitmap(frameSprite, new Rect(tileWidth * 3, tileHeight, tileWidth * 4, tileHeight * 2), new RectF(x + width - (tileWidth * scale), y + (tileHeight * scale), x + width, y + height - (tileHeight * scale)), GlobalVariables.paint); //Right

        //Render subUiComponents
        ArrayList<GameEntityInterface> tempArray = new ArrayList<>(uiSubComponents);
        Iterator<GameEntityInterface> i = tempArray.iterator();

        while (i.hasNext())
        {
            GameEntityInterface temp = i.next();
            if (temp.getNeedsRemoval())
                i.remove();
            else temp.render(canvas);
        }
    }

    /**
     * User input method. Passes the user input to GameEntityInterface objects in uiComponents.
     * @param event The MotionEvent that the device has detected
     * @return true if user input position is within the frame, false otherwise.
     */
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
        {
            for (GameEntityInterface x : uiSubComponents)
                x.onTouchEvent(event);

            return true;
        }
        return false;
    }
}