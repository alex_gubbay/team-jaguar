package com.example.nightcrysis.sunka.GameEntity;

import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;

import java.util.Stack;

/**
 * Created by tuffail on 21/10/15.
 * Adds onto the pot class by preventing the player from getting the pebbles from the big pot
 */
public class BigPot extends Pot{

    /**
     * Takes the top left coordinate of the big pot along with its attributes
     * @param _x X coordinate of big pot
     * @param _y Y coordinate of the big pot
     * @param _width Width of the big pot
     * @param _height Height of the big pot
     */
    public BigPot(float _x, float _y, float _width, float _height){
        super(_x,_y,_width,_height);

        sprite = new Sprite(x, y, width, height, R.drawable.big_hole);
        highlightedSprite1 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_1);
        highlightedSprite2 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_2);
        highlightedSprite3 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_3);
        highlightedSprite4 = new Sprite(x,y,width,height, R.drawable.highlight_overlay_4);
    }

    /**
     * Returns null so the user cannot get the pebbles from the big pot
     * @return returns @code null meaning it returns no pebbles
     */
    @Override
    public Stack<Pebble> getPebbles(){
        return null;
    }

    /**
     * Checks whether the big pot has been touched or not from the user touch event
     * @param event touch event from the user
     * @return whether the big pot has been touched ot not
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height);
    }

}
