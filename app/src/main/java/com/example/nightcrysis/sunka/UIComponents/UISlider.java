package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 23/10/2015.
 * Custom UI class for displaying a slider. Contains a minimum and maximum value
 * of which can be edited. The user can use the slider to change the value using the slider button
 * to drag it left and right.
 * This Slider class is horizontal.
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class UISlider extends GameEntity {
    /* The empty bar image and slider bar image should be of the same image size. The actual design should show the two overlaps perfectly. */
    private Bitmap sliderBar = null, sliderBarEmpty = null;
    private UIButton sliderButton = null;
    private int value = 0, minValue = 0, maxValue = 100;
    private float buttonWidth = 0, buttonHeight = 0; //This assumes the image to be symmetrically designed.
    private boolean touched = false;
    private float factor = 0;

    /**
     * Constructor for the UISlider class. Default range for the minimum value
     * and the maximum value are 0 to 100 respectively
     * Parameters passed are used to set the position and size of this UIComponent
     * @param _x
     * @param _y
     * @param _width
     * @param _height
     * @param sliderBarID
     * @param sliderBarEmptyID
     * @param sliderButtonUnpressedID
     * @param sliderButtonPressedID
     */
    public UISlider(float _x, float _y, float _width, float _height, int sliderBarID, int sliderBarEmptyID, int sliderButtonUnpressedID, int sliderButtonPressedID)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

        buttonWidth = ImageManager.getBitmapWidth(sliderButtonUnpressedID);
        buttonHeight = ImageManager.getBitmapHeight(sliderButtonUnpressedID);

        //sliderImage = ImageManager.getBitmap(sliderImageID);
        sliderBar = ImageManager.getBitmap(sliderBarID);
        sliderBarEmpty = ImageManager.getBitmap(sliderBarEmptyID);
        sliderButton = new UIButton(x, y, buttonWidth, buttonHeight, sliderButtonUnpressedID, sliderButtonPressedID);
    }

    /**
     * Setting the value range of the slider
     * @param min    minimum value (left end)
     * @param max    maximum value (right end
     */
    public void setValueRange(int min, int max)
    {
        minValue = min;
        maxValue = max;
    }

    /**
     * Sets the current slider value
     * @param _value    slider value
     */
    public void setCurrentValue(int _value)
    {
        value = _value;
    }

    /**
     * Getter method for the current value of the slider
     * @return value as an integer
     */
    public int getCurrentValue()
    {
        return value;
    }

    /**
     * Sets the button size of the slider
     * @param _width     width of the button
     * @param _height    height of the button
     */
    public void setButtonSize(int _width, int _height)
    {
        buttonWidth = _width;
        buttonHeight = _height;
        sliderButton.setSize(buttonWidth, buttonHeight);
    }

    /**
     * Getter method for the button width
     * @return buttonWidth as a floating-point value
     */
    public float getButtonWidth() {
        return buttonWidth;
    }

    /**
     * Getter method for the button height
     * @return buttonHeight as a floating-point value
     */
    public float getButtonHeight() {
        return buttonHeight;
    }

    /**
     * Getter method for touched value. Returns true if the slider button or the
     * slider bar has been touched.
     * @return touched
     * @see UISlider onTouchEvent method
     */
    public boolean getIsTouched(){
        return touched;
    }

    /** Update method. Updates the slider button's position according to the current
     * slider value set
     * @param delta time since last update
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void update(float delta) {
        sliderButton.setPosition((x + (width * factor)) - buttonWidth/2,
                y + (height/2) - buttonHeight/2);

        sliderButton.setSelected(touched);
    }

    /** Render method. Renders the slider bars and the slider button
     * @param canvas Canvas object for drawing
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public void render(Canvas canvas) {
        //Layered rendering approach.
        factor = (float)(value - minValue) / (float)(maxValue - minValue);
        canvas.drawBitmap(sliderBarEmpty, new Rect(0, 0, sliderBarEmpty.getWidth(), sliderBarEmpty.getHeight()), new RectF(x, y, x + width, y + height), GlobalVariables.paint);
        canvas.drawBitmap(sliderBar, new Rect(0, 0, (int)(sliderBar.getWidth() * factor), sliderBar.getHeight()), new RectF(x, y, (int)(x + (width * factor)), y + height), GlobalVariables.paint);
        sliderButton.render(canvas);
    }

    /** Touch event method. Detects if the user has pressed the slider bar or the slider button,
     * in which both cases will let the user to drag the slider button, by setting touched to true.
     * Touched becomes false if user event does not collide on touch DOWN, or when user has lifted
     * their finger up and that touched is true.
     * @param event The MotionEvent that the device has detected
     * @see com.example.nightcrysis.sunka.Interfaces.GameEntityInterface */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height) | sliderButton.onTouchEvent(event))
                {
                    touched = true;
                    return true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (touched)
                {
                    touched = false;
                    return true;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (touched)
                {
                    int tempValue = (int)((maxValue - minValue) * ((event.getX() - x) / width)) + minValue;
                    if (tempValue < minValue) tempValue = minValue;
                    if (tempValue > maxValue) tempValue = maxValue;
                    value = tempValue;
                    return true;
                }
                break;
        }
        return false;
    }
}
