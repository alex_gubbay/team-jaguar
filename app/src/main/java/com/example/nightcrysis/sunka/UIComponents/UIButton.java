package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.Sprite;
import com.example.nightcrysis.sunka.Physics;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/10/21.
 * Custom UI class for displaying a button, along with a set image that is resized
 * according to its specified width and height
 *
 * Extends GameEntity for update, render, onTouchEvent and getNeedsRemoval methods,
 * as well as position access and modification methods and variables
 */
public class UIButton extends GameEntity {
    protected boolean selected, pressed, enabled; //Selected means the user has pressed down and that this is selected. Pressed means that the user has touched, and untouched the button, making it pressed.
    private Sprite pressedSprite = null, unpressedSprite = null, disabledOverlay = null;

    private int textColor = Color.RED, pressedSoundID = -1;
    private float textX = 0, textY = 0, textSize = 32;
    private String text = "";

    /**
     * Constructor for UIButton. Sets the size and position of the UIButton.
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedSpriteImageID    unpressed sprite resource ID
     * @param pressedSpriteImageID      pressed sprite resource ID
     */
    public UIButton(float  _x, float _y, float _width, float _height, int unpressedSpriteImageID, int pressedSpriteImageID)
    {
        initialize(_x, _y, _width, _height, unpressedSpriteImageID, pressedSpriteImageID);
    }

    /**
     * Constructor for UIButton. Sets the size and position of the UIButton.
     * Has an extra parameter for disabled sprite to be set.
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedSpriteImageID    unpressed sprite resource ID
     * @param pressedSpriteImageID      pressed sprite resource ID
     * @param disabledOverlayImageID         disabled sprite resource ID
     */
    public UIButton(float  _x, float _y, float _width, float _height, int unpressedSpriteImageID, int pressedSpriteImageID, int disabledOverlayImageID)
    {
        initialize(_x, _y, _width, _height, unpressedSpriteImageID, pressedSpriteImageID);
        disabledOverlay = new Sprite(x, y, width, height, disabledOverlayImageID);
    }

    /**
     * Constructor for UIButton. Sets the size and position of the UIButton.
     * Has an extra parameter for disabled sprite to be set.
     * Further extension of text can be set with this constructor
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param unpressedSpriteImageID    unpressed sprite resource ID
     * @param pressedSpriteImageID      pressed sprite resource ID
     * @param disabledOverlayImageID         disables sprite resource ID
     * @param _text                          Text to be displayed over the UIButtonFrame
     */
    public UIButton(float  _x, float _y, float _width, float _height, int unpressedSpriteImageID, int pressedSpriteImageID, int disabledOverlayImageID, String _text)
    {
        text = _text;

        initialize(_x, _y, _width, _height, unpressedSpriteImageID, pressedSpriteImageID);
        disabledOverlay = new Sprite(x, y, width, height, disabledOverlayImageID);
        setTextPositions();
    }

    private void initialize(float  _x, float _y, float _width, float _height, int unpressedSpriteImageID, int pressedSpriteImageID)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

        selected = false;
        pressed = false;
        enabled = true;

        unpressedSprite = new Sprite(x, y, width, height, unpressedSpriteImageID);
        pressedSprite = new Sprite(x, y, width, height, pressedSpriteImageID);
    }

    private void setTextPositions()
    {
        //Calculates the position to draw the strings, so that it would be at the center of the button
        Rect textBounds = new Rect();
        GlobalVariables.paint.setTextSize(textSize);
        GlobalVariables.paint.getTextBounds(text, 0, text.length(), textBounds);
        textX = x + (width/2) - (textBounds.width()/2);
        textY = y + (height/2) + (textBounds.height()/2);
    }

    /**
     * Sets the text size shown manually
     * @param _textSize    new text size
     */
    public void setTextSize(float _textSize)
    {
        textSize = _textSize;
        setTextPositions();
    }

    /**
     * Sets the sound to be played when the button is pressed
     * @param soundID    Sound resource ID
     */
    public void setPressedSoundID(int soundID)
    {
        pressedSoundID = soundID;
    }

    /**
     * Sets the text size programmatically and automatically
     * @param xPadding    Spacing on both sides of the frame for the text
     * @param yPadding    Spacing on the top and bottom of the frame for the text.
     */
    public void setTextSizeAuto(float xPadding, float yPadding)
    {
        float textSize1 = UIUtilities.findTextSizeByWidth(text, width - (2 * xPadding));
        float textSize2 = UIUtilities.findTextSizeByHeight(text, height - (2 * yPadding));

        if (textSize1 < textSize2)
            textSize = textSize1;
        else textSize = textSize2;

        setTextPositions();
    }

    /**
     * Sets the text color
     * @param _textColor    new text color
     */
    public void setTextColor(int _textColor)
    {
        textColor = _textColor;
    }

    public void setText(String _text)
    {
        text = _text;
        setTextPositions();
    }

    /**
     * Getter method to check if the button is enabled.
     * @return enabled value
     */
    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * Sets is the UIButton is selected
     * @param value    selected value
     */
    public void setSelected(boolean value)
    {
        selected = value;
    }

    /**
     * Getter method to check if the UIButton is pressed.
     * If pressed, it will set pressed to false, then return true.
     * @return pressed boolean value
     */
    public boolean isPressed()
    {
        if (pressed) {
            pressed = false;
            return true;
        }
        return false;
    }

    /**
     * Setter method for setting if the UIButtonFrame is enabled
     * @param enabled    new enabled boolean value
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Method for setting the position of the button
     * @param _x    x coordinates as a floating-point value
     * @param _y    y coordinates as a floating-point value
     * @see GameEntity
     */
    @Override
    public void setPosition(float _x, float _y) {
        super.setPosition(_x, _y);

        pressedSprite.setPosition(x, y);
        unpressedSprite.setPosition(x, y);

        if (disabledOverlay != null)
            disabledOverlay.setPosition(x, y);
    }

    /**
     * method for setting the size of the button. The sprite is stretched accordingly
     * @param _width Sets the width of object
     * @param _height Sets the height of object
     * @see GameEntity
     */
    @Override
    public void setSize(float _width, float _height) {
        super.setSize(_width, _height);
        pressedSprite.setSize(width, height);
        unpressedSprite.setSize(width, height);

        if (disabledOverlay != null)
            disabledOverlay.setSize(width, height);
    }

    /**
     * Update method, Sets the position of the sprite at every call
     * @param delta The time elapsed since the last update cycle#
     */
    @Override
    public void update(float delta) {
        pressedSprite.setPosition(x, y);
        unpressedSprite.setPosition(x, y);
    }

    /**
     * Render method. Draws a pressed sprite if the button is pressed,
     * draws unpressed sprite otherwise
     * Disabled overlay sprite is also drawn if disabled and the sprite is specifies
     * in the constructor.
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas) {
        if (selected)
            pressedSprite.render(canvas);
        else unpressedSprite.render(canvas);

        GlobalVariables.paint.setColor(textColor);
        GlobalVariables.paint.setTextSize(textSize);
        canvas.drawText(text, textX, textY, GlobalVariables.paint);

        if (!enabled)
            if (disabledOverlay != null)
                disabledOverlay.render(canvas);
    }

    /**
     * User input method. Checks if the user has pressed the UIButton or not.
     * @param event The MotionEvent that the device has detected
     * @return true if user input coordinates collides with this object
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                if (enabled)
                    if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                    {
                        if (pressedSoundID >= 0)
                            SoundManager.playSound(pressedSoundID);
                        selected = true;
                        return true;
                    }
                break;
            case MotionEvent.ACTION_UP:
                if (selected)
                    if (Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                    {
                        SoundManager.playSound(R.raw.ui_press);
                        pressed = true;
                        selected = false;
                        return true;
                    }
                break;
            case MotionEvent.ACTION_MOVE:
                if (selected)
                    if (!Physics.checkPointRectangleCollision(event.getX(), event.getY(), x, y, width, height))
                        selected = false;
                    break;
        }
        return false;
    }
}
