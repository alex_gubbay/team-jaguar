package com.example.nightcrysis.sunka.UIComponents;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.FileIO.GamePlayer;
import com.example.nightcrysis.sunka.GameEntity.GameEntity;
import com.example.nightcrysis.sunka.GlobalVariables;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.Interfaces.GameEntityInterface;
import com.example.nightcrysis.sunka.R;

/**
 * Created by NIghtCrysIs on 2015/11/11.
 * Displays a HighScoreEntry in HighScoreFrame.
 * Shows the player statistics alongside their ranks.
 */
public class HighScoreEntry extends GameEntity {
    private static float fontSize = -1;

    private UIFrame frame;

    private String name, averageScore, totalTimePlayed = "", rank, entryScore, wins, draws, losses;

    /**
     * HighScoreEntry constructor. Sets the size and position, and retrieves the
     * corresponding statistics to be shown on screen.
     * Rankings devices the border colour the player entry has.
     * 1 = gold
     * 2 = silver
     * 3 = bronze
     * rest = green (complies with the jungle theme)
     * @param _x                             x position
     * @param _y                             y position
     * @param _width                         width
     * @param _height                        height
     * @param _rank                          Ranking of the player
     * @param _entryScore                    Entry score of the play
     * @param player                         Player object to retrieve statistics from.
     */
    public HighScoreEntry(float _x, float _y, float _width, float _height, int _rank, int _entryScore, GamePlayer player) {
        x = _x;
        y = _y;
        width = _width;
        height = _height;

        //Instantiating the frame object according to the player's rank
        if (_rank == 1)
            frame = new UIFrame(x, y, width, height, R.drawable.highscore_first);
        else if (_rank == 2)
            frame = new UIFrame(x, y, width, height, R.drawable.highscore_second);
        else if (_rank == 3)
            frame = new UIFrame(x, y, width, height, R.drawable.highscore_third);
        else
            frame = new UIFrame(x, y, width, height, R.drawable.highscore_rest);

        //Initializing static variables
        if (fontSize < 0)
        {
            fontSize = UIUtilities.findTextSizeByHeight("1", _height * 2 / 5);
        }

        rank = String.valueOf(_rank);
        entryScore = String.valueOf(_entryScore);
        name = player.getName();
        averageScore = String.valueOf(player.getAverageScore());

        //Sorting the time into hours, minutes and seconds
        long tempTimePlayer = player.getTotalTime();
        if (tempTimePlayer >= 60)
        {
            int seconds = (int) tempTimePlayer % 60;
            tempTimePlayer = (tempTimePlayer - seconds) / 60;
            if (tempTimePlayer >= 60)
            {
                int minutes = (int) tempTimePlayer % 60;
                tempTimePlayer = (tempTimePlayer - minutes) / 60;
                totalTimePlayed += tempTimePlayer + "h" + minutes + "m";
            }
            else
            {
                totalTimePlayed += tempTimePlayer + "m";
            }

            totalTimePlayed += seconds + "s";
        }
        else
            totalTimePlayed += tempTimePlayer + "s";

        draws = String.valueOf(player.getDraws());
        wins = String.valueOf(player.getWins());
        losses = String.valueOf(player.getLosses());
    }

    /**
     * Getter method for the font size
     * @return fontSize, floating-point value
     */
    public static float getFontSize()
    {
        return fontSize;
    }

    /**
     * Updates the HighScoreEntry
     * @param delta The time elapsed since the last update cycle#
     */
    @Override
    public void update(float delta) {
        super.update(delta);
        frame.update(delta);
    }

    /**
     * Sets the position of the high score entry.
     * @param _x    x coordinates as a floating-point value
     * @param _y    y coordinates as a floating-point value
     */
    @Override
    public void setPosition(float _x, float _y) {
        super.setPosition(_x, _y);
        frame.setPosition(x, y);
    }

    /**
     * Changes the position of the high score entry
     * @param changeInX
     * @param changeInY
     */
    @Override
    public void changePosition(float changeInX, float changeInY) {
        super.changePosition(changeInX, changeInY);
        frame.changePosition(changeInX, changeInY);
    }

    /**
     * Sets the size of the high score entry.
     * @param _width Sets the width of object
     * @param _height Sets the height of object
     */
    @Override
    public void setSize(float _width, float _height) {
        super.setSize(_width, _height);
        frame.setSize(_width, _height);
    }

    /**
     * Renders the high score entry
     * @param canvas Canvas object that is passed for drawing calls
     */
    @Override
    public void render(Canvas canvas) {
        if (!isVisible) return;
        super.render(canvas);

        //Scale the font to match the height when being expanded by SizeEvent
        GlobalVariables.paint.setTextSize(fontSize);
        GlobalVariables.paint.setColor(Color.WHITE);

        //Ranking
        canvas.drawText(rank, x + width / 32, y + height * 5 / 8, GlobalVariables.paint);

        //Score
        canvas.drawText(entryScore, x + width * 1.6f / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Name
        canvas.drawText(name, x + width * 3 / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Average scores
        canvas.drawText(averageScore, x + width * 7 / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Win Draw Loss text
        GlobalVariables.paint.setColor(Color.GREEN);
        canvas.drawText("W", x + width * 8.3f / 16, y + height * 5 / 8, GlobalVariables.paint);
        GlobalVariables.paint.setColor(Color.YELLOW);
        canvas.drawText("D", x + width * 10f / 16, y + height * 5 / 8, GlobalVariables.paint);
        GlobalVariables.paint.setColor(Color.RED);
        canvas.drawText("L", x + width * 11.5f / 16, y + height * 5 / 8, GlobalVariables.paint);
        GlobalVariables.paint.setColor(Color.WHITE);

        //Wins
        canvas.drawText(wins, x + width * 9 / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Draws
        canvas.drawText(draws, x + width * 10.5f / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Losses
        canvas.drawText(losses, x + width * 12 / 16, y + height * 5 / 8, GlobalVariables.paint);

        //Total time played
        canvas.drawText(totalTimePlayed, x + width * 13 / 16, y + height * 5 / 8, GlobalVariables.paint);

        frame.render(canvas);
    }

    /**
     * Getter method for needsRemoval
     * @param event The MotionEvent that the device has detected
     * @return false because nothing needs to be done
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
