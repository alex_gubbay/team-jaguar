package com.example.nightcrysis.sunka.Screen;

import static com.example.nightcrysis.sunka.GlobalVariables.*;

import android.graphics.Canvas;
import android.view.MotionEvent;

import com.example.nightcrysis.sunka.Audio.SoundManager;
import com.example.nightcrysis.sunka.EntityEvent.EventController;
import com.example.nightcrysis.sunka.EntityEvent.TimerEvent;
import com.example.nightcrysis.sunka.Enum.ScreenState;
import com.example.nightcrysis.sunka.GameEntity.SpriteEntity;
import com.example.nightcrysis.sunka.Graphics.ImageManager;
import com.example.nightcrysis.sunka.R;
import com.example.nightcrysis.sunka.EntityEvent.AccelerationEvent;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.HighscoreFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.InstructionsFrame;
import com.example.nightcrysis.sunka.UIComponents.CustomFrames.SettingsFrame;
import com.example.nightcrysis.sunka.Screen.TransitionEffect.ChangeScreenTimer;
import com.example.nightcrysis.sunka.EntityEvent.PositionEvent;
import com.example.nightcrysis.sunka.EntityEvent.SizeEvent;
import com.example.nightcrysis.sunka.UIComponents.SoundButton;
import com.example.nightcrysis.sunka.UIComponents.UIButton;

/**
 * Created by NIghtCrysIs on 2015/10/20.
 * MainMenuScreen represents the MainMenu in game. It extends the abstract class Screen
 */
public class MainMenuScreen extends Screen{
    private boolean menuDown = false;
    private SpriteEntity titleName, ropes;
    private UIButton startButton, instructionButton, settingsButton, highscoresButton, exitButton;
    private SoundButton soundButton;

    /**
     * Constructor for MainMenuScreen. Instantiates variables and objects to their
     * respective positions, sizes, etc.
     */
    public MainMenuScreen()
    {
        //Setting ratio for ropes and buttons
        float ratio = (backgroundWidth * 0.45f) / ImageManager.getBitmapWidth(R.drawable.rope);

        //Configuring size and position for ropes
        float tempWidth = ImageManager.getBitmapWidth(R.drawable.rope) * ratio,
        tempHeight = ImageManager.getBitmapHeight(R.drawable.rope) * ratio;

        ropes = new SpriteEntity(screenSizeX/2 - (tempWidth/2), backgroundOffsetY - (70 * backgroundRatio) - tempHeight,
                tempWidth, tempHeight, R.drawable.rope);

        // Instantiating, setting text, and adding to screen cycle for each button.
        startButton = new UIButton(ropes.getX(), ropes.getY() + (358 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_start_game) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_start_game) * ratio,
                R.drawable.main_menu_start_game, R.drawable.main_menu_start_game_pressed);
        userInterface.add(startButton);
        
        instructionButton = new UIButton(ropes.getX(), startButton.getY() + (118 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_instructions) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_instructions) * ratio,
                R.drawable.main_menu_instructions, R.drawable.main_menu_instructions_pressed);
        userInterface.add(instructionButton);
        
        settingsButton = new UIButton(ropes.getX(), instructionButton.getY() + (118 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_settings) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_settings) * ratio,
                R.drawable.main_menu_settings, R.drawable.main_menu_settings_pressed);
        userInterface.add(settingsButton);
        
        highscoresButton = new UIButton(ropes.getX(), settingsButton.getY() + (120 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_highscores) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_highscores) * ratio,
                R.drawable.main_menu_highscores, R.drawable.main_menu_highscores_pressed);
        userInterface.add(highscoresButton);
        
        exitButton = new UIButton(ropes.getX(), highscoresButton.getY() + (113 * backgroundRatio),
                ImageManager.getBitmapWidth(R.drawable.main_menu_exit) * ratio, ImageManager.getBitmapHeight(R.drawable.main_menu_exit) * ratio,
                R.drawable.main_menu_exit, R.drawable.main_menu_exit_pressed);
        userInterface.add(exitButton);

        tempWidth = startButton.getHeight() + (10 * backgroundRatio);
        soundButton = new SoundButton(startButton.getX() + startButton.getWidth() + (20 * backgroundRatio), startButton.getY() - (5 * backgroundRatio) + ropes.getHeight(),
                tempWidth, tempWidth);
        soundButton.setVisible(false);
        userInterface.add(soundButton);

        userInterface.add(ropes);

        //Setting ratio for Sunka title
        ratio = (backgroundWidth * 0.55f) / ImageManager.getBitmapWidth(R.drawable.sunka_title);

        //Configuring size and position for title
        tempWidth = ImageManager.getBitmapWidth(R.drawable.sunka_title) * ratio;
        tempHeight = ImageManager.getBitmapHeight(R.drawable.sunka_title) * ratio;

        titleName = new SpriteEntity(screenSizeX/2 - (tempWidth/2), backgroundOffsetY - ropes.getHeight(),
                tempWidth, tempHeight, R.drawable.sunka_title);
        userInterface.add(titleName);
    }

    /**
     * Update method. Makes a check to is the menu is down,
     * if not, it will enterScreen() will be called to drop the
     * menu down once.
     * Calls MainBackground.update to update the cloud positions
     * @param delta    time passed since last update cycle
     * @see MainBackground
     * @see Screen
     */
    @Override
    public void update(float delta) {
        MainBackground.update(delta);

        if (!menuDown & !ScreenManager.transitioning)
        {
            enterScreen();
            SoundManager.playSound(R.raw.ui_transition1);
            menuDown = true;
        }

        super.update(delta);
    }

    /**
     * Rendering method. Renders MainBackground's image and clouds,
     * and makes a call to parent class' render method
     * @param canvas    Canvas object used to draw images on screen
     * @see MainBackground
     * @see Screen (parent)
     */
    @Override
    public void render(Canvas canvas) {
        MainBackground.render(canvas);
        super.render(canvas);
    }

    private void enterScreen()
    {
        float displacement = ropes.getHeight();

        //Moves menu down
        addToUnderlay(new PositionEvent(titleName, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(ropes, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(startButton, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(instructionButton, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(settingsButton, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(highscoresButton, 0, displacement, 0.8f, true, false));
        addToUnderlay(new PositionEvent(exitButton, 0, displacement, 0.8f, true, false));

        EventController eventController = new EventController();
        eventController.addEvent(new TimerEvent(0.5f));
        eventController.addEvent(new SizeEvent(soundButton, 0.3f, true, false));
        addToUnderlay(eventController);
        soundButton.setVisible(true);
    }

    /**
     * enterScreen method makes calls to PositionEvent for all related on screen entities
     * to make them drop down on screen.
     */
    private void changeScreen(ScreenState transitionTo)
    {
        //Moves the entire menu up (accelerated movement)
        float initVelocityY = startButton.getHeight() * 2,
                accelerationY = -AccelerationEvent.accelYOffScreenValueUp(0, ropes.getHeight(), initVelocityY, 0.8f);

        addToUnderlay(new AccelerationEvent(titleName, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(ropes, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(startButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(instructionButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(settingsButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(highscoresButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));
        addToUnderlay(new AccelerationEvent(exitButton, 0, initVelocityY, 0, accelerationY, 0.8f, true));

        addToUnderlay(new SizeEvent(soundButton, 0.3f, false, true));

        startButton.setEnabled(false);
        instructionButton.setEnabled(false);
        settingsButton.setEnabled(false);
        highscoresButton.setEnabled(false);
        exitButton.setEnabled(false);
        soundButton.setEnabled(false);

        //Adds a timer for screen transition
        addToUnderlay(new ChangeScreenTimer(1, transitionTo));
    }

    /**
     * User input method. Checks collision between input and buttons, then
     * makes calls to respective pressed buttons.
     * @param event    The MotionEvent that the device has detected
     */
    @Override
    public void onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        if (event.getAction() == MotionEvent.ACTION_UP)
        {
            if (startButton.isPressed())
            {
                changeScreen(ScreenState.gameSelection);
            }
            else if (instructionButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                InstructionsFrame tempFrame = new InstructionsFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + sidePadding,
                        backgroundWidth - (sidePadding * 2), backgroundHeight - (sidePadding * 2), R.drawable.frame2_unpressed);
                //tempFrame.setScale(2);
                addToUserInterface(tempFrame);
            }
            else if (settingsButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                SettingsFrame tempFrame = new SettingsFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + (sidePadding / 2),
                        backgroundWidth - (sidePadding * 2), backgroundHeight - sidePadding, R.drawable.frame2_unpressed);
                //tempFrame.setScale(2);
                addToUserInterface(tempFrame);
            }
            else if (highscoresButton.isPressed())
            {
                float sidePadding = backgroundWidth / 8;
                HighscoreFrame tempFrame = new HighscoreFrame(backgroundOffsetX + sidePadding, backgroundOffsetY + (sidePadding * 2 / 3),
                        backgroundWidth - (sidePadding * 2), backgroundHeight - sidePadding, R.drawable.frame2_unpressed);
                addToUserInterface(tempFrame);
            }
            else if (exitButton.isPressed())
                System.exit(0);
        }
    }
}
