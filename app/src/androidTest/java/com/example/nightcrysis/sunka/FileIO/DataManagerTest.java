package com.example.nightcrysis.sunka.FileIO;

import android.test.AndroidTestCase;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Alex on 24/10/2015.
 */
public class DataManagerTest extends AndroidTestCase {


    @Test
    public void testReadPlayers() throws Exception {


        DataManager dm = new DataManager();
        dm.clearPlayers();

        dm.addPlayer(new GamePlayer("alex2", 100, 100000, 20, 30, 40, 50));
        for (int i = 0; i < 100; i++) {

            dm.addPlayer(new GamePlayer("alex", 10, 10, 20, 30, 40, 50));
        }

        Assert.assertEquals(2, dm.getAllPlayers().size());
        //random sample
    }

    @Test
    public void testWriteGeneralFile() throws Exception {

        DataManager dm = new DataManager();

        ArrayList<String> testValues = new ArrayList<String>();
        testValues.add("value 1");
        testValues.add("value 2");
        testValues.add("value 3");
        dm.saveData("test", testValues);
        Assert.assertEquals(dm.getData("test").size(), 3);

    }

    @Test
    public void testReadGeneralFile() throws Exception {

        DataManager dm = new DataManager();

        ArrayList<String> testValuesData = dm.getData("test");

        Assert.assertEquals(testValuesData.size(), 3);

    }
}