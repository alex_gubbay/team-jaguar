package com.example.nightcrysis.sunka.GameEntity;

/**
 * Created by tuffail on 27/10/15.
 */

import android.test.ActivityInstrumentationTestCase2;

import com.example.nightcrysis.sunka.MainActivity;
import com.example.nightcrysis.sunka.R;

public class BigPotTest extends ActivityInstrumentationTestCase2<MainActivity>{

    MainActivity mainActivity;
    BigPot bigPot;

    public BigPotTest(){

        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception{
        super.setUp();
        mainActivity = getActivity();
    }

    public void testGetPebbles(){
        bigPot = new BigPot(0,0,20,20);
        assertEquals(null, bigPot.getPebbles());
    }

}
