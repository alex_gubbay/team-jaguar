package com.example.nightcrysis.sunka.FileIO;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by Alex on 03/11/2015.
 */
public class moveTimerTest extends TestCase {


    private moveTimer mt;
    public void setUp() throws Exception {
        //create a new move timer object;
        super.setUp();
        mt = new moveTimer();
    }

    public void testStartGame() throws Exception {

        mt.startGame();
        mt.startNewEvent("test0");
        //timer should hold true to 3 seconds.
        Thread.sleep(3000);
        Assert.assertEquals(3, mt.getEventTimeSeconds("test0"));
    }

    public void testGetGameElapsedTimeSeconds() throws Exception {

        long startTime = System.currentTimeMillis();

        Thread.sleep(1000);

        //checks that the game time progresses as expected.
        boolean testValue = System.currentTimeMillis() - startTime < mt.getGameElapsedTimeSeconds();
        Assert.assertEquals(true, testValue);
    }

    public void testStartNewEvent() throws Exception {
        for (int i = -3; i < 1000; i++) {
            mt.startNewEvent("test" + i);
        }
        //Tests the handling for events that don't exist.
        for (int i = -4; i < 1000; i++) {
            Assert.assertNotNull(mt.getEventTimeSeconds("test" + i));
        }
    }

    public void testGetEventTimeSeconds() throws Exception {
        //makes sure that the test still exists.
        mt.startNewEvent("test1");
        Assert.assertNotNull(mt.getEventTimeSeconds("test1"));

        for (int i = -4; i < 1000; i++) {
            Assert.assertNotNull(mt.getEventTimeSeconds("test" + i));
        }
    }

    public void testEndEvent() throws Exception {

        //tests that all events end correctly.
        for (int i = -4; i < 1000; i++) {
            mt.endEvent("test" + i);
        }
        for (int i = -4; i < 1000; i++) {
            //checks that events are created consistently.
            Assert.assertEquals(0, mt.getEventTimeSeconds("test" + i));
        }
    }

    public void testGetDeadEventSeconds() throws Exception {
        //creates an event, kills it, and checks how old it was when it was destroyed.
        mt.startNewEvent("deadTest");
        Thread.sleep(1000);
        mt.endEvent("deadTest");
        Assert.assertEquals(1, mt.getDeadEventSeconds("deadTest"));
    }


}