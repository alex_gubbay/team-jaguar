package com.example.nightcrysis.sunka.FileIO;



/**
 *
 * Test class for highscoreManager
 * Created by Alex on 21/10/2015.
 */

import android.test.AndroidTestCase;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;


public class HighscoreManagerTest extends AndroidTestCase {


    @Before
    public void setupWriter() {
    }
    @Test
    public void testAdd() {

        HighScoreManager high = new HighScoreManager(getContext());
        high.clearScores();

        //Test adding a score
        high.addScore("John Smith", 47);
        high.addScore("Alex Gubbay", 23);

        Assert.assertEquals(2, high.getScores().size());

        //test consistency of data retrieved from file

        Assert.assertEquals(high.getScores().get(0).getName(), "John Smith");
        Assert.assertEquals(high.getScores().get(0).getScoreValue(), 47);
        Assert.assertEquals(high.getScores().get(1).getName(), "Alex Gubbay");
        Assert.assertEquals(high.getScores().get(1).getScoreValue(), 23);
    }

    @Test
    public void testSequentialWrite() {

        HighScoreManager high = new HighScoreManager(getContext());
        //test Functionality of sequential write to file
        high.addScore("Pablo Escabar", 55);

        Assert.assertEquals(high.getScores().get(0).getName(), "Pablo Escabar");
        Assert.assertEquals(high.getScores().get(2).getScoreValue(), 23);

        //test functionality of clearing the scores.

    }

    @Test
    public void testClear() {
        HighScoreManager high = new HighScoreManager(getContext());
        high.clearScores();

        Assert.assertEquals(high.getScores().size(), 0);


    }

    @Test
    public void testOrderLogic() {
        HighScoreManager high = new HighScoreManager(getContext());

        high.clearScores();

        //Test adding a score
        high.addScore("John Smith", 47);
        high.addScore("Alex Gubbay", 23);
        high.addScore("Pablo Escabar", 55);

        Assert.assertEquals(high.getScores().get(0).getName(), "Pablo Escabar");
    }
}
