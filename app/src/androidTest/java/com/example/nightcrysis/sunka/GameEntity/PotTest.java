package com.example.nightcrysis.sunka.GameEntity;

/**
 * Created by tuffail on 31/10/15.
 */

import android.test.ActivityInstrumentationTestCase2;

import com.example.nightcrysis.sunka.Enum.Player;
import com.example.nightcrysis.sunka.MainActivity;
import com.example.nightcrysis.sunka.R;

import java.util.Stack;

public class PotTest extends ActivityInstrumentationTestCase2<MainActivity>{

    MainActivity mainActivity;
    Pot pot;

    public PotTest(){

        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception{
        super.setUp();
        mainActivity = getActivity();
    }

    public void testGetPebbles(){
        pot = new BigPot(0,0,20,20);
        Stack<Pebble> pebbles = pot.getPebbles();

        for (int i = 0; i < 5; ++i){
            Pebble newPebble = new Pebble(0, 0, 0, 0, R.drawable.blue_gem_1);
            pot.addPebble(newPebble);
        }
        assertEquals(pebbles,pot.getPebbles());
    }

    public void testPebbleCount(){
        pot = new BigPot(0,0,20,20);
        Pebble[] pebbles = new Pebble[5];

        for (int i = 0; i < pebbles.length; ++i){
            pebbles[i] = new Pebble(0, 0, 0, 0, R.drawable.blue_gem_1);
        }
        for (int i = 0; i < pebbles.length; ++i){
            pot.addPebble(pebbles[i]);
        }
        assertEquals(pebbles.length, pot.getPebbleCount());
    }

    public void testNextPot(){
        pot = new BigPot(0,0,20,20);
        SmallPot nextPot = new SmallPot(0,0,10,10);
        pot.setNextPot(nextPot);
        assertEquals(nextPot, pot.getNextPot());
    }

    public void testPlayer(){
        pot = new BigPot(0,0,20,20);
        pot.setPlayer(Player.A);
        assertEquals(Player.A,pot.getPlayer());
    }

    public void testHighlighted(){
        pot = new BigPot(0,0,20,20);
        pot.setHighlighted(true);
        assertEquals(true,pot.getHighlighted());
    }

    public void testLocked(){
        pot = new BigPot(0,0,20,20);
        pot.setLocked(false);
        assertEquals(false,pot.isLocked());
    }
}
