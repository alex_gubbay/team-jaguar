package com.example.nightcrysis.sunka.FileIO;

import android.util.Log;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;


/**
 * Created by Alex on 07/11/2015.
 */
public class socketServerTest extends TestCase {

    @Test
    public void testSetUp() throws Exception {
        Log.d("RUN", "NOW RUNNING TEST!");


        DataTransmissionManager.openServer();

        Log.d("RUN", DataTransmissionManager.getIp() + " ");
        Log.d("RUN", DataTransmissionManager.getPort() + " ");

        while (!DataTransmissionManager.connected()) {

        }
        Log.d("RUN", DataTransmissionManager.getPort() + " ");
        Assert.assertNotNull(DataTransmissionManager.getIp());
        Assert.assertNotNull(DataTransmissionManager.getPort());
    }

    @Test
    public void testSendData() throws Exception {
        DataTransmissionManager.queueMessageForSending("TEST");
    }

}
