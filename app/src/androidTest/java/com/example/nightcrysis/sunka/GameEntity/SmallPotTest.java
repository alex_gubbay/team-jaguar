package com.example.nightcrysis.sunka.GameEntity;

/**
 * Created by tuffail on 27/10/15.
 */

import android.test.ActivityInstrumentationTestCase2;

import com.example.nightcrysis.sunka.MainActivity;




public class SmallPotTest extends ActivityInstrumentationTestCase2<MainActivity>{

    MainActivity mainActivity;
    SmallPot smallPot;

    public SmallPotTest(){

        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception{
        super.setUp();
        mainActivity = getActivity();
    }

    public void testOppositePot(){
        smallPot = new SmallPot(0,0,20,20);
        SmallPot oppositePot = new SmallPot(0,0,1,1);
        smallPot.setOppositePot(oppositePot);
        assertEquals(oppositePot, smallPot.getOppositePot());
    }

}
